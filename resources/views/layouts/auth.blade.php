<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <title>@yield('title')</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    
    {!! Html::style('resources/assets/bootstrap/css/bootstrap.min.css') !!}
    {!! Html::style('resources/assets/skin/default/css/font-awesome.min.css') !!}
    {!! Html::style('resources/assets/skin/default/css/AdminLTE.min.css') !!}
    {!! Html::style('resources/assets/skin/default/css/skin-purple.min.css') !!}
  </head>
  <body class="hold-transition login-page">
    
      <!-- Page Content -->
      @yield('content')
      <!-- /#page-wrapper -->

    <!-- jQuery 2.1.4 -->
    <script src="../../plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="../../bootstrap/js/bootstrap.min.js"></script>
    <!-- iCheck -->
    <script src="../../plugins/iCheck/icheck.min.js"></script>
    <script>
      $(function () {
        $('input').iCheck({
          checkboxClass: 'icheckbox_square-blue',
          radioClass: 'iradio_square-blue',
          increaseArea: '20%' // optional
        });
      });
    </script>
  </body>
</html>
