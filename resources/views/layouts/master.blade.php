<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="description" content="MKGT DASHBOARD">
    <meta name="author" content="DEQUODE">
    <title>@yield('title')</title>
    
    {!! Html::style('resources/assets/bootstrap/css/bootstrap.min.css') !!}
    {!! Html::style('resources/assets/skin/default/css/font-awesome.min.css') !!}
    {!! Html::style('resources/assets/skin/default/css/AdminLTE.min.css') !!}
    {!! Html::style('resources/assets/skin/default/css/skin-purple.min.css') !!}
    {!! Html::style('resources/assets/skin/default/css/font-awesome.min.css') !!}
    {!! Html::style('resources/assets/skin/default/css/custom-styles.css') !!}

    @yield('css')
</head>
<body class="hold-transition skin-purple sidebar-mini">
    <div class="wrapper">
        <!-- Page Header , Nav -->
        <header class="main-header">
            @include('include.header.header')
        </header>
        <!-- /Page Header , Nav -->

        <!-- Page Sidebar  -->
        <aside class="main-sidebar">
            @include('include.sidebar.sidebar')
        </aside>
        <!-- /Page Sidebar  -->
        

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            @yield('content')
        </div>
        <!-- /Content Wrapper. Contains page content -->

        <!-- Footer content -->
        <footer class="main-footer" style="height:48px">
            <div class="pull-right" style="font-weight:bold;">Powered by: DEQUODE</div>
            @yield('footer')
        </footer>
        <!-- /Footer content -->
    </div>
    <!-- jQuery -->
    {!! Html::script('resources/assets/plugins/jquery/jQuery-2.1.4.min.js') !!}
    <!-- Bootstrap Core JavaScript -->
    {!! Html::script('resources/assets/bootstrap/js/bootstrap.min.js') !!}
    <!-- App Menu Plugin JavaScript -->
    {!! Html::script('resources/assets/skin/default/js/app.min.js') !!}
    <!-- Fast click JavaScript -->
    {!! Html::script('resources/assets/plugins/fastclick/fastclick.min.js') !!}
    <!-- Slim scroll click JavaScript -->
    {!! Html::script('resources/assets/plugins/slimScroll/jquery.slimscroll.min.js') !!}

    @yield('jsaddon')
</body>
</html>
