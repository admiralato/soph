<section class="content-header">
	<h1>{{ isset($pageTitle) ? $pageTitle : '' }}</h1>
	<ol class="breadcrumb">
		<li>
			<a href="{{ url('/home') }}">Home</a>
		</li>
		<li class="active">{{ isset($pageTitle) ? $pageTitle : '' }}</li>
	</ol>
</section>