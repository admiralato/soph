<!-- Logo -->
<a href="{{ URL::to('/dashboard') }}" class="logo">
  <!-- mini logo for sidebar mini 50x50 pixels -->
  <span class="logo-mini">BMBOC</span>
  <!-- logo for regular state and mobile devices -->
  <span class="logo-lg"><b>BMBOC</b></span>
</a>

@include('include.header.nav')