<ul class="sidebar-menu">
	<li class="header">MAIN NAVIGATION</li>
<!--
	<li class="active">
      <a href="{{ url('/dashboard')}}">
        <i class="fa fa-dashboard"></i> <span>Dashboard</span></i>
      </a>
    </li>
	<li>
      <a href="{{ url('/sales')}}">
        <i class="fa fa-usd"></i> <span>Sales</span>
        <small class="label pull-right bg-green">new</small>
      </a>
    </li>
    <li>
      <a href="{{ url('/packages')}}">
        <i class="fa fa-dropbox"></i> <span>Packages</span>
      </a>
    </li>
    <li>
      <a href={{ url('/incentives') }}>
        <i class="fa fa-money"></i> <span>Incentives</span>
      </a>
    </li>
    <li>
      <a href="{{ url('/profile')}}">
        <i class="fa fa-info-circle"></i> <span>Profiles</span>
      </a>
    </li>
    <li>
      <a href="{{ url('/downlines')}}">
        <i class="fa fa-sitemap"></i> <span>Downlines</span>
      </a>
    </li>
    <li>
      <a href="{{ url('/member')}}">
        <i class="fa fa-group"></i> <span>Members</span>
      </a>
    </li>
    <li class="treeview">
      <a href="#">
        <i class="fa fa-university"></i> 
        <span>Accounts</span>
        <i class="fa fa-angle-left pull-right"></i>
      </a>
      <ul class="treeview-menu">
        <li><a href="{{ url('/')}}"><i class="fa fa-circle-o"></i> Cheques</a></li>
      </ul>
    </li>
    
    <li class="header">SETTINGS</li>
    <li class="treeview">
      <a href="#">
        <i class="fa fa-group"></i> 
        <span>User Manager</span>
        <i class="fa fa-angle-left pull-right"></i>
      </a>
      <ul class="treeview-menu">
        <li><a href="{{ url('/useraccounts')}}"><i class="fa fa-circle-o"></i> Users</a></li>
        <li><a href="{{ url('/userrole')}}"><i class="fa fa-circle-o"></i> Role</a></li>
        <li><a href="{{ url('/usertype')}}"><i class="fa fa-circle-o"></i> Type</a></li>
      </ul>
    </li>
-->
    {!! menuItems() !!}
    <li class="active">
      <a href="{{ url('auth/logout') }}">
        <i class="fa fa-sign-out"></i> <span>Logout</span></i>
      </a>
    </li>
</ul>
