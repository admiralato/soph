@if ($data && count($data) > 0)
	@foreach($data as $item)
		@if($searchBy==='PROFILE')
		<li>
			<a href="javascript:void(0)" onclick="selectMember({{ $item->id }}, '{{ $item->name }}' ); return false;">{{ $item->name }}</a>
		</li>
		@elseif($searchBy==='SPONSOR')
		<li>
			<a href="javascript:void(0)" onclick="loadUrl('{{ url('member/create/') }}/{{ $item->id }}'); return false;">{{ $item->name }}</a>
		</li>
		@elseif($searchBy==='SPONSORSHORTCUT')
		<li>
			<a href="javascript:void(0)" onclick="loadUrl('{{ url('member/create/') }}/{{ $item->id }}?shortcut=1'); return false;">{{ $item->name }}</a>
		</li>
		@elseif($searchBy==='CATALOG')
		<li>
			<a href="javascript:void(0)" onclick="loadUrl('{{ url('catalog/member') }}?id={{ $item->id }}'); return false;">{{ $item->name }}</a>
		</li>
        @elseif($searchBy==='INCENTIVES')
		<li>
			<a href="javascript:void(0)" onclick="loadUrl('{{ url('bonus/member') }}?id={{ $item->id }}'); return false;">{{ $item->name }}</a>
		</li>
        @elseif($searchBy==='INQUIRY')
		<li>
            <a href="javascript:void(0)" onclick="loadData('{{ $item->id }}')">{{ $item->name }}</a>
		</li>
        @elseif($searchBy==='CHANGE_SPONSOR')
		<li>
            <a href="javascript:void(0)" onclick="selectMember({{ $item->id }}, '{{ $item->name }}' ); return false;">{{ $item->name }}</a>
		</li>
		@else
		<li>
			<a href="javascript:void(0)" onclick="loadUrl('{{ url('downlines/member') }}?id={{ $item->id }}'); return false;">{{ $item->name }}</a>
		</li>
		@endif
	@endforeach
@endif

