@extends('layouts.master')
@section('title')
    BMBOC Admin Panel :: {{ isset($pageTitle) ? $pageTitle : 'User Type' }}
@stop
@section('content')
    @include('include.content_header_block')
<section class="content">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">{{ $pageTitle }}</h3>
                </div><!-- /.box-header -->
                @if ($errors->any())
                    <ul class="alert alert-danger" style="list-style:none">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                @endif
                <div class="box-body">
                    @include ('content.usertype.form')
                </div><!-- /.box-body -->
                <div class="box-footer">
                    <a href="{{ Config::get('app.base_url') }}/usertype/{{ $usertype->id }}/edit" title="Edit" class="btn btn-primary btn-sm pull-right">Edit</a>
                    <a href="{{ Config::get('app.base_url') }}/usertype" class="btn btn-success btn-sm">Back</a>
                </div>
            </div>
        </div>
    </div>
</section>
@stop