<div class="box-body">
    {!! Form::hidden('id', (Route::current()->getName()==='usertype.show') ? $usertype->id: null ) !!}
    <div class="form-group">
        {!! Form::label('title', 'Title:') !!}
        @if (Route::current()->getName()==='usertype.show')
            {!! Form::text('title', $usertype->title, ['class'=>'form-control', 'disabled'=>'disabled'] ) !!}
        @else
            {!! Form::text('title', null, ['class'=>'form-control'] ) !!}
        @endif
    </div>
    <div class="form-group">
        {!! Form::label('employment_type_id', 'Employment Type:') !!}
        @if (Route::current()->getName()==='usertype.show')
            {!! Form::select('role_id', $userroles, $usertype->role_id, ['class'=>'form-control', 'disabled'=>'disabled'] ) !!}
        @else
            {!! Form::select('role_id', $userroles, null, ['placeholder' => 'Select a role...', 'class'=>'form-control'] ) !!}
        @endif
    </div>
</div>
