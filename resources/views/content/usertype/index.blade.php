@extends('layouts.master')
@section('title')
    BMBOC Admin Panel :: {{ isset($pageTitle) ? $pageTitle : 'User Types' }}
@stop
@section('content')
    @include('include.content_header_block')
    <section class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="box box-danger">
                    <div class="box-header with-border">
                        <a href="usertype/create" class="btn btn-sm btn-info btn-flat pull-left"><i class="fa fa-plus"></i> Create New User Type</a>
                    </div>
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover">
                            <tr>
                                <th>Action</th>
                                <th>Title</th>
                                <th>Date Created</th>
                                <th>Date Last Updated</th>
                            </tr>
                            @foreach ($usertype as $item)
                                <tr>
                                    <td>
                                        <a href="usertype/{{ $item->id }}" title="View"><i class="fa fa-eye fa-fw"></i></a>
                                        <a href="usertype/{{ $item->id }}/edit" title="Edit"><i class="fa fa-edit fa-fw"></i></a>
                                        <a href="usertype/{{ $item->id }}/edit" title="Delete"><i class="fa fa-trash fa-fw"></i></a>
                                    </td>
                                    <td>{{ $item->title }}</td>
                                    <td>{{ $item->created_at }}</td>
                                    <td>{{ $item->updated_at }}</td>
                                </tr>
                            @endforeach
                        </table>
                    </div><!-- /.box-body -->
                </div>
            </div>
        </div>
    </section>
@stop