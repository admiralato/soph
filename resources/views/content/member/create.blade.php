@extends('layouts.master')
@section('title')
    BMBOC Admin Panel :: {{ isset($pageTitle) ? $pageTitle : 'Add New Member' }}
@stop
@section('content')
    @include('include.content_header_block')

    <section class="content">
        <div class="row">
            <div class="col-lg-6">
                <div class="box box-danger">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-users"></i> New Member Form</h3>
                        @if ($errors->any())
                            <ul class="alert alert-danger" style="list-style:none">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        @if (Session::has('message'))
                        <div class="alert alert-{{ Session::get('classstyle') }} alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ Session::get('message') }}
                        </div>
                        @endif
                    </div>
                    {!! Form::open(['url'=>'member','method'=>'POST','role'=>'form']) !!}
                    <div class="box-body">
                        @include ('content.member.form')
                    </div>
                    <div class="box-footer">
                        {!! Form::submit('Add Member',['name'=>'create', 'class'=>'btn btn-primary btn-normal pull-right']) !!}
                        <a href="{{ URL::previous() }}" class="btn btn-success btn-normal">Back</a>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>

            <div class="col-lg-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-sitemap"></i> Downlines</h3>
                    </div>
                    <div class="box-body">
                        <div id="treeview-container" class=""></div>        
                    </div>
                </div>
                
            </div>
        </div>
    </section>

@stop

@section('jsaddon')
<script type="text/javascript">
    $(function() {

        var getUrlParameter = function getUrlParameter(sParam) {
            var sPageURL = decodeURIComponent(window.location.search.substring(1)),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');

                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : sParameterName[1];
                }
            }
        };

        var parentId = $('input[name="parent_id"]').val();
        var newMemberId =  getUrlParameter('newid');
        
        if (parentId) {
            var memberParam = "";
            if (parseInt(newMemberId)) {
                memberParam = "?memberid=" +newMemberId;
            }

            $.get("{{ url('api/downlines/members') }}" + memberParam, function(data){
                if (data.length > 0) {
                    //$('#treeview-container').html(data);
                    var treeObj = $(data).appendTo('#treeview-container');
                    $('li[id="'+ parentId+'"] > span').append('<small class="label pull-right bg-orange">sponsor</small>');

                    $('html,body').animate({
                        scrollTop: ($("#"+parentId).offset().top) - 50 },
                    'slow');
                }
            });
        }
        
    });
</script>
@stop