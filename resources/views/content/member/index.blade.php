@extends('layouts.master')
@section('title')
    BMBOC Admin Panel :: {{ isset($pageTitle) ? $pageTitle : 'Member' }}
@stop
@section('content')
    @include('include.content_header_block')

    <section class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="box box-danger">
                    <div class="box-header with-border">
                        <a href="member/create/0" class="btn btn-sm btn-info btn-flat pull-left"><i class="fa fa-plus"></i> Add Root Member</a>
                        <div class="box-tools" style="margin-top:5px;">
                          <a href="{{ url('/member') }}" class="btn btn-sm btn-default btn-flat pull-right" style="margin-left:10px;">Reset</a>
                          <div class="input-group input-group-sm pull-right" style="width: 300px;">
                            <input type="text" name="keywords" class="form-control pull-right" placeholder="Search">
                            <div class="input-group-btn">
                              <button type="submit" class="btn btn-default" id="btn-search"><i class="fa fa-search"></i></button>
                            </div>
                          </div>
                          <div class="input-group input-group-sm pull-right" style="width: 120px; margin-right:10px;">
                            {!! Form::select('search-by', array('mid'=>'Member ID', 'ln' => 'Last Name','fn' => 'First Name','sid' => 'Sponsor ID' ), 'ln', ['class'=>'form-control'] ) !!}
                          </div>
                        </div>
                    </div>
                    <div class="box-body">
                        @if ($members && count($members) > 0) 

                            @if (Session::has('message'))
                            <div class="alert alert-{{ Session::get('classstyle') }} alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                {{ Session::get('message') }}
                            </div>
                            @endif


                            <table class="table table-bordered">
                              <thead>
                                <tr>
                                  <th style="width:140px;">Action</th>
                                  <th style="width:24px;"></th>
                                  <th style="width:110px;">Upper Line ID</th>
                                  <th style="width:250px;">Name</th>
                                  <th>Member Since</th>
                                  <th>Last Purchase Date</th>
                                  <th style="text-align:right">Total Purchase</th>
                                </tr>
                              </thead>
                              <tbody>
                                @foreach($members as $item)
                                  <tr>
                                    <td>
                                        <!--
                                        <a href="member/{{ $item->id }}/edit" title="Edit"><i class="fa fa-edit fa-fw"></i></a>
                                        <a href="member/{{ $item->id }}/edit" title="Delete"><i class="fa fa-trash fa-fw"></i></a>
                                        <span style="padding:0 8px;">|</span>
                                        -->
                                        
                                        <a href="downlines/member?id={{ $item->id }}" title="View Downlines"><i class="fa fa-sitemap fa-fw"></i></a>
                                        <a href="member/create/{{ $item->id }}" title="Add Member"><i class="fa fa-plus-square fa-fw"></i></a>
                                        <a href="catalog/member?id={{ $item->id }}" title="Buy Item"><i class="fa fa-shopping-cart fa-fw"></i></a>
                                        <a href="member/create/{{ $item->id }}?shortcut=1" title="Add Member (Shortcut Version)"><i class="fa fa-bolt fa-fw"></i></a>
                                        <a href="member/change-sponsor/{{ $item->id }}" title="Change Sponsor"><i class="fa fa-child fa-fw"></i></a>
                                        <a href="javascript:void(0);" onclick="deleteMember({{ $item->id}}); return false;" title="Delete"><i class="fa fa-trash fa-fw"></i></a>
                                    </td>
                                    <td style="text-align:center"><img src="{{asset('resources/assets/static/images/flat-'. strtolower($item->gender) .'.png')}}" style="with:24px; height:24px;" alt="{{ $item->last_name }}, {{ $item->first_name }}"></td>
                                    <td>{{{ $item->parent_id == 0? 'ROOT': $item->parent_id }}}</td>
                                    <td>{{ $item->last_name }}, {{ $item->first_name }}</td>
                                    <td>{{ $item->created_at }}</td>
                                    <th>{{ $item->last_purchase_date }}</th>
                                    <td style="text-align:right">{{ $item->sum_grand_total }} Php </td>
                                  </tr>
                                @endforeach

                              </tbody>
                            </table>
                            <div class="paging pull-right">
                              {!! stripos($members->render(),'member?q') > 0? str_replace('?page', '&page', $members->render()): $members->render()  !!}
                            </div>
                        @else
                          <div class="box-body table-responsive">
                            No results found. {{ count($members) }}
                          </div>
                        @endif
                    </div>
                    <div class="box-footer">
                        <a href="{{ URL::previous() }}" class="btn btn-success btn-normal">Back</a>
                    </div>
                </div>
            </div>
        </div>
    </section>

@stop

@section('jsaddon')
<script type="text/javascript">
    $(function() {
      
    var getUrlParameter = function getUrlParameter(sParam) {
            var sPageURL = decodeURIComponent(window.location.search.substring(1)),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');

                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : sParameterName[1];
                }
            }
        };
    var sb =  getUrlParameter('sb');
    if (sb != undefined && sb.length > 0) {
      $('select[name="search-by"]').val(sb);
    }


    $( 'button#btn-search' ).on('click', function(e) {
      e.preventDefault();
      var keywords = $( 'input[name="keywords"]' ).val();
      var searchby = $( 'select[name="search-by"]' ).val();
      var searchurl = "{{ url('/member?q=') }}" + keywords + '&sb='+searchby;
      window.location.href = searchurl;
    });

    });
    
    function deleteMember(memberid)
    {
        if (confirm('Are you sure you want to delete this record from the database?')) {
            // Delete
            $.ajax({
                type: "POST",
                url: "{{ url('member/delete') }}",
                dataType:"json",
                data: { 
                    pMemberId: memberid,
                    "_token": "{{ csrf_token() }}"
                },
                success: function(data) {
                    alert(data.response);
                    window.location.reload();
                }
            });
        } else {
            // Do nothing!
        }
    }
</script>
@stop
