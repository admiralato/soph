@extends('layouts.master')
@section('title')
    BMBOC Admin Panel :: {{ isset($pageTitle) ? $pageTitle : 'Change Sponsor' }}
@stop
@section('content')
    @include('include.content_header_block')
<section class="content">
    <div class="row">
        <div class="col-md-6">
            <div class="info-box bg-green">
                <span class="info-box-icon"><i class="fa fa-user" style="margin-top:20px;"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">MEMBER</span>
                    @if($childProfile)
                        <span class="info-box-number">{!! $childProfile->first_name.' '.$childProfile->last_name !!}</span>
                    @else
                        <span class="info-box-number"></span>
                    @endif
                    <div class="progress">
                        <div class="progress-bar" style="width: 50%"></div>
                    </div>
                    <span class="progress-description">
                        @if($parentProfile)
                            SPONSOR: {!! $parentProfile->first_name.' '.$parentProfile->last_name !!}
                        @else
                            SPONSOR: 
                        @endif
                    </span>
                </div><!-- /.info-box-content -->
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="box box-info">
                <div class="box-header with-border">
                  <h3 class="box-title">New Sponsor</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    {!! Form::open(['url'=>'member/updateSponsor','method'=>'POST', 'role'=>'form']) !!}
                    {!! Form::hidden('member_id', $memberId) !!}
                    {!! Form::hidden('old_sponsor_id', $currentParentId) !!}
                    {!! Form::hidden('new_sponsor_id', null) !!}
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-search"></i>
                                    </div>
                                    {!! Form::text('member_profile', null, ['placeholder'=>'Search member name', 'class'=>'form-control', 'onkeyup'=>'up(this)', 'onkeydown'=>'down(this)', 'autocomplete'=>'off']) !!}
                                </div>
                                <ul id="member-profile" class="dropdown-menu" style="top:33px;">
                                </ul>
                            </div>
                        </div>
                    </div>
                </div><!-- /.box-body -->
                <div class="box-footer clearfix">
                    <input name="create" class="btn btn-primary  btn-normal pull-right" type="submit" value="Set New Sponsor">
                    <a href="{{ URL::previous() }}" class="btn btn-success btn-normal">Back</a>
                </div><!-- /.box-footer -->
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</section>
@stop
@section('jsaddon')
    <script type="text/javascript">
        $(function() 
        {
            var getUrlParameter = function getUrlParameter(sParam) {
                var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;

                for (i = 0; i < sURLVariables.length; i++) {
                    sParameterName = sURLVariables[i].split('=');

                    if (sParameterName[0] === sParam) {
                        return sParameterName[1] === undefined ? true : sParameterName[1];
                    }
                }
            };

            var newMemberId =  getUrlParameter('id');
            var memberParam = "";

            if (newMemberId && parseInt(newMemberId)) {
                memberParam = "?id=" +newMemberId;
            }
            
            $('#member-profile').css('top','83px !important');
            $('#member-profile').css('left','10px !important');
        });
        
        var keyTimer;

        function up(obj) 
        {
            keyTimer = setTimeout(function() {
                var keywords = $(obj).val();
                keywords = encodeURI(keywords);
                if (keywords.length > 1){
                    $.get("{{ url('member/search/profilename') }}?q=" + keywords + "&member=1", function(data){
                        if (data.length > 0) {
                            $('#member-profile').html(data);
                            $('#member-profile').dropdown().toggle();	
                        }
                    });
                }
            }, 400);
        }

        function down(obj)
        {
            clearTimeout(keyTimer);
        }
        
        function selectMember(id, name)
        {
            $('input[name="new_sponsor_id"]').val(id);
            $('input[name="member_profile"]').val(name);
            $('#member-profile').dropdown().toggle();
        }
        
    </script>
@stop