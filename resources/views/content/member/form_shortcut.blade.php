
<div class="row">
	<div class="col-lg-12">
		<strong><i class="fa fa-sitemap margin-r-5"></i> Sponsor</strong>
		<p>
			<div class="form-group">
				{!! Form::hidden('parent_id', $item->id) !!}
			    {!! Form::hidden('member_profile_id', $item->profile_id) !!}
			    <div class="input-group" style="width:100%"> 
			        {!! Form::text('parent', $item->name, ['placeholder'=>'Enter Sponsor name', 'class'=>'form-control', 'onkeyup'=>'up(this)', 'onkeydown'=>'down(this)', 'autocomplete'=>'off']) !!}
			    </div>
			    <ul id="sponsor-profile" class="dropdown-menu"></ul>
			</div>
		</p>
		<hr>
	</div>
	
	<div class="col-lg-12">
		<strong><i class="fa fa-user margin-r-5"></i> Profile Basic Info</strong>
		<p class="text-muted" style="font-size:0.8em;">
	    	Please ensure that all fields are completed and formatted correctly before you submit.
	  	</p>
		<p>
			<div class="form-group">
	            <div class="input-group">
	                <div class="input-group-addon">
	                    <i class="fa fa-info"></i>
	                </div>
	                {!! Form::text('first_name', null, ['placeholder'=>'Enter first name', 'class'=>'form-control']) !!}
	            </div>
	        </div>
		</p>
		<p>
			<div class="form-group">
	            <div class="input-group">
	                <div class="input-group-addon">
	                    <i class="fa fa-info"></i>
	                </div>
	                {!! Form::text('last_name', null, ['placeholder'=>'Enter last name', 'class'=>'form-control']) !!}
	            </div>
	        </div>
		</p>
		<p>
			<div class="form-group">
	            <div class="input-group" style="width:100%">
	                {!! Form::text('member_since', null, ['placeholder'=>'Date Registered (YYYY-MM-DD)', 'class'=>'form-control']) !!}
	            </div>
	            
	        </div>
		</p>
	</div>
</div>


