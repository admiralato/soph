

<div class="row">
    <div class="col-lg-12">
        <div class="form-group">
        	{!! Form::hidden('parent_id', $item->id) !!}
            {!! Form::hidden('member_profile_id', $item->profile_id) !!}
            {!! Form::label('parent', 'Sponsor') !!} <span class="required">*</span>
            <div class="input-group">
                <div class="input-group-addon">
                    <i class="fa fa-sitemap"></i>
                </div>
                {!! Form::text('parent', $item->name, ['placeholder'=>'Parent', 'class'=>'form-control', 'onkeyup'=>'up(this)', 'onkeydown'=>'down(this)', 'autocomplete'=>'off']) !!}
            </div>
            <ul id="sponsor-profile" class="dropdown-menu"></ul>
        </div>
    </div>
    <div class="col-lg-12">
        <div class="form-group">
        	{!! Form::hidden('id', null) !!}
			{!! Form::hidden('profile_id', null) !!}
            {!! Form::label('member_profile', 'Member Profile') !!} <span class="required">*</span>
            <div class="input-group">
                <div class="input-group-addon">
                    <i class="fa fa-user"></i>
                </div>
                {!! Form::text('member_profile', null, ['placeholder'=>'Enter member (required)', 'class'=>'form-control', 'onkeyup'=>'up(this)', 'onkeydown'=>'down(this)', 'autocomplete'=>'off']) !!}
            </div>
            <ul id="member-profile" class="dropdown-menu"></ul>
        </div>
    </div>
            
</div>
<script type="text/javascript">
	var keyTimer;

	function up(obj) {
        var elName = $(obj).attr('name');
		keyTimer = setTimeout(function() {
			var keywords = $(obj).val();
			keywords = encodeURI(keywords);
			if (keywords.length > 1){
                if (elName=='member_profile') {
                    $.get("{{ url('api/search/profilename') }}?q=" + keywords + "&profile=1", function(data){
                        if (data.length > 0) {
                            $('#member-profile').html(data);
                            $('#member-profile').dropdown().toggle();   
                        }
                    });    
                } else {
                    $.get("{{ url('api/search/profilename') }}?q=" + keywords + "&sponsor=1", function(data){
                        if (data.length > 0) {
                            $('#sponsor-profile').html(data);
                            $('#sponsor-profile').dropdown().toggle();   
                        }
                    });
                }
				
			}
		}, 400);
	}

	function down(obj){
		clearTimeout(keyTimer);
	}

    function loadUrl(url){
        window.location.href = url;
        return false;
    }

	function selectMember(id, name){

		var memberProfileId = $('input[name="member_profile_id"]').val();
        if(memberProfileId==id) { 
			alert('Action not allowed! Invalid assignment of Parent and Member.'); 
			return;
		}
		$('input[name="profile_id"]').val(id);
		$('input[name="member_profile"]').val(name);
		$('#member-profile').dropdown().toggle();

		window.location.hash = id+'-'+ name.replace(' ','-');
		
	}
</script>