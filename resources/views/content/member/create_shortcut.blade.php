@extends('layouts.master')
@section('title')
    BMBOC Admin Panel :: {{ isset($pageTitle) ? $pageTitle : 'Add New Member (Shorcut Version)' }}
@stop
@section('content')
    @include('include.content_header_block')

    <section class="content">
        <div class="row">
        	@if ($errors->any())
        	<div class="col-lg-12">
                <ul class="alert alert-danger" style="list-style:none">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif

            @if (Session::has('message'))
            <div class="col-lg-12">
	            <div class="alert alert-{{ Session::get('classstyle') }} alert-dismissable">
	                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	                {{ Session::get('message') }}
	            </div>
	        </div>
            @endif
        	{!! Form::open(['url'=>'membershort','method'=>'POST','role'=>'form', 'id'=>'form-member-shortcut']) !!}
            <div class="col-lg-3">
            	<div class="box box-primary">
	            	<div class="box-body">
	              		@include ('content.member.form_shortcut')
		            </div>
		            <!-- /.box-body -->
          		</div>
            </div>
            <div class="col-lg-5">
            	<div class="box box-primary">
	            	<div class="box-body">
	              		@include ('content.packages.catalog_shortcut')
		            </div>
		            <!-- /.box-body -->
          		</div>
            </div>
            <div class="col-lg-4">
            	<div class="box box-warning">
            		<div class="box-body">
            			@include ('content.packages.cart')
            		</div>
		            <!-- /.box-body -->
          		</div>
            </div>
            {!! Form::close() !!}
        </div>
        <div class="row">
        	<div class="col-lg-12">
                <div class="box box-danger">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-sitemap"></i> Downlines</h3>
                    </div>
                    <div class="box-body">
                        <div id="treeview-container" class=""></div>        
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop
@section('jsaddon')
{!! Html::script('resources/assets/plugins/date/jquery.plugin.min.js') !!}
{!! Html::script('resources/assets/plugins/date/jquery.dateentry.min.js') !!}
<script type="text/javascript">
	$(function() {
		$('input[name="created_at"]').dateEntry({dateFormat: 'ymd-'});
		$('input[name="member_since"]').dateEntry({dateFormat: 'ymd-'});

		$('input[id="packageno"]').keypress(function(e) {
		    if(e.which == 13) {
		    	var pno = $( this ).val();
		    	var pobj = $('a[data-id="'+pno+'"]');
		    	
		    	if(pno.length == 0 | parseInt(pno) < 1 | pobj.length ==0) { 
					$( "span#warning" ).text( "Invalid Package ..." ).show().fadeOut( 2000 );
					$('input[id="packageno"]').select();
					return false;
				} else {
					addToCart(pno);
				}
				e.preventDefault();
		    }

		});

		$('#add-to-cart').on('click', function(e) {
			var pno = $( 'input[id="packageno"]' ).val();
			var pobj = $('a[data-id="'+pno+'"]');

			if(pno.length == 0 | parseInt(pno) < 1 | pobj.length ==0) { 
				$( "span#warning" ).text( "Invalid Package ..." ).show().fadeOut( 2000 );
				$('input[id="packageno"]').select();
				return false;
			} else {
				addToCart(pno);
			}
		});

		$('#products a').on('click', function(e) {
			var pno = $( this ).attr('data-id');
			addToCart(pno);
		});

		$( "#form-member-shortcut" ).submit(function( event ) {
        	var fname = $('input[name="first_name"]').val();
        	var lname = $('input[name="last_name"]').val();
        	var packages = $('input[name="packages"]').val();
        	
        	  	
        	if (packages && packages.length >0 && lname.length >0 && fname.length >0) {
        		$( "#form-member-shortcut" ).submit();
        	} else {
        		alert('Something went wrong. Please check your cart and all required fields..');
        		event.preventDefault();
        	}

		});

		$(document).on('click', 'a.x-cart-item', function(){
			var id = $( this ).attr('id');			
			$('#package-id-'+id).remove();
			updateCart();
		});

		var addToCart = function addToCart(sku) {
			var id = $('a[data-id="'+sku+'"]').attr('id');
			var amount = $('a[data-id="'+sku+'"]').attr('data-amount');

			var html = '<tr id="package-id-'+ id +'" data-amount="'+ amount +'">';
		    html += '<td><a href="#" title="Remove" class="x-cart-item" id="'+ id +'"><i class="fa fa-close fa-fw"></i></a></td>';
		    html += '<td>Package '+ sku +'</td>';
		    html += '<td>'+ amount +'</td>';
		    html += '</tr>';

		    $( html ).appendTo('table#cart-items > tbody');
		    $( '#place-order').removeClass('disabled');
		    updateCart();
		    $('input[id="packageno"]').select();
		    alert('Package '+ sku +' was successfully added in the cart.');
		}

		var formatNumber = function numberWithCommas(x) {
		    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		}

		var updateCart = function updateCart(){
			var total =0;
			var packages = '';
			var count =0;
			$('table#cart-items > tbody tr').each(function( index ) {
				if ($( this ).attr('id') != undefined) {
					var id = $( this ).attr('id');
					var amount = $( this ).attr('data-amount');
					amount = amount.replace(',','');
					console.log(amount);
					console.log(parseFloat(amount));
					total = parseFloat(total) + parseFloat(amount);
					id = id.replace("package-id-", "")
					packages += (packages.length > 0? ',': '') + id;
					count +=1;
					//console.log( index + ": " + id  + " : " + amount );
				}
			});

			$('input[name="packages"]').val(packages);
			$('#grand-total').html(formatNumber(total));
			$('#cart-count').html(count);

			if (total <= 0){
				$( '#place-order').addClass('disabled');
			}

			console.log("packages: " + packages  + " : " + total );
		}


		/* -------------------------  Downlines ---------------------------- */
		var getUrlParameter = function getUrlParameter(sParam) {
            var sPageURL = decodeURIComponent(window.location.search.substring(1)),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');

                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : sParameterName[1];
                }
            }
        };

        var parentId = $('input[name="parent_id"]').val();
        var newMemberId =  getUrlParameter('newid');
        
        if (parentId) {
            var memberParam = "";
            if (parseInt(newMemberId)) {
                memberParam = "?memberid=" +newMemberId;
            }

            $.get("{{ url('api/downlines/members') }}" + memberParam, function(data){
                if (data.length > 0) {
                    //$('#treeview-container').html(data);
                    var treeObj = $(data).appendTo('#treeview-container');
                    $('li[id="'+ parentId+'"] > span').append('<small class="label pull-right bg-orange">sponsor</small>');
                }
            });
        }

		/* 
		$( "#products a" )
		  .on( "mouseenter", function() {
		    var pno = $( this ).attr('data-id');
		    var pamt = $( this ).attr('data-amount');
		  })
		  .on( "mouseleave", function() {
			$('#product-info').html('<p>No selected package</p>');
		});
		*/
	});

	/*------------------------- Parent Search ------------------------*/
	var keyTimer;

	function up(obj) {
        var elName = $(obj).attr('name');
		keyTimer = setTimeout(function() {
			var keywords = $(obj).val();
			keywords = encodeURI(keywords);
			if (keywords.length > 1){
                if (elName=='member_profile') {
                    $.get("{{ url('api/search/profilename') }}?q=" + keywords + "&profile=1", function(data){
                        if (data.length > 0) {
                            $('#member-profile').html(data);
                            $('#member-profile').dropdown().toggle();   
                        }
                    });    
                } else {
                    $.get("{{ url('api/search/profilename') }}?q=" + keywords + "&sponsor=1&shortcut=1", function(data){
                        if (data.length > 0) {
                            $('#sponsor-profile').html(data);
                            $('#sponsor-profile').dropdown().toggle();   
                        }
                    });
                }
				
			}
		}, 400);
	}

	function down(obj){
		clearTimeout(keyTimer);
	}

    function loadUrl(url){
        window.location.href = url;
        return false;
    }

	function selectMember(id, name){

		var memberProfileId = $('input[name="member_profile_id"]').val();
		if(memberProfileId==id) { 
			alert('Action not allowed! Invalid assignment of Parent and Member.'); 
			return;
		}
		$('input[name="profile_id"]').val(id);
		$('input[name="member_profile"]').val(name);
		$('#member-profile').dropdown().toggle();

		window.location.hash = id+'-'+ name.replace(' ','-');
		
	}
</script>
@stop