<style>
    td.details-control {
        background: url('../soph/resources/assets/static/images/details_open.png') no-repeat center center;
        cursor: pointer;
    }
    tr.shown td.details-control {
        background: url('../soph/resources/assets/static/images/details_close.png') no-repeat center center;
    }
</style>
@extends('layouts.master')
@section('title')
    BMBOC Admin Panel :: {{ isset($pageTitle) ? $pageTitle : 'Members Inquiry' }}
@stop
@section('content')
    @include('include.content_header_block')

{!! Html::style('resources/assets/css/ionicons.min.css') !!}

<section class="content">
    <div class="row">
        <div class="col-lg-4">
            <div class="box box-warning">
                <div class="box-header with-border">
                    <h3 class="box-title"><i class="fa fa-dot-circle-o"></i> Search Member Name</h3>
                </div>
                <div class="box-body">
                    {!! Form::hidden('member_id', $memberId) !!}
                    {!! Form::hidden('profile_id', null) !!}
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-search"></i>
                        </div>
                        @if ($profile)
                            {!! Form::text('member_profile', $profile->first_name.' '.$profile->last_name, ['placeholder'=>'Search member name', 'class'=>'form-control', 'onkeyup'=>'up(this)', 'onkeydown'=>'down(this)', 'autocomplete'=>'off']) !!}
                        @else
                            {!! Form::text('member_profile', null, ['placeholder'=>'Search member name', 'class'=>'form-control', 'onkeyup'=>'up(this)', 'onkeydown'=>'down(this)', 'autocomplete'=>'off']) !!}
                        @endif
                    </div>
                    <ul id="member-profile" class="dropdown-menu" style="top:82px;"></ul>
                </div>
<!--
                <div class="box-footer">
                    <a href="{{ URL::previous() }}" class="btn btn-success btn-normal">Back</a>
                </div>
-->

            </div>
        </div>
        <div class="col-md-4">
            <div id="member-status-box" class="info-box bg-green">
                <span class="info-box-icon"><i class="fa fa-group" style="margin-top: 20px;"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Status</span>
                    <span id="member-status" class="info-box-number">ACTIVE</span>
                    <div class="progress">
                        <div class="progress-bar" style="width: 50%"></div>
                    </div>
                    <span class="progress-description">
                    Sponsor: <span id="sponsor-name"></span>
                  </span>
                </div><!-- /.info-box-content -->
            </div>
        </div>
        <div class="col-md-4">
            <div class="info-box bg-aqua">
                <span class="info-box-icon"><i class="fa fa-shopping-cart" style="margin-top: 20px;"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Purchases</span>
                  <span id="total-purchase" class="info-box-number">0.00 Php</span>
                  <div class="progress">
                    <div class="progress-bar" style="width: 50%"></div>
                  </div>
                  <span class="progress-description">
                    Last Purchase Date: <span id="last-purchase-date"></span>
                  </span>
                </div><!-- /.info-box-content -->
              </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Purchases <img src={{ url("resources/assets/static/images/ajax-loader.gif") }} id="loading-purchases" style="" /></h3>
                </div>
                <div class="box-body">
                    <div class="table-responsive">
                        <table id="table-purchases" class="table table-responsive no-margin">
                          <thead>
                            <tr>
                              <th>Purchase Date</th>
                              <th>Package</th>
                              <th>Price</th>
                            </tr>
                          </thead>
                          <tbody>

                          </tbody>
                        </table>
                  </div>
                </div>
                <div class="box-footer clearfix">
                      <a id="btn-place-order" href={!! url('/catalog') !!} class="btn btn-sm btn-info btn-flat pull-left">Place New Order</a>
                  </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Released Incentives <img src={{ url("resources/assets/static/images/ajax-loader.gif") }} id="loading-incentives" style="" /></h3>
                </div>
                <div class="box-body">
                    <div class="table-responsive">
                        <table id="table-incentives" class="table no-margin">
                          <thead>
                            <tr>
                              <th>Released Date</th>
                              <th>Amount</th>
                            </tr>
                          </thead>
                          <tbody>

                          </tbody>
                        </table>
                  </div>
                </div>
                <div class="box-footer clearfix">
                  <a id="btn-compute" href={!! url('/bonus') !!} class="btn btn-sm btn-info btn-flat pull-left">Compute Incentives</a>
                </div>
            </div>
        </div>
    </div>
    <section id="preview" class="invoice">
        <div class="row">
            <div class="col-xs-12 table-responsive">
                <table id="member-downline" class="table table-striped">
                    <thead>
                      <tr>
                        <th>Downline Name</th>
                        <th>Date Registered</th>
                        <th>Status</th>
                      </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section>
</section>

@stop

@section('jsaddon')
{!! Html::script('resources/assets/js/jquery.json.js') !!}
<script type="text/javascript">
    var level = 1;
    var maxLevel = 10;
    var parentIds = null;
    
	$(function() {
        
        $('#loading-purchases').hide();
        $('#loading-incentives').hide();
        $('#loading-downlines').hide();
        $('#btn-place-order').addClass('disabled');
        $('#btn-compute').addClass('disabled');
        
//        $(window).scroll(function(){
//            if($('#preview').is(':visible') && $(window).scrollTop() + screen.height > $('#preview').height() + 100) {
//                // Load another level of incentives
//                level++;
//                $.ajax({
//                    type: "POST",
//                    async: false,
//                    url: "{{ url('/inquiry/get/remaining-purchases') }}",
//                    dataType:"json",
//                    data: { 
//                        pLevel: level,
//                        pParents: parentIds,
//                        "_token": "{{ csrf_token() }}"
//                    },
//                    success: function(data) {
//                        parentIds = null;
//                        level = data.level;
//                        parentIds = data.parents;
//
//                        $('#level'+level).after(data.html);
//                        $('#loading-level'+level).remove();
//                        $('#loading-level').hide();
//                        if(level == maxLevel) {
//                            $('#loading-level').hide();
//                        }
//                    }
//                });
//                
//            }
//        });

        var getUrlParameter = function getUrlParameter(sParam) {
            var sPageURL = decodeURIComponent(window.location.search.substring(1)),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');

                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : sParameterName[1];
                }
            }
        };

        var newMemberId =  getUrlParameter('id');
        var memberParam = "";
        if (newMemberId && parseInt(newMemberId)) {
            memberParam = "?memberid=" +newMemberId;
        }

        $('#member-profile').css('top','83px !important');
        $('#member-profile').css('left','10px !important');
    });

	var keyTimer;

	function up(obj) {
		keyTimer = setTimeout(function() {
			var keywords = $(obj).val();
			keywords = encodeURI(keywords);
			if (keywords.length > 1){
				$.get("{{ url('inquiry/search/profilename') }}?q=" + keywords + "&member=1", function(data){
					if (data.length > 0) {
						$('#member-profile').html(data);
						$('#member-profile').dropdown().toggle();	
					}
				});
			}
		}, 400);
	}

	function down(obj){
		clearTimeout(keyTimer);
	}


	function loadUrl(url){
		window.location.href = url;
		return false;
	}
    
    function loadData(memberId) {
        level = 1;
        var rowHtml = '';
        
        $('#member-profile').dropdown().toggle();
        var member = "";
        if (memberId && parseInt(memberId)) {
            member = "?id=" +memberId;
        }
        
        if(memberId != null) {
            $('#loading-purchases').show();
            $('#loading-incentives').show();
            $('#loading-downlines').show();
            
            // Purchases
            $.ajax({
                type: "GET",
                url: "{{ url('inquiry/purchases/purchases') }}" + member,
                dataType:"json",
                success: function( data ) {
                    var status = data.status;
                    if(status) {
                        $('#member-status').html('ACTIVE');
                        $('#member-status-box').addClass('bg-green').removeClass('bg-red');
                    } else {
                        $('#member-status').html('INACTIVE');
                        $('#member-status-box').addClass('bg-red').removeClass('bg-green');
                    }
                    
                    $('#total-purchase').html(data.totalPurchase + ' Php');
                    $('#last-purchase-date').html(data.lastPurchaseDate);
                    
                    $('#table-purchases').find('tr:gt(0)').remove();
                    $('#table-purchases tbody').append(data.details);
                    $('#parent-name').html(data.parent);
                    $('input[name="member_profile"]').val(data.member);
                    $('#loading-purchases').hide();
                    $('#btn-place-order').removeClass('disabled');
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    $('#table-purchases tbody').append('<tr>No results found.</tr>');
                    $('#loading-purchases').hide();
                }
            });
            
            // Released Incentives
            $.ajax({
                type: "GET",
                url: "{{ url('inquiry/incentives/incentives') }}" + member,
                dataType:"json",
                success: function( data ) {
                    $('#table-incentives').find('tr:gt(0)').remove();
                    $('#table-incentives tbody').append(data.details);
                    $('#loading-incentives').hide();
                    $('#btn-compute').removeClass('disabled');
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    $('#table-incentives tbody').append('<tr>No results found.</tr>');
                    $('#loading-incentives').hide();
                }
            });
            
            // Downlines
            $.ajax({
                type: "POST",
                url: "{{ url('inquiry/get/referrals') }}",
                dataType:"json",
                data: { 
                    pMemberId: memberId,
                    pParents: memberId,
                    pLevel: level,
                    "_token": "{{ csrf_token() }}"
                },
                success: function(data) {
                    maxLevel = data.maxLevel;
                    parentIds = data.parents;
                    level = data.level;
                    
                    for(c = 1; c <= maxLevel; c++) {
                        rowHtml += '<tr id="level'+c+'">';
                        rowHtml += '<td colspan="3" style="font-weight:bold;">LEVEL '+c+'</td>';
                        rowHtml += '</tr>';
                        rowHtml += '<tr id="loading-level'+c+'"><td colspan="3" style="text-align:center;"><img id="loading-level" src="{!! url("resources/assets/static/images/loader_1.gif") !!}" /></td></tr>';
                    }
                    $('#sponsor-name').html(data.sponsor.first_name + ' ' + data.sponsor.last_name);
                    $('#member-downline').find('tr:gt(0)').remove();
                    $('#member-downline tbody').append(rowHtml);
                    $('#level'+level).after(data.html);
                    $('#loading-level'+level).remove();
                    
                    requestAll().done(function(items) {
                        $('#loading-level').hide();
                    });
                }
            });
        }
    }

	function selectMember(id, parentid, profileid, name){
        alert('yes');
		$('input[name="profile_id"]').val(profileid);
		$('input[name="member_id"]').val(id);
		$('input[name="member_profile"]').val(name);
		$('#member-profile').dropdown().toggle();
	}
    
    /* Formatting function for row details - modify as you need */
    function format ( member ) {
        var detail = member.detail;
        var detailCount = detail.length;
        var htmlString = "";
        for(var i = 0; i < detailCount; i++) {
            if(detail[i].status) {
                htmlString = htmlString + '<tr><td></td><td>'+ detail[i].name + '</td><td>'+ detail[i].registration +'</td><td><span class="label label-success">ACTIVE</span></td></tr>';
            } else {
                htmlString = htmlString + '<tr><td></td><td>'+ detail[i].name + '</td><td>' + detail[i].registration + '</td><td><span class="label label-danger">INACTIVE</span></td></tr>';
            }
            
        }
        // `d` is the original data object for the row
        return '<table class="table table-hover">'+
                htmlString
        '</table>';
    }
    
    function request(unilevel) { 
        // Load another level of incentives
        var dates = $('#coverage').val();
        var coverage = null;
        if(dates != null && dates != "") {
            coverage = dates.split('-');
        }
        $('#loading-level').show();
        
        // return the AJAX promise
        return $.ajax({
            type: "POST",
            url: "{{ url('/inquiry/get/remaining-purchases') }}",
            dataType:"json",
            data: { 
                pLevel: unilevel,
                pParents: parentIds,
                "_token": "{{ csrf_token() }}"
            }
        });
    }
    
    function requestUnilevels(page) {
        return request(page).then(function(data){
            if (parseInt(data.level) > maxLevel) {
                return null;
            } else {
                level = data.level;
                parentIds = data.parents;

                $('#level'+level).after(data.html);
                $('#loading-level'+level).remove();
                $('#loading-level').hide();
                return requestUnilevels(parseInt(data.level) + 1);
            }
        });
    }
    
    function requestAll(){
        return requestUnilevels(2);
    }
</script>
@stop