@extends('layouts.master')
@section('title')
    BMBOC Admin Panel :: {{ isset($pageTitle) ? $pageTitle : 'Released Incentives' }}
@stop
@section('content')
    @include('include.content_header_block')
    <section class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <a href="bonus" class="btn btn-sm btn-info btn-flat pull-left"><i class="fa fa-plus"></i> Release Incentive</a>
                        <div class="box-tools" style="margin-top:5px;">
                          <a href="{{ url('/released_incentives') }}" class="btn btn-sm btn-default btn-flat pull-right" style="margin-left:10px;">Reset</a>
                          <div class="input-group input-group-sm pull-right" style="width: 300px;">
                            <input type="text" name="keywords" class="form-control pull-right" placeholder="Search">
                            <div class="input-group-btn">
                              <button type="submit" class="btn btn-default" id="btn-search"><i class="fa fa-search"></i></button>
                            </div>
                          </div>
                          <div class="input-group input-group-sm pull-right" style="width: 120px; margin-right:10px;">
                            {!! Form::select('search-by', array('ln' => 'Last Name','fn' => 'First Name'), 'ln', ['class'=>'form-control'] ) !!}
                          </div>
                        </div>
                    </div>
                    @if($released && count($released) > 0)
                    <div class="box-body table-responsive">
                        <table class="table table-hover">
                            <tr>
                                <th style="width:100px;">Action</th>
                                <th>Last Name</th>
                                <th>First Name</th>
                                <th>Middle Name</th>
                                <th>Grand Total</th>
                                <th>Date Released</th>
                                <th>Released By</th>
                            </tr>
                            @foreach ($released as $bonus)
                                <tr>
                                    <td>
                                        <a href="released_incentives/{{ $bonus->ri_id }}" title="View"><i class="fa fa-eye fa-fw"></i></a>
                                        <a href="javascript:void(0);" onclick="deleteRecord({{ $bonus->ri_id }}); return false;" title="Delete"><i class="fa fa-trash fa-fw"></i></a>
<!--
                                        <a href="#" title="Edit"><i class="fa fa-edit fa-fw"></i></a>
                                        <a href="#" title="Delete"><i class="fa fa-trash fa-fw"></i></a>
-->
                                    </td>
                                    <td>{{ $bonus->last_name }}</td>
                                    <td>{{ $bonus->first_name }}</span></td>
                                    <td>{{ $bonus->middle_name }}</td>
                                    <td>{{ number_format($bonus->grand_total, 2) }}</td>
                                    <td>{{ $bonus->released_date }}</td>
                                    <td>{{ $bonus->released_by }}</td>
                                </tr>
                            @endforeach
                        </table>
                        <div class="paging pull-right">
                          {!! $released->render() !!}
                        </div>
                    </div><!-- /.box-body -->
                    @else
                      <div class="box-body table-responsive">
                        No results found. {{ count($released) }}
                      </div>
                    @endif
                </div>
            </div>
        </div>

       

    </section>
@stop

@section('jsaddon')
<script type="text/javascript">
  $(function() {

    var getUrlParameter = function getUrlParameter(sParam) {
            var sPageURL = decodeURIComponent(window.location.search.substring(1)),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');

                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : sParameterName[1];
                }
            }
        };

    var sb =  getUrlParameter('sb');
    if (sb != undefined && sb.length > 0) {
      $('select[name="search-by"]').val(sb);
    }
    

    $( 'button#btn-search' ).on('click', function(e) {
      e.preventDefault();
      var keywords = $( 'input[name="keywords"]' ).val();
      var searchby = $( 'select[name="search-by"]' ).val();
      var searchurl = "{{ url('/released_incentives?q=') }}" + keywords + '&sb='+searchby;
      window.location.href = searchurl;
    });

  });
    
    function deleteRecord(id)
    {
        if(confirm('Are you sure you want to delete this record?')) {
            $.ajax({
                type: "POST",
                url: "{{ url('released_incentives/delete') }}",
                dataType:"json",
                data: { 
                    released_incentives_id: id,
                    "_token": "{{ csrf_token() }}"
                },
                success: function(data) {
                    alert(data.response);
                    window.location.href = "{{ url('/released_incentives') }}";
                }
            });
        } else {
            return;
        }
    }
</script>
@stop