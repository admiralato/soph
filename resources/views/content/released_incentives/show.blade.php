@extends('layouts.master')
@section('title')
    BMBOC Admin Panel :: {{ isset($pageTitle) ? $pageTitle : 'Released Incentives' }}
@stop
@section('content')
    @include('include.content_header_block')
<section class="content">
    <div class="row">
        <div class="col-md-6">
            <div class="info-box bg-yellow">
                <span class="info-box-icon"><i class="fa fa-user" style="margin-top:20px;"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">MEMBER</span>
                    <span class="info-box-number">{!! $released->first_name.' '.$released->last_name !!}</span>
                    <div class="progress">
                        <div class="progress-bar" style="width: 50%"></div>
                    </div>
                    <span class="progress-description">
                        Registered: {!! $released->registration_date !!}
                    </span>
                </div><!-- /.info-box-content -->
            </div>
        </div>
        <div class="col-md-6">
            <div class="info-box bg-aqua">
                <span class="info-box-icon"><i class="fa fa-money" style="margin-top:20px;"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">RELEASED INCENTIVES</span>
                    <span class="info-box-number">{{ number_format($released->grand_total, 2) }} Php</span>
                    <div class="progress">
                        <div class="progress-bar" style="width: 40%"></div>
                    </div>
                    <span class="progress-description">
                        Released On: {!! $released->released_date !!} | By: {!! $released->released_by !!}
                    </span>
                </div><!-- /.info-box-content -->
            </div>
        </div>
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Incentive</h3>
                    @if($released->from)
                        <h3 id="coverage" class="box-title label label-primary pull-right">{!! $released->from !!} to {!! $released->to !!}</h3>
                    @endif
                </div><!-- /.box-header -->
                <div class="box-body">
                    <table id="sales-details" class="table table-hover">
                        <thead>
                            <tr>
                                <th>Sales Order No.</th>
                                <th>Member</th>
                                <th>Total</th>
                                <th>Purchased Date</th>
                            </tr>
                        </thead>
                        <tfoot>

                        </tfoot>
                        <tbody>

                        </tbody>
                    </table>
                </div><!-- /.box-body -->
                <div class="box-footer">
                    <a href="{{ URL::previous() }}" class="btn btn-success btn-sm">Back</a>
                </div>
            </div>
        </div>
    </div>
</section>
@stop
@section('jsaddon')
<script type="text/javascript">
	$(function() {
        var sales_ids = {!! $released->sales_ids !!};
        var html = null;
        if(sales_ids != null) {
            $.ajax({
                type: "POST",
                url: "{{ url('released_incentives/sales_details') }}",
                dataType:"json",
                data: { 
                    pSales: sales_ids,
                    "_token": "{{ csrf_token() }}"
                },
                success: function(data) {
                    var sales = data.sales;
                    for(c = 0; c < sales.length; c++)
                    {
                        html += '<tr>';
                        html += "<td><a href=javascript:void(0) onclick=\"viewSalesDetails('" + sales[c].id + "'); return false;\">" + sales[c].id + '</a></td>';
                        html += '<td>' + sales[c].name + '</td>';
                        html += '<td>' + sales[c].grand_total + '</td>';
                        html += '<td>' + sales[c].created_at + '</td>';
                        html += '</tr>';
                    }
                    
                    $('#sales-details tbody').append(html);
                }
            });
        }
	});
    
    function viewSalesDetails(memberid) 
    {
        // TODO: Display modal form here to view details
        $.ajax({
            type: "POST",
            url: "{{ url('released_incentives/details') }}",
            dataType:"json",
            data: { pMemberId: memberid, "_token": "{{ csrf_token() }}" },
            success: function(data) {
                var salesData = data.sales;
                var html = null;
                for(c = 0; c < salesData.length; c++) {
                    html += '<tr>';
                    html += '<td>' + salesData[c].created_at + '</td>';
                    html += '<td>Package ' + salesData[c].package_title + '</td>';
                    html += '<td>' + salesData[c].price + '</td>';
                    html += '</tr>';
                }
                $('#details tbody tr:not(:first-child)').remove();
                $('#details tbody').append(html);
            }
        });
        $('#view-details').modal('show');
    }
</script>
@stop

<div id="view-details" class="modal modal-primary fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Sales Details</h4>
            </div>
            <div class="modal-body">
                <div class="box box-danger">
                <div class="box-header">
                  <h3 class="box-title">Purchases</h3>
                </div><!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                  <table id="details" class="table table-striped table-hover">
                    <tbody><tr>
                      <th>Purchased On</th>
                      <th>Package</th>
                      <th>Amount</th>
                    </tr>
                    
                  </tbody></table>
                </div><!-- /.box-body -->
              </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>