<div class="col-md-4">
    <div class="info-box bg-aqua">
        <span class="info-box-icon"><i class="ion ion-ios-pricetag-outline"></i></span>
        <div class="info-box-content">
            <span class="info-box-number">Admiral Ato</span>
            <span class="info-box-text">Paid bonuses <span class="label label-success">Php 200,650</span></span>
            <div class="progress">
                <div class="progress-bar" style="width: 50%"></div>
            </div>
            <span class="progress-description">
                Unpaid bonuses <span class="label label-warning">Php 100,800</span>
            </span>
        </div><!-- /.info-box-content -->
      </div>
</div>
<div class="col-md-4">
    <div class="info-box bg-yellow">
        <span class="info-box-icon"><i class="ion ion-ios-heart-outline"></i></span>
        <div class="info-box-content">
            <span class="info-box-text">Status</span>
            <span class="info-box-number"><span class="label label-success">ACTIVE</span></span>
            <div class="progress">
                <div class="progress-bar" style="width: 20%"></div>
            </div>
            <span class="progress-description">
                Member since January 23, 2015
            </span>
        </div><!-- /.info-box-content -->
      </div>
</div>
<div class="col-md-12">
    <!-- Custom Tabs (Pulled to the right) -->
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs pull-right">
            <li class=""><a href="#tab_3-2" data-toggle="tab" aria-expanded="true">Level 3</a></li>
            <li class=""><a href="#tab_2-2" data-toggle="tab" aria-expanded="false">Level 2</a></li>
            <li class="active"><a href="#tab_1-1" data-toggle="tab" aria-expanded="false">Level 1</a></li>
            <li class="pull-left header"><i class="fa fa-th"></i> Downlines</li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="tab_1-1">
                <table class="table table-bordered">
                    <tbody>
                        <tr>
                            <th style="width: 10px">#</th>
                            <th>Name</th>
                            <th>Income</th>
                        </tr>
                        <tr>
                            <td>1.</td>
                            <td>Karl Fonacier</td>
                            <td>
                                Php 12,000
                            </td>
                        </tr>
                        <tr>
                            <td>2.</td>
                            <td>Nico Robin</td>
                            <td>
                                Php 8,000
                            </td>
                        </tr>
                        <tr>
                            <td>3.</td>
                            <td>RObin Nico</td>
                            <td>
                                Php 9,000
                            </td>
                        </tr>
                        <tr>
                            <td>4.</td>
                            <td>Monkey Luffy</td>
                            <td>
                                Php 7,500
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div><!-- /.tab-pane -->
            <div class="tab-pane" id="tab_2-2">
                <table class="table table-bordered">
                    <tbody>
                        <tr>
                            <th style="width: 10px">#</th>
                            <th>Name</th>
                            <th>Sponsor</th>
                            <th>Income</th>
                        </tr>
                        <tr>
                            <td>1.</td>
                            <td>Barrack Obama</td>
                            <td>Karl Fonacier</td>
                            <td>
                                Php 2,000
                            </td>
                        </tr>
                        <tr>
                            <td>2.</td>
                            <td>Robin Hood</td>
                            <td>Nico Robin</td>
                            <td>
                                Php 4,700
                            </td>
                        </tr>
                        <tr>
                            <td>3.</td>
                            <td>Jennifer Lopez</td>
                            <td>Robin Nico</td>
                            <td>
                                Php 9,100
                            </td>
                        </tr>
                        <tr>
                            <td>4.</td>
                            <td>Justin Bieber</td>
                            <td>Monkey Luffy</td>
                            <td>
                                Php 17,500
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div><!-- /.tab-pane -->
            <div class="tab-pane" id="tab_3-2">
                    <table class="table table-bordered">
                    <tbody>
                        <tr>
                            <th style="width: 10px">#</th>
                            <th>Name</th>
                            <th>Sponsor</th>
                            <th>Income</th>
                        </tr>
                        <tr>
                            <td>1.</td>
                            <td>Maria Meredes</td>
                            <td>Barrack Obama</td>
                            <td>
                                Php 2,000
                            </td>
                        </tr>
                        <tr>
                            <td>2.</td>
                            <td>Juan dela Cruz</td>
                            <td>Robin Hood</td>
                            <td>
                                Php 4,700
                            </td>
                        </tr>
                        <tr>
                            <td>3.</td>
                            <td>Madonna</td>
                            <td>Jennifer Lopez</td>
                            <td>
                                Php 9,100
                            </td>
                        </tr>
                        <tr>
                            <td>4.</td>
                            <td>Selena Gomez</td>
                            <td>Justin Bieber</td>
                            <td>
                                Php 17,500
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div><!-- /.tab-pane -->
        </div><!-- /.tab-content -->
    </div><!-- nav-tabs-custom -->
</div>