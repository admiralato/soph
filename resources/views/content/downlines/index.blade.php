@extends('layouts.master')
@section('title')
    BMBOC Admin Panel :: {{ isset($pageTitle) ? $pageTitle : 'Downlines' }}
@stop
@section('content')
    @include('include.content_header_block')

    <section class="content">
        <div class="row">
            <div class="col-lg-4">
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-search"></i> Search</h3>
                    </div>  
                    
                    <div class="box-body">
                    	{!! Form::hidden('member_id', $memberId) !!}
                    	{!! Form::hidden('profile_id', null) !!}
			            <div class="input-group">
			                <div class="input-group-addon">
			                    <i class="fa fa-search"></i>
			                </div>
			                {!! Form::text('member_profile', null, ['placeholder'=>'Search member name', 'class'=>'form-control', 'onkeyup'=>'up(this)', 'onkeydown'=>'down(this)', 'autocomplete'=>'off']) !!}
			            </div>
			            <ul id="member-profile" class="dropdown-menu" style="top:83px;">
					    </ul>
                    </div>
                    <div class="box-footer">
                        <a href="{{ URL::previous() }}" class="btn btn-success btn-normal">Back</a>
                    </div>
                </div>
                <div class="content-center" style="margin:10px 0;">
			    	<a href="{{ url('/catalog') }}" class="add-cart btn btn-info btn-lg">View Products/Packages</a>
			    </div> 
            </div>
            <div class="col-lg-8">
	            <div class="box box-info">
	            	<div class="box-header with-border">
	                      <h3 class="box-title"><i class="fa fa-sitemap"></i> Downlines</h3>
	                </div>  
	                <div class="box-body">
	                	<div id="treeview-container" class=""></div> 
	                </div>
                </div>
            </div>
        </div>
    </section>
@stop
@section('jsaddon')
<script type="text/javascript">
	$(function() {

        var getUrlParameter = function getUrlParameter(sParam) {
            var sPageURL = decodeURIComponent(window.location.search.substring(1)),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');

                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : sParameterName[1];
                }
            }
        };

        var newMemberId =  getUrlParameter('id');
        var memberParam = "";
        if (newMemberId && parseInt(newMemberId)) {
            memberParam = "?memberid=" +newMemberId;
        }
        
        $.get("{{ url('api/downlines/members') }}" + memberParam, function(data){
            if (data.length > 0) {
                var treeObj = $(data).appendTo('#treeview-container');
                $('li[id="'+ newMemberId+'"] small').remove();
                $('li[id="'+ newMemberId+'"] > span > i').attr('class','fa fa-check-square');

                $('html,body').animate({
                    scrollTop: ( $("#"+newMemberId).offset().top ) - 50 },
                'slow');
            }
        });
        

        $('#member-profile').css('top','83px !important');
        $('#member-profile').css('left','10px !important');
    });

	var keyTimer;

	function up(obj) {
		keyTimer = setTimeout(function() {
			var keywords = $(obj).val();
			keywords = encodeURI(keywords);
			if (keywords.length > 1){
				$.get("{{ url('api/search/profilename') }}?q=" + keywords + "&member=1", function(data){
					if (data.length > 0) {
						$('#member-profile').html(data);
						$('#member-profile').dropdown().toggle();	
					}
				});
			}
		}, 400);
	}

	function down(obj){
		clearTimeout(keyTimer);
	}


	function loadUrl(url){
		window.location.href = url;
		return false;
	}


	function selectMember(id, parentid, profileid, name){
        alert('yes');
		$('input[name="profile_id"]').val(profileid);
		$('input[name="member_id"]').val(id);
		$('input[name="member_profile"]').val(name);
		$('#member-profile').dropdown().toggle();

		/*
		window.location.href = "{{ url('downlines/member') }}?id=" + id;
		
		$('#treeview-container ul li.active > span > i').removeAttr('class');
		$('li[id="'+ id+'"]').attr('class','active');
		$('li[id="'+ id+'"] small').remove();
        $('li[id="'+ id+'"] > span > i').attr('class','fa fa-check-square');
		window.location.hash = 'bypass-'+id+'-'+name.replace(' ','-');
		*/
		
	}
</script>
@stop