<section class="content" id="products">
    <div class="row">
    	
    	@if ($packages && count($packages) > 0) 
    		@foreach($packages as $item)
	    	<div class="col-xs-6 col-sm-6 col-md-4">
		    	<div class="box box-widget widget-user-2">
		    		<div class="widget-user-header bg-{{ $item->color }}">
		    			<div class="panel-title"><i class="fa fa-dropbox fa-fw"></i> Package {{ $item->title }}</div>
	        			<div class="plan-price">{{ $item->amount }}<span style="font-size:14px;">Php</span></div>
		    		</div>
		    		<div class="box-footer no-padding">
		    			<ul class="nav nav-stacked content-center">
					      <li>100% Lorem ipsum dolor sit amet</li>
					      <li><small class="label bg-navy">{{ $item->first_fifteen }}</small> 1st</li>
					      <li><small class="label bg-navy">{{ $item->second_fifteen }}</small> 2nd</li>
					      <li><small class="label bg-navy">{{ $item->third_fifteen }}</small> 3rd</li>
					    </ul>
					    <div class="content-center" style="margin:10px 0;">
					    	<a href="javascript:void(0);" id="{{ $item->id }}" data-sku="{{ $item->title }}" data-amount="{{ $item->amount }}" class="add-cart btn btn-info btn-lg{{ ($member == null? ' disabled':'') }} ">Buy Now!</a>
					    </div>
		    		</div>
		    	</div>
		    </div>
		    @endforeach
	    @else
			<h3>
			No results found... }}
			</h3>
	    @endif
    </div>
</section>