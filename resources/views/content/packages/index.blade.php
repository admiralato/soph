@extends('layouts.master')
@section('title')
    BMBOC Admin Panel :: {{ isset($pageTitle) ? $pageTitle : 'Packages' }}
@stop
@section('content')
    @include('include.content_header_block')
    <section class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="box box-danger">
                    <div class="box-header with-border">
                        <a href="packages/create" class="btn btn-sm btn-info btn-flat pull-left"><i class="fa fa-plus"></i> Create New Package</a>
                    </div>
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover">
                            <tr>
                                <th style="width:100px;">Action</th>
                                <th>Packages</th>
                                <th>Amount</th>
                                <th>15 days</th>
                                <th>15 days</th>
                                <th>15 days</th>
                                <th>Total</th>
                            </tr>
                            @foreach ($package as $pack)
                                <tr>
                                    <td>
                                        <a href="packages/{{ $pack->id }}" title="View"><i class="fa fa-eye fa-fw"></i></a>
                                        <a href="packages/{{ $pack->id }}/edit" title="Edit"><i class="fa fa-edit fa-fw"></i></a>
                                        <a href="packages/{{ $pack->id }}/edit" title="Delete"><i class="fa fa-trash fa-fw"></i></a>
                                    </td>
                                    <td><i class="fa fa-dropbox"></i> Package {{ $pack->title }}</td>
                                    <td><span class="label bg-{{ $pack->color }}" style="font-size:1em;">{{ number_format($pack->amount, 2) }}</span></td>
                                    <td>{{ number_format($pack->first_fifteen, 2) }}</td>
                                    <td>{{ number_format($pack->second_fifteen, 2) }}</td>
                                    <td>{{ number_format($pack->third_fifteen, 2) }}</td>
                                    <td>{{ number_format($pack->first_fifteen + $pack->second_fifteen + $pack->third_fifteen, 2) }}</td>
                                </tr>
                            @endforeach
                        </table>
                    </div><!-- /.box-body -->
                </div>
            </div>
        </div>

       

    </section>
	

@stop