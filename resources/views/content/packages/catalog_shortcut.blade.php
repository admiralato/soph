<div class="row">
	<div class="col-lg-12">
		<strong class="margin-r-5"><i class="fa fa-user margin-r-5"></i> Product Catalog</strong><span id="warning" class="bg-red" style="padding:4px;display:none"></span>
		<p>
			<div id="product-info" class="callout callout-success" style="display:none">
				<p>No selected package</p>
			</div>
		</p>
	</div>
  	
	<div class="col-lg-12">
		<p>
			<div class="input-group input-group-md">
				{!! Form::number('packageno', null, ['id'=>'packageno', 'placeholder'=>'Enter Package Number', 'class'=>'form-control']) !!}
				<span class="input-group-btn">
					<button type="button" id="add-to-cart" class="btn btn-info btn-flat">Add to Cart</button>
				</span>
			</div>
		</p>
	</div>
	<div class="col-lg-12">
		<hr>
		<strong><i class="fa fa-dropbox fa-fw"></i> Packages</strong>
  		<p id="products">
  			@if($packages && count($packages) > 0)
  				@foreach($packages as $item)
  					<a class="btn btn-xs btn-default" style="margin:5px 5px 5px 0;" id="{{ $item->id }}" data-id="{{ $item->title }}" data-color="{{ $item->color }}" data-amount="{{ $item->amount }}"><span class="label bg-{{ $item->color }}">P{{ $item->title }}</span> {{ $item->amount }}</a>
  				@endforeach
	    	@endif
	  	</p>
	  	<hr>
  	</div>
</div>