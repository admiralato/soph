<div class="box-body table-responsive no-padding">
  {!! Form::hidden('packages', null) !!}
  <table id="cart-items" class="table table-hover">
    <thead>
      <tr>
        <th></th>
        <th>Item</th>
        <th>Sub Total</th>
      </tr>
    </thead>
    <tbody>
    </tbody>
  </table>
</div>
<div class="box-footer table-responsive no-padding">
  <table id="cart-date" class="table">
    <tbody>
      <tr>
        <td><div style="margin-top:6px;">Purchase Date</div></td>
        <td>
          {!! Form::text('created_at', null, ['class'=>'form-control', 'data-mask'=>'', 'placeholder'=>'YYYY-MM-DD']) !!}
        </td>
      </tr>
    </tbody>
  </table>
  <table id="cart-total" class="table">
    <tbody>
      <tr>
        <td>TOTAL</td>
        <td><span id="grand-total">0</span></td>
      </tr>
      <tr>
        <td colspan="2">
          {!! Form::submit('Place Order',['id'=>'place-order','name'=>'create', 'class'=>'btn btn-success btn-lg pull-right disabled']) !!}
        </td>
      </tr>
    </tbody>
  </table>

</div>