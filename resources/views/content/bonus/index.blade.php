<style>
    td.details-control {
        background: url('../resources/assets/static/images/details_open.png') no-repeat center center;
        cursor: pointer;
    }
    tr.shown td.details-control {
        background: url('../resources/assets/static/images/details_close.png') no-repeat center center;
    }
</style>
@extends('layouts.master')
@section('title')
    BMBOC Admin Panel :: {{ isset($pageTitle) ? $pageTitle : 'Unilevel Bonus' }}
@stop
{!! Html::style('resources/assets/css/jquery.dataTables.min.css') !!}

@section('content')
    @include('include.content_header_block')
    <section class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-search"></i> Search</h3>
                        <h3 id="status" style="margin-left:5px;" class="box-title label label-success pull-right">ACTIVE</h3>
                        <h3 id="coverage" class="box-title label label-primary pull-right"></h3>
                    </div>  
                    
                    <div class="box-body">
                        {!! Form::open(['url'=>'bonus/release','method'=>'POST', 'role'=>'form']) !!}
                    	{!! Form::hidden('member_id', $memberId) !!}
                    	{!! Form::hidden('profile_id', null) !!}
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-search"></i>
                                        </div>
                                        {!! Form::text('member_profile', null, ['placeholder'=>'Search member name', 'class'=>'form-control', 'onkeyup'=>'up(this)', 'onkeydown'=>'down(this)', 'autocomplete'=>'off']) !!}
                                    </div>
                                    <ul id="member-profile" class="dropdown-menu" style="top:33px;">
                                    </ul>
                                </div>
                            </div>
                            <div class="col-lg-8">
                                <div class="form-group">
                                    {!! Form::label('totalbonus', 'Grand Total: 0.00 PHP', ['class'=>'form-control', 'id'=>'totalbonus']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <h3 class="box-title">Incentives <img src={{ url("resources/assets/static/images/ajax-loader.gif") }} id="loading-indicator" style="" /></h3>
                                <div class="box-body">
                                    <table id="incentives-content" class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th>Level</th>
                                                <th>Products</th>
                                                <th>Incentive</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            
                                        </tfoot>
                                        <tbody>
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        {!! Form::submit('Release Incentives',['name'=>'create', 'id'=>'create', 'class'=>'btn btn-primary  btn-sm pull-right']) !!}
                        <a href="{{ URL::previous() }}" class="btn btn-success btn-sm">Back</a>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </section>
@stop
@section('jsaddon')
<meta name="_token" content="{{ app('Illuminate\Encryption\Encrypter')->encrypt(csrf_token()) }}" />
{!! Html::script('resources/assets/js/jquery.json.js') !!}
{!! Html::script('resources/assets/plugins/datatables/jquery.dataTables.min.js') !!}
{!! Html::script('resources/assets/plugins/datatables/dataTables.bootstrap.min.js') !!}
<script type="text/javascript">
    var coverageFrom = null;
    var coverageTo = null;
    var salesids = null;
    var grandTotal = 0;
    var dataSet = [];
    var sales_ids = null;
    
	$(function() 
    {
        var oTable = $('#incentives-content').dataTable({
            data: dataSet,
            columns: [
                {
                    "className":    'details-control',
                    "orderable":    false,
                    "data":          null,
                    "defaultContent": ''
                },
                { data: "level" },
                { data: "products" },
                { data: "incentive" }
            ],
            bLengthChange: false,
            filter: false,
            info: false,
            ordering: false,
            processing: true,
            retrieve: true
        });
        
        // Add event listener for opening and closing details
        $('#incentives-content tbody').on('click', 'td.details-control', function () {
            var tr = $(this).closest('tr');
            var row = oTable.api().row( tr );

            if ( row.child.isShown() ) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            }
            else {
                // Open this row
                row.child( format(row.data()) ).show();
                tr.addClass('shown');
            }
        } );
        
        $('#create').on('click', function(e) {
            e.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-XSRF-Token': $('meta[name="_token"]').attr('content')
                }
            });
            
            if($('#create').hasClass('disabled')) return;
            
            if(grandTotal == 0) {
                alert("There is nothing to release!");
                return;
            }
            
            if(parseInt(grandTotal.replace(/,/g, ''), 10) > 0) {
                save();
            } else {
                alert("There is nothing to release!");
                return;
            }
        });
        
        var getUrlParameter = function getUrlParameter(sParam) {
            var sPageURL = decodeURIComponent(window.location.search.substring(1)),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');

                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : sParameterName[1];
                }
            }
        };
        
        var newMemberId =  getUrlParameter('id');
        var memberParam = "";
        
        if (newMemberId && parseInt(newMemberId)) {
            memberParam = "?id=" +newMemberId;
        }
        
        $.ajax({
            type: "GET",
            url: "{{ url('bonus/member/incentive') }}" + memberParam,
            dataType:"json",
            success: function( data ) {
                var status = data.status;
                dataSet = JSON.parse(data.dataSet);
                oTable.fnAddData(dataSet);
                oTable.fnDraw();

                grandTotal = data.grandTotal;
                sales_ids = data.sales_ids;
                if(data.coverageFrom == "" || data.coverageFrom == null) {
                    $('#coverage').html('First-Time Release');
                } else {
                    coverageFrom = data.coverageFrom;
                    coverageTo = data.coverageTo;
                    $('#coverage').html(data.coverageFrom + ' to ' + data.coverageTo);
                }
                $('#totalbonus').text('Grand Total: ' + data.grandTotal + ' PHP');
                $('input[name="member_profile"]').val(data.memberName);
                if(status) {
                    $('#status').addClass('label-success').removeClass('label-danger');
                    $('#status').html('ACTIVE');
                    $('#create').removeClass('disabled');
                } else {
                    $('#status').addClass('label-danger').removeClass('label-success');
                    $('#status').html('INACTIVE');
                    $('#create').addClass('disabled');
                }
                $('#loading-indicator').hide();
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                grandTotal = 0;
                $('#loading-indicator').hide();
            }
        });
        

        $('#member-profile').css('top','83px !important');
        $('#member-profile').css('left','10px !important');
    });

	var keyTimer;

	function up(obj) 
    {
		keyTimer = setTimeout(function() {
			var keywords = $(obj).val();
			keywords = encodeURI(keywords);
			if (keywords.length > 1){
				$.get("{{ url('bonus/search/profilename') }}?q=" + keywords + "&member=1", function(data){
					if (data.length > 0) {
						$('#member-profile').html(data);
						$('#member-profile').dropdown().toggle();	
					}
				});
			}
		}, 400);
	}

	function down(obj)
    {
		clearTimeout(keyTimer);
	}
    
	function loadUrl(url)
    {
		window.location.href = url;
		return false;
	}

	function selectMember(id, parentid, profileid, name)
    {
		$('input[name="profile_id"]').val(profileid);
		$('input[name="member_id"]').val(id);
		$('input[name="member_profile"]').val(name);
		$('#member-profile').dropdown().toggle();
        $('#loading-indicator').show();
	}

    function storeTblValues()
    {
        var TableData = new Array();

        $('#incentives-content tr').each(function(row, tr){
            TableData[row]={
                "level" : $(tr).find('td:eq(1)').text()
                , "productTotal" :$(tr).find('td:eq(2)').text()
                , "incentive" : $(tr).find('td:eq(3)').text()
            }    
        }); 
        TableData.shift();  // next row will be the footer - remove as well
        return TableData;
    }
    
    function save()
    {
        var TableData;
        TableData = $.toJSON(storeTblValues());
        
        var memberID = $('input[name="member_id"]').val();
        
        $.ajax({
            type: "POST",
            url: "{{ url('bonus/release') }}",
            dataType:"json",
            data: { 
                pTableData: TableData, 
                pMemberId: memberID, 
                pTotal: grandTotal, 
                pCoverageFrom: coverageFrom, 
                pCoverageTo: coverageTo,
                pSalesIds: sales_ids
            },
            success: function(data) {
                alert(data.message);
                loadUrl("{{ url('bonus') }}");
            }
        });
    }
    
    /* Formatting function for row details - modify as you need */
    function format ( member ) 
    {
        var detail = member.detail;
        var detailCount = detail.length;
        var htmlString = "";
        for(var i = 0; i < detailCount; i++) {
            var incentiveDate = detail[i].lastIncentiveDate;
            if(detail[i].lastIncentiveDate == "") {
                incentiveDate = null;
            } else {
                incentiveDate = incentiveDate.replace(/"([^"]+(?="))"/g, '$1');
            }
            htmlString = htmlString + "<tr><td><a href='javascript:void(0)' onclick=\"viewSalesDetails('" + detail[i].memberid + "','" + incentiveDate + "'); return false;\">" + detail[i].name + "</a></td><td>" + detail[i].purchases + "</td></tr>";
        }
        
        // `d` is the original data object for the row
        return '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">'+
                htmlString
        '</table>';
    }
    
    function viewSalesDetails(memberid, lastIncentiveDate) 
    {
        // TODO: Display modal form here to view details
        $.ajax({
            type: "POST",
            url: "{{ url('bonus/sales_details') }}",
            dataType:"json",
            data: { pMemberId: memberid, pDate: lastIncentiveDate, "_token": "{{ csrf_token() }}" },
            success: function(data) {
                $('#sales-details tbody tr:not(:first-child)').remove();
                $('#sales-details tbody').append(data.data);
            }
        });
        $('#view-details').modal('show');
    }
    
</script>
@stop

<div id="view-details" class="modal modal-primary fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Sales Details</h4>
            </div>
            <div class="modal-body">
                <div class="box box-danger">
                <div class="box-header">
                  <h3 class="box-title">Purchases</h3>
                </div><!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                  <table id="sales-details" class="table table-striped table-hover">
                    <tbody><tr>
                      <th>Purchased On</th>
                      <th>Package</th>
                      <th>Amount</th>
                    </tr>
                    
                  </tbody></table>
                </div><!-- /.box-body -->
              </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>