<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!--    <meta http-equiv="X-UA-Compatible" content="IE=edge">-->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="description" content="MKGT DASHBOARD">
    <meta name="author" content="DEQUODE">
    <title>Export to Excel</title>
    
    {!! Html::style('resources/assets/bootstrap/css/bootstrap.min.css') !!}
    {!! Html::style('resources/assets/skin/default/css/font-awesome.min.css') !!}
    {!! Html::style('resources/assets/skin/default/css/AdminLTE.min.css') !!}
    {!! Html::style('resources/assets/skin/default/css/skin-purple.min.css') !!}
    {!! Html::style('resources/assets/skin/default/css/font-awesome.min.css') !!}
    {!! Html::style('resources/assets/skin/default/css/custom-styles.css') !!}

    @yield('css')
</head>
<body onload="window.print();">
    <div class="wrapper">
        <!-- Main content -->
        <section class="invoice">
            {!! $html !!}
        </section>
    </div>
    <!-- App Menu Plugin JavaScript -->
    {!! Html::script('resources/assets/skin/default/js/app.min.js') !!}
    @yield('jsaddon')
</body>
</html>
