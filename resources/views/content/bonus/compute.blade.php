{!! Html::style('resources/assets/plugins/daterangepicker/daterangepicker-bs3.css') !!}

@extends('layouts.master')
@section('title')
    BMBOC Admin Panel :: {{ isset($pageTitle) ? $pageTitle : 'Compute Incentives' }}
@stop

@section('content')
    @include('include.content_header_block')
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-info">
                <div class="box-header with-border">
                  <h3 class="box-title">Select Member</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    {!! Form::open(['url'=>'member/updateSponsor','method'=>'POST', 'role'=>'form']) !!}
                    {!! Form::hidden('member_id', null) !!}
                    
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>Search:</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-search"></i>
                                    </div>
                                    {!! Form::text('member_profile', null, ['placeholder'=>'Search member name', 'class'=>'form-control', 'onkeyup'=>'up(this)', 'onkeydown'=>'down(this)', 'autocomplete'=>'off']) !!}
                                </div>
                                <ul id="member-profile" class="dropdown-menu" style="top:57px;">
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Date range:</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control pull-right active" id="coverage">
                                </div><!-- /.input group -->
                            </div>
                        </div>
                    </div>
                </div><!-- /.box-body -->
                <div class="box-footer clearfix">
                    <a href="javascript:void(0)" onclick="searchDownlines(); return false;" class="btn btn-default pull-right"><i class="fa fa-search"></i> Search</a>
                    <a href="{{ URL::previous() }}" class="btn btn-success btn-normal">Back</a>
                </div><!-- /.box-footer -->
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    <section id="loader"><img src="{!! url('resources/assets/static/images/loading.gif') !!}" style="top:45%; left:60%; position:absolute;"/></section>
    <section id="preview" class="invoice">
        <!-- title row -->
        <div class="row">
            <div class="col-xs-12">
              <h2 class="page-header">
                <i class="fa fa-globe"></i> BMBOC
                <small class="pull-right" id="status"></small>
                <small class="pull-right" style="margin-right: 10px;">Coverage: <span id="from"></span>-<span id="to"></span></small>
              </h2>
            </div><!-- /.col -->
        </div>
        <!-- info row -->
        <div class="row invoice-info">
            <div class="col-sm-4 invoice-col">
              Account:
              <address>
                <strong><span id="member-name"></span></strong><br>
                  <span id="member-address-primary"></span><br>
                Phone: <span id="member-contactno"></span><br>
                Registration: <span id="member-registration"></span>
              </address>
            </div><!-- /.col -->
            <div class="col-sm-4 invoice-col">
              Sponsor:
              <address>
                <strong><span id="sponsor-name"></span></strong><br>
                  <span id="sponsor-address-primary"></span><br>
                Phone: <span id="sponsor-contactno"></span><br>
                Registration: <span id="sponsor-registration"></span>
              </address>
            </div><!-- /.col -->
        </div><!-- /.row -->

        <!-- Table row -->
        <div class="row">
            <div class="col-xs-12 table-responsive">
                <table id="member-downlines" class="table table-striped">
                    <thead>
                      <tr>
                        <th>Downline Name</th>
                        <th>Purchase Date</th>
                        <th>Package</th>
                        <th>Price</th>
                        <th>Total</th>
                        <th>Status</th>
                      </tr>
                    </thead>
                    <tbody>
                    
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="5"><strong>TOTAL AMOUNT TO BE RELEASED</strong></td>
                            <td id="total-incentives" style="text-align:right;font-weight:bold;">0.00 Php</td>
                        </tr>
                    </tfoot>
              </table>
            </div><!-- /.col -->
        </div><!-- /.row -->

        <div class="row">
            <!-- accepted payments column -->
            <div class="col-xs-4">
                <p>Generated By: {{ Auth::user()->name }}</p>
            </div><!-- /.col -->
            <div class="col-xs-4">
                <p></p>
            </div><!-- /.col -->
            <div class="col-xs-4">
                <p class="pull-right">Date/Time: {{ Carbon\Carbon::now() }}</p>
            </div>
        </div><!-- /.row -->

        <!-- this row will not appear when printing -->
        <div id="print-area" class="row no-print">
            <div class="col-xs-12">
<!--                <a href="javascript:void(0);" onclick="preview(); return false;" target="_blank" class="btn btn-default"><i class="fa fa-print"></i> Print</a>-->
                <a href="javascript:void(0);" onclick="release(); return false;" class="btn btn-success pull-right"><i class="fa fa-check-circle"></i> Release Incentives</a>
<!--                <button class="btn btn-primary pull-right" style="margin-right: 5px;"><i class="fa fa-download"></i> Generate PDF</button>-->
            </div>
        </div>
        {!! Form::open(['url'=>'bonus/print/referral-claim','method'=>'POST', 'role'=>'form', 'id'=>'printable', 'target'=>'_blank']) !!}
        {!! Form::hidden('html_data', null) !!}
        {!! Form::close() !!}
    </section>
</section>

@stop
@section('jsaddon')
{!! Html::script('resources/assets/plugins/daterangepicker/moment.min.js') !!}
{!! Html::script('resources/assets/plugins/daterangepicker/daterangepicker.js') !!}
<script type="text/javascript">
    var count = 2; 
    var level = 1;
    var maxLevel = 10;
    var totalIncentives = 0;
    var parentIds = null;
    var processedSales = '';
    var salesIds = null;
    var memberIsActive = true;
    
    $(function () {
        //Date range picker
        $('#coverage').daterangepicker();
        $('#loader').hide();
        $('#loading-level').hide();
        $('#print-area').hide();
        
//        $(window).scroll(function(){
//            if($('#preview').is(':visible') && $(window).scrollTop() + screen.height > $('#preview').height() + 100) {
//                // Load another level of incentives
//                var dates = $('#coverage').val();
//                var coverage = null;
//                if(dates != null && dates != "") {
//                    coverage = dates.split('-');
//                }
//                if(coverage != null && level <= maxLevel)
//                {
//                    level++;
//                    $('#loading-level').show();
//                    $.ajax({
//                        type: "POST",
//                        async: false,
//                        url: "{{ url('/bonus/get/remaining-purchases') }}",
//                        dataType:"json",
//                        data: { 
//                            pSalesFrom: coverage[0],
//                            pSalesTo: coverage[1],
//                            pLevel: level,
//                            pParents: parentIds,
//                            pProcessedSales: processedSales,
//                            "_token": "{{ csrf_token() }}"
//                        },
//                        success: function(data) {
//                            parentIds = null;
//                            level = data.level;
//                            totalIncentives += data.totalIncentives;
//                            parentIds = data.parents;
//                            console.log('data: ' + data.sales_ids);
//                            if(data.sales_ids && salesIds) {
//                                salesIds += ',' + data.sales_ids;
//                            } else {
//                                salesIds += data.sales_ids;
//                            }
//                            //if(data.sales_ids) { 
//                                //salesIds += ',' + data.sales_ids; // orig
//                            //} 
//                        
//                            processedSales = data.processedSales;
//                            console.log('while scrolling:' + salesIds);
//                            
//                            $('#level'+level).after(data.html);
//                            $('#loading-level'+level).remove();
//                            $('#total-incentives').text(replaceNumberWithCommas(totalIncentives) + ' PHP');
//                            $('#loading-level').hide();
//                            if(level == maxLevel) {
//                                $('#loading-level').hide();
//                                $('#print-area').show();
//                            }
//                        }
//                    });
//                }
//            }
//        });
        
        var getUrlParameter = function getUrlParameter(sParam) {
            var sPageURL = decodeURIComponent(window.location.search.substring(1)),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');

                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : sParameterName[1];
                }
            }
        };

        var newMemberId =  getUrlParameter('id');
        var memberParam = "";

        if (newMemberId && parseInt(newMemberId)) {
            memberParam = "?id=" +newMemberId;
        }

        $('#member-profile').css('top','83px !important');
        $('#member-profile').css('left','10px !important');
        
        $('#preview').hide();
        $('#loader').hide();
    });
    
    var keyTimer;
    
    function up(obj) 
    {
        keyTimer = setTimeout(function() {
            var keywords = $(obj).val();
            keywords = encodeURI(keywords);
            if (keywords.length > 1){
                $.get("{{ url('member/search/profilename') }}?q=" + keywords + "&member=1", function(data){
                    if (data.length > 0) {
                        $('#member-profile').html(data);
                        $('#member-profile').dropdown().toggle();	
                    }
                });
            }
        }, 400);
    }

    function down(obj)
    {
        clearTimeout(keyTimer);
    }

    function selectMember(id, name)
    {
        $('input[name="member_id"]').val(id);
        $('input[name="member_profile"]').val(name);
        $('#member-profile').dropdown().toggle();
    }
    
    function getDocHeight() 
    {
        var D = document;
        return Math.max(
            D.body.scrollHeight, D.documentElement.scrollHeight,
            D.body.offsetHeight, D.documentElement.offsetHeight,
            D.body.clientHeight, D.documentElement.clientHeight
        );
    }
    
    var coverage_from = null;
    var coverate_to = null;
    var member_name = null;
    var member_address = null;
    var member_contact = null;
    var member_registration = null;
    var table_data = null;
    
    function request(unilevel) { 
        // Load another level of incentives
        var dates = $('#coverage').val();
        var coverage = null;
        if(dates != null && dates != "") {
            coverage = dates.split('-');
        }
        $('#loading-level').show();
        
        // return the AJAX promise
        return $.ajax({
            type: "POST",
            url: "{{ url('/bonus/get/remaining-purchases') }}",
            dataType:"json",
            data: { 
                pSalesFrom: coverage[0],
                pSalesTo: coverage[1],
                pLevel: unilevel,
                pParents: parentIds,
                pProcessedSales: processedSales,
                "_token": "{{ csrf_token() }}"
            }
        });
    }
    
    function requestUnilevels(page, items) {
        return request(page).then(function(data){
            if (parseInt(data.level) > maxLevel) {
                return items;
            } else {
                if(data.sales_ids && salesIds) {
                    salesIds += ',' + data.sales_ids;
                } else {
                    salesIds += data.sales_ids;
                }
                parentIds = data.parents;
                totalIncentives += data.totalIncentives;
                processedSales = data.processedSales;
                level = data.level;
                $('#loading-level').hide();
                
                $('#level'+level).after(data.html);
                $('#loading-level'+level).remove();
                $('#total-incentives').text(replaceNumberWithCommas(totalIncentives) + ' PHP');
                return requestUnilevels(parseInt(data.level) + 1, processedSales);
            }
        });
    }
    
    function requestAll(){
        return requestUnilevels(2, processedSales);
    }

    function searchDownlines()
    {
        $('#preview').hide();
        level = 1;
        totalIncentives = 0;
        parentIds = null;
        processedSales = '';
        salesIds = null;
        
        var memberId = $('input[name="member_id"]').val();
        var dates = $('#coverage').val();
        var coverage = null;
        var rowHtml = '';
        var status = true;
        
        if(dates != null && dates != "") {
            coverage = dates.split('-');
        }
        if(coverage != null && memberId != "" && memberId != null)
        {
            $('#loader').show();
            $.ajax({
                type: "POST",
                url: "{{ url('bonus/get/referrals') }}",
                dataType:"json",
                data: { 
                    pSalesFrom: coverage[0],
                    pSalesTo: coverage[1],
                    pMemberId: memberId,
                    pParents: memberId,
                    pLevel: level,
                    "_token": "{{ csrf_token() }}"
                },
                success: function(data) {
                    coverage_from = data.from;
                    coverate_to = data.to;
                    member_name = data.member.first_name + ' ' + data.member.last_name;
                    member_address = data.member.address_primary;
                    member_contact = data.member.contact_no;
                    member_registration = data.member.registration_date;
                    table_data = data.html;
                    parentIds = data.parents;
                    status = data.status; 
                    memberIsActive = status;
                    processedSales = data.processedSales;
                    salesIds = data.sales_ids;
                    console.log('salesIds: ' + salesIds);
                    if(status) {
                        $('#status').html('<span class="label label-success">ACTIVE</span>');
                    } else {
                        $('#status').html('<span class="label label-danger">INACTIVE</span>');
                    }
                    
                    $('#member-downlines tbody tr').remove();
                    
                    maxLevel = data.maxLevel;
                    for(c = 1; c <= maxLevel; c++) {
                        rowHtml += '<tr id="level'+c+'">';
                        rowHtml += '<td colspan="6" style="font-weight:bold;">LEVEL '+c+'</td>';
                        rowHtml += '</tr>';
                        rowHtml += '<tr id="loading-level'+c+'"><td colspan="6" style="text-align:center;"><img id="loading-level" src="{!! url("resources/assets/static/images/loader_1.gif") !!}" /></td></tr>';
                    }
                    
                    totalIncentives = data.totalIncentives;
                    $('#member-downlines tbody').append(rowHtml);
                    $('#total-incentives').text(replaceNumberWithCommas(totalIncentives) + ' PHP');
                    
                    $('#from').text(data.from);
                    $('#to').text(data.to);
                    $('#member-name').text(data.member.first_name + ' ' + data.member.last_name);
                    $('#member-address-primary').text(data.member.address_primary);
                    $('#member-contactno').text(data.member.contact_no);
                    $('#member-registration').text(data.member.registration_date);
                    
                    // Sponsor
                    if(data.sponsor != null) {
                        $('#sponsor-name').text(data.sponsor.first_name + ' ' + data.sponsor.last_name);
                        $('#sponsor-address-primary').text(data.sponsor.address_primary);
                        $('#sponsor-contactno').text(data.sponsor.contact_no);
                        $('#sponsor-registration').text(data.sponsor.registration_date);
                    }
                    
                    $('#level'+level).after(data.html);
                    $('#loading-level'+level).remove();
                    $('#preview').show();
                    $('#loader').hide();
                    
                    requestAll().done(function(items) {
                        console.dir(items);
                        $('#loading-level').hide();
                        $('#print-area').show();
                    });
                }
            });
        } else {
            alert('Please select member and date of coverage!');
        }
    }
    
    function preview()
    {
        var html = $('#preview').html();
        $('input[name="html_data"]').val(html);
        $('#printable').submit();
    }
    
    function replaceNumberWithCommas(yourNumber) 
    {
        //Seperates the components of the number
        var n= yourNumber.toString().split(".");
        //Comma-fies the first part
        n[0] = n[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        //Combines the two sections
        return n.join(".");
    }
    
    function release()
    {
        // member_id, grand_total, released_by_id, sales_ids, from, to
        var member_id = $('input[name="member_id"]').val();
        var grand_total = totalIncentives;
        var sales_ids = salesIds;
        var from = coverage_from;
        var to = coverate_to;
        
//        if(!memberIsActive) {
//            alert('Member is currently inactive!');
//            return;
//        }
        
        if(totalIncentives > 0) {
            $.ajax({
                type: "POST",
                url: "{{ url('bonus/release') }}",
                dataType:"json",
                data: { 
                    pMemberId: member_id, 
                    pTotal: grand_total, 
                    pCoverageFrom: from, 
                    pCoverageTo: to,
                    pSalesIds: salesIds,
                    "_token": "{{ csrf_token() }}"
                },
                success: function(data) {
                    alert(data.message);
                    loadUrl("{{ url('bonus') }}");
                }
            });
        } else {
            alert("There is nothing to release!");
            return;
        }
    }
    
    function loadUrl(url)
    {
		window.location.href = url;
		return false;
	}
</script>
@stop