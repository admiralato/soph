{!! Html::style('resources/assets/plugins/daterangepicker/daterangepicker-bs3.css') !!}

@extends('layouts.master')
@section('title')
    BMBOC Admin Panel :: {{ isset($pageTitle) ? $pageTitle : 'Referral Claims' }}
@stop

@section('content')
    @include('include.content_header_block')
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-info">
                <div class="box-header with-border">
                  <h3 class="box-title">Select Member</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    {!! Form::open(['url'=>'member/updateSponsor','method'=>'POST', 'role'=>'form']) !!}
                    {!! Form::hidden('member_id', null) !!}
                    
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>Search:</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-search"></i>
                                    </div>
                                    {!! Form::text('member_profile', null, ['placeholder'=>'Search member name', 'class'=>'form-control', 'onkeyup'=>'up(this)', 'onkeydown'=>'down(this)', 'autocomplete'=>'off']) !!}
                                </div>
                                <ul id="member-profile" class="dropdown-menu" style="top:57px;">
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Date range:</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control pull-right active" id="coverage">
                                </div><!-- /.input group -->
                            </div>
                        </div>
                    </div>
                </div><!-- /.box-body -->
                <div class="box-footer clearfix">
                    <a href="javascript:void(0)" onclick="searchDownlines(); return false;" class="btn btn-default pull-right"><i class="fa fa-search"></i> Search</a>
                    <a href="{{ URL::previous() }}" class="btn btn-success btn-normal">Back</a>
                </div><!-- /.box-footer -->
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    <section id="loader"><img src="{!! url('resources/assets/static/images/loading.gif') !!}" style="top:45%; left:60%; position:absolute;"/></section>
    <section id="preview" class="invoice" style="font-size:12px;">
        <!-- title row -->
        <div class="row">
            <div class="col-xs-12">
              <h2 class="page-header">
                <i class="fa fa-globe"></i> BMBOC
                <small class="pull-right">Coverage: <span id="from"></span>-<span id="to"></span></small>
              </h2>
            </div><!-- /.col -->
        </div>
        <!-- info row -->
        <div class="row invoice-info">
            <div class="col-sm-4 invoice-col">
              Account:
              <address>
                <strong><span id="member-name"></span></strong><br>
                  <span id="member-address-primary"></span><br>
                Phone: <span id="member-contactno"></span><br>
                Registration: <span id="member-registration"></span><br>
                TIN: <span id="member-tin"></span><br>
              </address>
            </div><!-- /.col -->
            <div class="col-sm-4 invoice-col">
              Sponsor:
              <address>
                <strong><span id="sponsor-name"></span></strong><br>
                  <span id="sponsor-address-primary"></span><br>
                Phone: <span id="sponsor-contactno"></span><br>
                Registration: <span id="sponsor-registration"></span>
              </address>
            </div><!-- /.col -->
        </div><!-- /.row -->

        <!-- Table row -->
        <div class="row">
            <div class="col-xs-12 table-responsive">
                <table id="member-downlines" class="table table-striped" style="font-size:12px;">
                    <thead>
                      <tr>
                        <th>Downline Name</th>
                        <th>Purchase Date</th>
                        <th>Package</th>
                        <th>Amount</th>
                        <th>Less 10&#37; Tax</th>
                        <th>Total</th>
                      </tr>
                    </thead>
                    <tbody>
                    
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="5"><strong>TOTAL</strong></td>
                            <td id="total-incentives" style="text-align:right;font-weight:bold;">0.00 Php</td>
                        </tr>
                    </tfoot>
              </table>
            </div><!-- /.col -->
        </div><!-- /.row -->

        <div class="row">
            <!-- accepted payments column -->
            <div class="col-xs-4">
                <p>Generated By: {{ Auth::user()->name }}</p>
            </div><!-- /.col -->
            <div class="col-xs-4">
                <p></p>
            </div><!-- /.col -->
            <div class="col-xs-4">
                <p class="pull-right">Date/Time: {{ Carbon\Carbon::now() }}</p>
            </div>
        </div><!-- /.row -->

        <!-- this row will not appear when printing -->
        <div id="print-area" class="row no-print">
            <div class="col-xs-12">
                <a href="javascript:void(0);" onclick="preview(); return false;" target="_blank" class="btn btn-default"><i class="fa fa-print"></i> Print</a>

                <a id="btnExport" href="javascript:void(0);" onclick="exportToExcel(); return false;" target="_blank" class="btn btn-success pull-right"><i class="fa fa-file-excel-o"></i> Export to Excel</a>
<!--                <button class="btn btn-primary pull-right" style="margin-right: 5px;"><i class="fa fa-download"></i> Generate PDF</button>-->
            </div>
        </div>
        {!! Form::open(['url'=>'bonus/print/referral-claim','method'=>'POST', 'role'=>'form', 'id'=>'printable', 'target'=>'_blank']) !!}
        {!! Form::hidden('html_data', null) !!}
        {!! Form::close() !!}
        
        {!! Form::open(['url'=>'bonus/export/referral-claim','method'=>'POST', 'role'=>'form', 'id'=>'exportable', 'target'=>'_blank']) !!}
        {!! Form::hidden('excel_data', null) !!}
        {!! Form::close() !!}
    </section>
</section>

@stop
@section('jsaddon')
{!! Html::script('resources/assets/plugins/daterangepicker/moment.min.js') !!}
{!! Html::script('resources/assets/plugins/daterangepicker/daterangepicker.js') !!}
<script type="text/javascript">
    var count = 2; 
    var level = 1;
    var maxLevel = 10;
    var totalIncentives = 0;
    var parentIds = null;
    
    $(function () {
        //Date range picker
        $('#coverage').daterangepicker();
        $('#loader').hide();
        $('#loading-level').hide();
        $('#print-area').hide();
        
        var getUrlParameter = function getUrlParameter(sParam) {
            var sPageURL = decodeURIComponent(window.location.search.substring(1)),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');

                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : sParameterName[1];
                }
            }
        };

        var newMemberId =  getUrlParameter('id');
        var memberParam = "";

        if (newMemberId && parseInt(newMemberId)) {
            memberParam = "?id=" +newMemberId;
        }

        $('#member-profile').css('top','83px !important');
        $('#member-profile').css('left','10px !important');
        
        $('#preview').hide();
        $('#loader').hide();
    });
    
    var keyTimer;
    
    function up(obj) 
    {
        keyTimer = setTimeout(function() {
            var keywords = $(obj).val();
            keywords = encodeURI(keywords);
            if (keywords.length > 1){
                $.get("{{ url('member/search/profilename') }}?q=" + keywords + "&member=1", function(data){
                    if (data.length > 0) {
                        $('#member-profile').html(data);
                        $('#member-profile').dropdown().toggle();	
                    }
                });
            }
        }, 400);
    }

    function down(obj)
    {
        clearTimeout(keyTimer);
    }

    function selectMember(id, name)
    {
        $('input[name="member_id"]').val(id);
        $('input[name="member_profile"]').val(name);
        $('#member-profile').dropdown().toggle();
    }
    
    function getDocHeight() 
    {
        var D = document;
        return Math.max(
            D.body.scrollHeight, D.documentElement.scrollHeight,
            D.body.offsetHeight, D.documentElement.offsetHeight,
            D.body.clientHeight, D.documentElement.clientHeight
        );
    }
    
    var coverage_from = null;
    var coverate_to = null;
    var member_name = null;
    var member_address = null;
    var member_contact = null;
    var member_registration = null;
    var table_data = null;
    
    function searchDownlines()
    {
        $('#preview').hide();
        $('#loader').show();
        level = 1;
        totalIncentives = 0;
        parentIds = null;
        
        var memberId = $('input[name="member_id"]').val();
        var dates = $('#coverage').val();
        var coverage = null;
        var rowHtml = '';
        
        if(dates != null && dates != "") {
            coverage = dates.split('-');
        }
        if(coverage != null && memberId != "" && memberId != null)
        {
            $.ajax({
                type: "POST",
                url: "{{ url('bonus/search/referrals') }}",
                dataType:"json",
                data: { 
                    pSalesFrom: coverage[0],
                    pSalesTo: coverage[1],
                    pMemberId: memberId,
                    pParents: memberId,
                    pLevel: level,
                    "_token": "{{ csrf_token() }}"
                },
                success: function(data) {
                    coverage_from = data.from;
                    coverate_to = data.to;
                    member_name = data.member.first_name + ' ' + data.member.last_name;
                    member_address = data.member.address_primary;
                    member_contact = data.member.contact_no;
                    member_registration = data.member.registration_date;
                    table_data = data.html;
                    parentIds = data.parents;
                    
                    $('#member-downlines tbody tr').remove();
                    
                    maxLevel = data.maxLevel;
                    for(c = 1; c <= maxLevel; c++) {
                        rowHtml += '<tr id="level'+c+'">';
                        rowHtml += '<td colspan="6" style="font-weight:bold;">LEVEL '+c+'</td>';
                        rowHtml += '</tr>';
                        rowHtml += '<tr id="loading-level'+c+'"><td colspan="6" style="text-align:center;"><img id="loading-level" src="{!! url("resources/assets/static/images/loader_1.gif") !!}" /></td></tr>';
                    }
                    
                    totalIncentives = data.totalIncentives;
                    $('#member-downlines tbody').append(rowHtml);
                    $('#total-incentives').text(replaceNumberWithCommas(totalIncentives) + ' PHP');
                    
                    $('#from').text(data.from);
                    $('#to').text(data.to);
                    $('#member-name').text(data.member.first_name + ' ' + data.member.last_name);
                    $('#member-address-primary').text(data.member.address_primary);
                    $('#member-contactno').text(data.member.contact_no);
                    $('#member-registration').text(data.member.registration_date);
                    $('#member-tin').text(data.member.tin);
                    
                    // Sponsor
                    $('#sponsor-name').text(data.sponsor.first_name + ' ' + data.sponsor.last_name);
                    $('#sponsor-address-primary').text(data.sponsor.address_primary);
                    $('#sponsor-contactno').text(data.sponsor.contact_no);
                    $('#sponsor-registration').text(data.sponsor.registration_date);
                    
                    $('#level'+level).after(data.html);
                    $('#loading-level'+level).remove();
                    $('#preview').show();
                    $('#loader').hide();
                    
                    requestAll().done(function(items) {
                        console.dir(items);
                        $('#loading-level').hide();
                        $('#print-area').show();
                    });
                }
            });
        } else {
            alert('Please select member and date of coverage!');
        }
    }
    
    function preview()
    {
        var html = $('#preview').html();
        $('input[name="html_data"]').val(html);
        $('#printable').submit();
    }
    
    function exportToExcel()
    {
        var html = $('input[name="member_id"]').val();
        $('input[name="excel_data"]').val(html);
        $('#exportable').submit();
    }
    
    function replaceNumberWithCommas(yourNumber) 
    {
        //Seperates the components of the number
        var n= yourNumber.toString().split(".");
        //Comma-fies the first part
        n[0] = n[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        //Combines the two sections
        return n.join(".");
    }
    
    function request(unilevel) { 
        // Load another level of incentives
        var dates = $('#coverage').val();
        var coverage = null;
        if(dates != null && dates != "") {
            coverage = dates.split('-');
        }
        $('#loading-level').show();
        
        // return the AJAX promise
        return $.ajax({
            type: "POST",
            url: "{{ url('bonus/search/remaining-level') }}",
            dataType:"json",
            data: { 
                pSalesFrom: coverage[0],
                pSalesTo: coverage[1],
                pLevel: unilevel,
                pParents: parentIds,
                "_token": "{{ csrf_token() }}"
            }
        });
    }
    
    function requestUnilevels(page) {
        return request(page).then(function(data){
            if (parseInt(data.level) > maxLevel) {
                return null;
            } else {
                parentIds = data.parents;
                totalIncentives += data.totalIncentives;
                level = data.level;
                $('#loading-level').hide();
                
                $('#level'+level).after(data.html);
                $('#loading-level'+level).remove();
                $('#total-incentives').text(replaceNumberWithCommas(totalIncentives) + ' PHP');
                $('#loading-level').hide();
                return requestUnilevels(parseInt(data.level) + 1);
            }
        });
    }
    
    function requestAll(){
        return requestUnilevels(2);
    }
</script>
@stop