@extends('layouts.master')
@section('title')
    BMBOC Admin Panel :: {{ isset($pageTitle) ? $pageTitle : 'Packages' }}
@stop
@section('content')
    @include('include.content_header_block')
    <section class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="box box-danger">
                    <div class="box-header with-border">
                        <a href="incentives/create" class="btn btn-sm btn-info btn-flat pull-left"><i class="fa fa-plus"></i> Create New Incentive</a>
                    </div>
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover">
                            <tr>
                                <th>Action</th>
                                <th>Level</th>
                                <th>Incentive Per Product</th>
                            </tr>
                            @foreach ($incentives as $incentive)
                                <tr>
                                    <td>
                                        <a href="incentives/{{ $incentive->id }}" title="View"><i class="fa fa-eye fa-fw"></i></a>
                                        <a href="incentives/{{ $incentive->id }}/edit" title="Edit"><i class="fa fa-edit fa-fw"></i></a>
                                        <a href="incentives/{{ $incentive->id }}/edit" title="Delete"><i class="fa fa-trash fa-fw"></i></a>
                                    </td>
                                    <td>{{ $incentive->level }}</td>
                                    <td>{{ number_format($incentive->incentive, 2) }}</td>
                                </tr>
                            @endforeach
                        </table>
                    </div><!-- /.box-body -->
                </div>
            </div>
        </div>
    </section>
@stop