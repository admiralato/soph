@extends('layouts.master')
@section('title')
    BMBOC Admin Panel :: {{ isset($pageTitle) ? $pageTitle : 'Incentives' }}
@stop
@section('content')
    @include('include.content_header_block')
<section class="content">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Incentives</h3>
                </div><!-- /.box-header -->
                @if ($errors->any())
                    <ul class="alert alert-danger" style="list-style:none">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                @endif
                {!! Form::open(['url'=>'incentives','method'=>'POST', 'role'=>'form']) !!}
                @include ('content.incentives.form')
                <div class="box-footer">
                    {!! Form::submit('Create',['name'=>'create', 'class'=>'btn btn-primary  btn-sm pull-right']) !!}
                    <a href="{{ Config::get('app.base_url') }}/incentives" class="btn btn-success btn-sm">Back</a>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</section>
@stop