@extends('layouts.master')
@section('title')
    BMBOC Admin Panel :: {{ isset($pageTitle) ? $pageTitle : 'Packages' }}
@stop
@section('content')
    @include('include.content_header_block')
<section class="content">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Incentives</h3>
                </div><!-- /.box-header -->
                @if ($errors->any())
                    <ul class="alert alert-danger" style="list-style:none">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                @endif
                <div class="box-body">
                    <div class="form-group">
                        {!! Form::label('level', 'Level') !!}
                        {!! Form::number('level', $item->level, ['id'=>'level', 'placeholder'=>'Member level', 'class'=>'form-control', 'disabled']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('incentive', 'Incentive per product') !!}
                        {!! Form::text('incentive', $item->incentive, ['id'=>'incentive', 'placeholder'=>'Enter amount', 'class'=>'form-control', 'disabled']) !!}
                    </div>
                </div><!-- /.box-body -->
                <div class="box-footer">
                    <a href="{{ URL::previous() }}" class="btn btn-success btn-sm">Back</a>
                </div>
            </div>
        </div>
    </div>
</section>
@stop