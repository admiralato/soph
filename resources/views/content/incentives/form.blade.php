<div class="box-body">
    <div class="form-group">
        {!! Form::label('level', 'Level') !!}
        {!! Form::number('level', $item->level, ['id'=>'level', 'placeholder'=>'Enter level', 'class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('incentive', 'Incentive per product') !!}
        {!! Form::text('incentive', $item->incentive, ['id'=>'incentive', 'placeholder'=>'Enter amount', 'class'=>'form-control']) !!}
    </div>
</div><!-- /.box-body -->