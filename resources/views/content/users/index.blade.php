@extends('layouts.master')
@section('title')
    BMBOC Admin Panel :: {{ isset($pageTitle) ? $pageTitle : 'User Accounts' }}
@stop
@section('content')
    @include('include.content_header_block')
    <section class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="box box-danger">
                    <div class="box-header with-border">
                        <a href="useraccounts/create" class="btn btn-sm btn-info btn-flat pull-left"><i class="fa fa-users"></i> Create New User</a>
                        <a href="{{ Config::get('app.base_url') }}/usertype" class="btn btn-success btn-flat btn-sm pull-right"><i class="fa fa-user"></i> User Type</a>
                        <a href="{{ Config::get('app.base_url') }}/userrole" class="btn btn-success btn-flat btn-sm pull-right" style="margin-right:10px;"><i class="fa fa-group"></i> User Role</a>
                    </div>
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover">
                            <tr>
                                <th>Action</th>
                                <th>User</th>
                                <th>Email</th>
                                <th>Type</th>
                                <th>Role</th>
                            </tr>
                            @foreach ($users as $item)
                                @if(strtolower($item->email) != 'admin@dequode.com')
                                    <tr>
                                        <td>
                                            <a href="useraccounts/{{ $item->id }}" title="View"><i class="fa fa-eye fa-fw"></i></a>
                                            <a href="useraccounts/{{ $item->id }}/edit" title="Edit"><i class="fa fa-edit fa-fw"></i></a>
                                            <a href="useraccounts/{{ $item->id }}/edit" title="Delete"><i class="fa fa-trash fa-fw"></i></a>
                                        </td>
                                        <td>{{ $item->name }}</td>
                                        <td>{{ $item->email }}</td>
                                        <td>{{ $item->usertype }}</td>
                                        <td>{{ $item->userrole }}</td>
                                    </tr>
                                @endif
                            @endforeach
                        </table>
                    </div><!-- /.box-body -->
                </div>
            </div>
        </div>
    </section>
@stop