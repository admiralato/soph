<div class="box-body">
    {!! Form::hidden('id', null ) !!}
    <div class="form-group">
        @if (Route::current()->getName()==='useraccounts.show')
            {!! Form::text('name', $user->name, ['class'=>'form-control', 'disabled'=>'disabled', 'placeholder'=>'Name'] ) !!}
        @else
            {!! Form::text('name', null, ['class'=>'form-control', 'placeholder'=>'Name'] ) !!}
        @endif

    </div>
    <div class="form-group">
        @if (Route::current()->getName()==='useraccounts.show')
            {!! Form::text('title', $user->email, ['class'=>'form-control', 'disabled'=>'disabled', 'placeholder'=>'Email'] ) !!}
        @else
            {!! Form::text('email', null, ['class'=>'form-control', 'placeholder'=>'Email'] ) !!}
        @endif
    </div>
    <div class="form-group">
        <input class="form-control" placeholder="Password (minimum of 6 characters)" name="password" type="password" value="">
    </div>

    <div class="form-group">
        <input class="form-control" placeholder="Password" name="password_confirmation" type="password" value="">
    </div>

    <div class="form-group">
        {!! Form::select('usertype_id', array('' => 'Select User Type')+$usertype, old('usertype_id'), ['class'=>'form-control'] ) !!}
    </div>
</div>