@extends('layouts.master')
@section('title')
    BMBOC Admin Panel :: {{ isset($pageTitle) ? $pageTitle : 'User Accounts' }}
@stop
@section('content')
    @include('include.content_header_block')
<section class="content">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">{{ $pageTitle }}</h3>
                </div><!-- /.box-header -->
                @if ($errors->any())
                    <ul class="alert alert-danger" style="list-style:none">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                @endif
                {!! Form::model($user, ['route'=>['useraccounts.update',$user->id],'method'=>'PATCH']) !!}
                @include ('content.users.form')
                <div class="box-footer">
                    {!! Form::submit('Delete', ['name'=>'delete', 'class'=>'btn btn-danger  btn-sm pull-right', 'style'=>'margin-left:5px;']) !!}
                    {!! Form::submit('Update', ['name'=>'update', 'class'=>'btn btn-primary btn-sm pull-right']) !!}
                    <a href="{{ Config::get('app.base_url') }}/useraccounts" class="btn btn-success btn-sm">Back</a>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</section>
@stop
