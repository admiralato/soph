<div class="box-body">
    {!! Form::hidden('id', (Route::current()->getName()==='userrole.show') ? $userrole->id: null ) !!}
    {!! Form::hidden('route_ids', (Route::current()->getName()==='userrole.show') ? $userrole->route_ids: null ) !!}
    <div class="form-group">
        {!! Form::label('title', 'Title:') !!}
        @if (Route::current()->getName()==='userrole.show')
            {!! Form::text('title', $userrole->title, ['id'=>'title', 'class'=>'form-control', 'disabled'=>'disabled'] ) !!}
        @else
            {!! Form::text('title', null, ['id'=>'title', 'class'=>'form-control'] ) !!}
        @endif
    </div>
</div>

<div class="box-body">
    <h4 class="box-title">Roles Resources</h4>
    {!! $appsManager !!}
</div>