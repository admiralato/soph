@extends('layouts.master')
@section('title')
    BMBOC Admin Panel :: {{ isset($pageTitle) ? $pageTitle : 'User Roles' }}
@stop
@section('content')
    @include('include.content_header_block')
<section class="content">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">User Roles</h3>
                </div><!-- /.box-header -->
                @if ($errors->any())
                    <ul class="alert alert-danger" style="list-style:none">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                @endif
                {!! Form::open(['url'=>'userrole','method'=>'POST']) !!}
                @include ('content.userrole.form')
                <div class="box-footer">
                    {!! Form::submit('Create',['name'=>'create', 'class'=>'btn btn-primary  btn-sm pull-right']) !!}
                    <a href="{{ Config::get('app.base_url') }}/userrole" class="btn btn-success btn-sm">Back</a>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</section>
@stop

@section('jsaddon')
    <script type="text/javascript">
        $( ".roles-resources" ).change(function() {
            var sList='';
            var chk =  this.checked;
            if ($(this).is("[has-child]")) {
                $('input[parent-id="'+$(this).attr('id')+'"]').prop('checked', chk);
            }

            var attr = $(this).attr('parent-id');

            // `attr` is false.  Check for both.
            if (typeof attr !== typeof undefined && attr !== false) {
                var chldchk = chk;
                if (chk==true) {
                    $('input[id="'+attr+'"]').prop('checked', chk);
                } else {
                    $('input[parent-id="'+attr+'"]').each( function (e) {
                        if (this.checked==true){
                            chldchk = true;
                            return false;
                        } else {
                            chldchk = false;
                        }
                    });
                    $('input[id="'+$(this).attr('parent-id')+'"]').prop('checked', chldchk);
                }
                
            }

            $('input[type=checkbox].roles-resources:checked').each( function () {
                sList = sList + ( sList.length ==0 ? '': ',' ) + $(this).attr('id');
            });

            $('input[name="route_ids"]').val(sList);
        });
    </script>
@stop

