@extends('layouts.master')
@section('title')
    BMBOC Admin Panel :: {{ isset($pageTitle) ? $pageTitle : 'User Roles' }}
@stop
@section('content')
    @include('include.content_header_block')
<section class="content">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">{{ $pageTitle }}</h3>
                </div><!-- /.box-header -->
                @if ($errors->any())
                    <ul class="alert alert-danger" style="list-style:none">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                @endif
                {!! Form::model($userrole, ['route' => ['userrole.update', $userrole->id], 'method' => 'PATCH']) !!}
                @include ('content.userrole.form')
                <div class="box-footer">
                    {!! Form::submit('Delete', ['name'=>'delete', 'class'=>'btn btn-danger  btn-sm pull-right', 'style'=>'margin-left:5px;']) !!}
                    {!! Form::submit('Update', ['name'=>'update', 'class'=>'btn btn-primary btn-sm pull-right']) !!}
                    <a href="{{ URL::previous() }}" class="btn btn-success btn-sm">Back</a>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</section>
@stop

@section('jsaddon')
    <script type="text/javascript">
        var rids = $('input[name="route_ids"]').val();
        var arr = rids.split(",");

        for (i = 0; i < arr.length; i++) { 
            $('input[id="'+arr[i]+'"]').prop('checked', true);
        }

        $( ".roles-resources" ).change(function() {
            var sList='';
            var chk =  this.checked;
            if ($(this).is("[has-child]")) {
                $('input[parent-id="'+$(this).attr('id')+'"]').prop('checked', chk);
            }

            var attr = $(this).attr('parent-id');

            // `attr` is false.  Check for both.
            if (typeof attr !== typeof undefined && attr !== false) {
                var chldchk = chk;
                if (chk==true) {
                    $('input[id="'+attr+'"]').prop('checked', chk);
                } else {
                    $('input[parent-id="'+attr+'"]').each( function (e) {
                        if (this.checked==true){
                            chldchk = true;
                            return false;
                        } else {
                            chldchk = false;
                        }
                    });
                    $('input[id="'+$(this).attr('parent-id')+'"]').prop('checked', chldchk);
                }
                
            }

            $('input[type=checkbox].roles-resources:checked').each( function () {
                sList = sList + ( sList.length ==0 ? '': ',' ) + $(this).attr('id');
            });

            $('input[name="route_ids"]').val(sList);
        });

    </script>
@stop

