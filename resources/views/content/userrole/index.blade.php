@extends('layouts.master')
@section('title')
    BMBOC Admin Panel :: {{ isset($pageTitle) ? $pageTitle : 'User Roles' }}
@stop
@section('content')
    @include('include.content_header_block')
    <section class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="box box-danger">
                    <div class="box-header with-border">
                        <a href="userrole/create" class="btn btn-sm btn-info btn-flat pull-left"><i class="fa fa-plus"></i> Create New Role</a>
                    </div>
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover">
                            <tr>
                                <th>Action</th>
                                <th>Title</th>
                                <th>Date Created</th>
                                <th>Date Updated</th>
                            </tr>
                            @foreach ($userrole as $role)
                                <tr>
                                    <td>
                                        <a href="userrole/{{ $role->id }}" title="View"><i class="fa fa-eye fa-fw"></i></a>
                                        <a href="userrole/{{ $role->id }}/edit" title="Edit"><i class="fa fa-edit fa-fw"></i></a>
                                        <a href="userrole/{{ $role->id }}/edit" title="Delete"><i class="fa fa-trash fa-fw"></i></a>
                                    </td>
                                    <td>{{ $role->title }}</td>
                                    <td>{{ $role->created_at }}</td>
                                    <td>{{ $role->updated_at }}</td>
                                </tr>
                            @endforeach
                        </table>
                    </div><!-- /.box-body -->
                </div>
            </div>
        </div>
    </section>
@stop