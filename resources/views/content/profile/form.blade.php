@if (strpos(Route::current()->getName(), 'show') > 0 )
    {!! Form::hidden('id', $item->id) !!}
    {!! Form::hidden('profile_key', $item->profile_key) !!}
    {!! Form::hidden('profile_pic_url', $item->profile_pic_url) !!}
@else
    {!! Form::hidden('id', null) !!}
    {!! Form::hidden('profile_key', null) !!}
    {!! Form::hidden('profile_pic_url', null) !!}
@endif


<div class="row">
    <div class="col-lg-4">
        <div class="form-group">
            {!! Form::label('first_name', 'First Name') !!} <span class="required">*</span>
            @if (strpos(Route::current()->getName(), 'show') > 0 )
                {!! Form::text('first_name', $item->first_name, ['class'=>'form-control', 'disabled'=>'disabled'] ) !!}
            @else
                {!! Form::text('first_name', null, ['placeholder'=>'Enter first name (required)', 'class'=>'form-control']) !!}
            @endif
        </div>
    </div>
    <div class="col-lg-4">
        <div class="form-group">
            {!! Form::label('last_name', 'Last Name') !!} <span class="required">*</span>
            @if (strpos(Route::current()->getName(), 'show') > 0 )
                {!! Form::text('last_name', $item->last_name, ['class'=>'form-control', 'disabled'=>'disabled'] ) !!}
            @else
                {!! Form::text('last_name', null, ['placeholder'=>'Enter last name (required)', 'class'=>'form-control']) !!}
            @endif
        </div>
    </div>
    <div class="col-lg-4">
        <div class="form-group">
            {!! Form::label('middle_name', 'Middle Name') !!}
            @if (strpos(Route::current()->getName(), 'show') > 0 )
                {!! Form::text('middle_name', $item->middle_name, ['class'=>'form-control', 'disabled'=>'disabled']) !!}
            @else
                {!! Form::text('middle_name', null, ['placeholder'=>'Enter middle name', 'class'=>'form-control']) !!}
            @endif
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-2">
        <div class="form-group">
            {!! Form::label('registration_date', 'Date Registered') !!} <span class="required">*</span>
            <div class="input-group">
                <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                </div>
                @if (strpos(Route::current()->getName(), 'show') > 0 )
                    {!! Form::text('registration_date', $item->registration_date, ['class'=>'form-control', 'disabled'=>'disabled']) !!}
                @else
                    {!! Form::text('registration_date', null, ['data-inputmask'=>'\'alias\': \'yyyy-mm-dd\'', 'class'=>'form-control', 'data-mask'=>'', 'placeholder'=>'YYYY-MM-DD']) !!}
                @endif
            </div>
        </div>
    </div>
    <div class="col-lg-2">
        <div class="form-group">
            {!! Form::label('date_of_birth', 'Date Of Birth') !!} <span class="required">*</span>
            <div class="input-group">
                <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                </div>
                @if (strpos(Route::current()->getName(), 'show') > 0 )
                    {!! Form::text('date_of_birth', $item->date_of_birth, ['class'=>'form-control', 'disabled'=>'disabled', 'id'=>'date_of_birth']) !!}
                @else
                    {!! Form::text('date_of_birth', null, ['data-inputmask'=>'\'alias\': \'yyyy-mm-dd\'', 'class'=>'form-control', 'data-mask'=>'', 'placeholder'=>'YYYY-MM-DD', 'id'=>'date_of_birth']) !!}
                @endif
            </div>
        </div>
    </div>
    <div class="col-lg-2">
        <div class="form-group">
            {!! Form::label('age', 'Age') !!} <span class="required">*</span>
            @if (strpos(Route::current()->getName(), 'show') > 0 )
                {!! Form::text('age', $item->age, ['class'=>'form-control', 'disabled'=>'disabled', 'id'=>'age']) !!}
            @else
                {!! Form::text('age', null, ['placeholder'=>'Enter age', 'class'=>'form-control', 'id'=>'age']) !!}
            @endif
        </div>
    </div>
    <div class="col-lg-2">
        <div class="form-group">
            {!! Form::label('gender', 'Gender') !!} <span class="required">*</span>
            @if (strpos(Route::current()->getName(), 'show') > 0 )
                {!! Form::select('gender', array(''=>'', 'Male' => 'MALE','Female' => 'FEMALE'), $item->gender, ['class'=>'form-control', 'disabled'=>'disabled'] ) !!}
            @else
                {!! Form::select('gender', array(''=>'', 'Male' => 'MALE','Female' => 'FEMALE'), null, ['class'=>'form-control'] ) !!}
            @endif
        </div>
    </div>
    <div class="col-lg-2">
        <div class="form-group">
            {!! Form::label('status', 'Status') !!} <span class="required">*</span>
            @if (strpos(Route::current()->getName(), 'show') > 0 )
                {!! Form::select('status', array(''=>'', 'Single' => 'SINGLE','Married' => 'MARRIED','Separated' => 'SEPARATED','Divorced' => 'DIVORCED','Widowed' => 'WIDOWED'), $item->status, ['class'=>'form-control', 'disabled'=>'disabled'] ) !!}
            @else
                {!! Form::select('status', array(''=>'', 'Single' => 'SINGLE','Married' => 'MARRIED','Separated' => 'SEPARATED','Divorced' => 'DIVORCED','Widowed' => 'WIDOWED'), null, ['class'=>'form-control'] ) !!}
            @endif
        </div>
    </div>
    <div class="col-lg-2">
        <div class="form-group">
            {!! Form::label('tin', 'TIN') !!}
            @if (strpos(Route::current()->getName(), 'show') > 0 )
                {!! Form::text('tin', $item->tin, ['class'=>'form-control', 'disabled'=>'disabled'] ) !!}
            @else
                {!! Form::text('tin', null, ['placeholder'=>'TIN', 'class'=>'form-control']) !!}
            @endif
        </div>
    </div>
</div>
<hr>
<div class="row">
    <div class="col-lg-4">
        <div class="form-group">
            {!! Form::label('contact_no', 'Contact No.') !!} <span class="required">*</span>
            @if (strpos(Route::current()->getName(), 'show') > 0 )
                {!! Form::text('contact_no', $item->contact_no, ['class'=>'form-control', 'disabled'=>'disabled']) !!}
            @else
                {!! Form::text('contact_no', null, ['placeholder'=>'Enter contact number (required)', 'class'=>'form-control']) !!}
            @endif
        </div>
    </div>
    <div class="col-lg-4">
        <div class="form-group">
            {!! Form::label('mobile_no', 'Mobile No.') !!} <span class="required">*</span>
            @if (strpos(Route::current()->getName(), 'show') > 0 )
                {!! Form::text('mobile_no', $item->mobile_no, ['class'=>'form-control', 'disabled'=>'disabled']) !!}
            @else
                {!! Form::text('mobile_no', null, ['placeholder'=>'Enter mobile number (required)', 'class'=>'form-control']) !!}
            @endif
        </div>
    </div>
    <div class="col-lg-4">
        <div class="form-group">
            {!! Form::label('email', 'Email Address') !!}
            @if (strpos(Route::current()->getName(), 'show') > 0 )
                {!! Form::text('email', $item->email, ['class'=>'form-control', 'disabled'=>'disabled']) !!}
            @else
                {!! Form::text('email', null, ['placeholder'=>'Enter email address', 'class'=>'form-control']) !!}
            @endif
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-6">
        <div class="form-group">
            {!! Form::label('address_primary', 'Address') !!} <span class="required">*</span>
            @if (strpos(Route::current()->getName(), 'show') > 0 )
                {!! Form::textarea('address_primary', $item->address_primary, ['class'=>'form-control', 'rows'=>'3', 'disabled'=>'disabled'] ) !!}
            @else
                {!! Form::textarea('address_primary', null, ['class'=>'form-control', 'rows'=>'3', 'placeholder'=>'Enter primary address here... (required)'] ) !!}
            @endif
        </div>
    </div>
    <div class="col-lg-6">
        <div class="form-group">
            {!! Form::label('address_secondary', 'Address') !!}
            @if (strpos(Route::current()->getName(), 'show') > 0 )
                {!! Form::textarea('address_secondary', $item->address_secondary, ['class'=>'form-control', 'rows'=>'3', 'disabled'=>'disabled'] ) !!}
            @else
                {!! Form::textarea('address_secondary', null, ['class'=>'form-control', 'rows'=>'3', 'placeholder'=>'Enter secondary address here... (optional)'] ) !!}
            @endif
        </div>
    </div>
    <div class="col-lg-6">
        <div class="form-group">
            {!! Form::label('zip', 'Zip Code') !!}
            @if (strpos(Route::current()->getName(), 'show') > 0 )
                {!! Form::text('zip', $item->zip, ['class'=>'form-control', 'disabled'=>'disabled']) !!}
            @else
                {!! Form::text('zip', null, ['placeholder'=>'Enter Zip code', 'class'=>'form-control']) !!}
            @endif
        </div>
    </div>
    <div class="col-lg-6">
        <div class="form-group">
            {!! Form::label('country', 'Country') !!}
            @if (strpos(Route::current()->getName(), 'show') > 0 )
                {!! Form::select('country', array(''=>'', 'Philippines' => 'Philippines'), $item->country, ['class'=>'form-control', 'disabled'=>'disabled'] ) !!}
            @else
                {!! Form::select('country', array(''=>'', 'Philippines' => 'Philippines'), 'Philippines', ['class'=>'form-control'] ) !!}
            @endif
        </div>
    </div>
</div>
