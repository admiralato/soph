@extends('layouts.master')
@section('title')
    BMBOC Admin Panel :: {{ isset($pageTitle) ? $pageTitle : 'Profile' }}
@stop
@section('content')
    @include('include.content_header_block')

    <section class="content">
    
        <div class="row">
            <div class="col-lg-12">
                <div class="box box-danger">
                    <div class="box-header with-border">
                        <a href="profile/create" class="btn btn-sm btn-info btn-flat pull-left"><i class="fa fa-plus"></i> Create New Profile</a>
                        <div class="box-tools" style="margin-top:5px;">
                          <a href="{{ url('/profile') }}" class="btn btn-sm btn-default btn-flat pull-right" style="margin-left:10px;">Reset</a>
                          <div class="input-group input-group-sm pull-right" style="width: 300px;">
                            <input type="text" name="keywords" class="form-control pull-right" placeholder="Search">
                            <div class="input-group-btn">
                              <button type="submit" class="btn btn-default" id="btn-search"><i class="fa fa-search"></i></button>
                            </div>
                          </div>
                          <div class="input-group input-group-sm pull-right" style="width: 120px; margin-right:10px;">
                            {!! Form::select('search-by', array('pid'=>'Profile ID', 'ln' => 'Last Name','fn' => 'First Name','email' => 'Email','mb' => 'Mobile No.' ), 'ln', ['class'=>'form-control'] ) !!}
                          </div>
                        </div>
                    </div>                    

                    @if ($profiles && count($profiles) > 0) 
                      <div class="box-body table-responsive">

                        @if (Session::has('message'))
                        <div class="alert alert-{{ Session::get('classstyle') }} alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ Session::get('message') }}
                        </div>
                        @endif

                      
                        <table class="table table-bordered">
                          <thead>
                            <tr>
                              <th style="width:78px">Action</th>
                              <th></th>
                              <th>Last Name</th>
                              <th>First Name</th>
                              <th>Middle</th>
                              <th>Status</th>
                              <th>Mobile No.</th>
                              <th>Emal Address</th>
                              <th style="width:150px">Address</th>
                              <th>Date Registered</th>
                              <th style="width: 30px">Member</th>
                            </tr>
                          </thead>
                          <tbody>
                            @foreach($profiles as $item)
                              <tr>
                                <td>
                                    <a href="profile/{{ $item->id }}" title="View"><i class="fa fa-eye fa-fw"></i></a>
                                    <a href="profile/{{ $item->id }}/edit" title="Edit"><i class="fa fa-edit fa-fw"></i></a>
                                    <a href="profile/{{ $item->id }}/edit" title="Delete"><i class="fa fa-trash fa-fw"></i></a>
                                </td>
                                @if ( $item && $item->profile_pic_url && strlen($item->profile_pic_url) > 0 )
                                  <td style="text-align:center"><img src="{{ $item->profile_pic_url }}" style="with:24px; height:24px;" alt="{{ $item->last_name }}, {{ $item->first_name }}"></td>
                                @else
                                    <td style="text-align:center"><img src="{{asset('resources/assets/static/images/flat-'. $item->gender .'.png')}}" style="with:24px; height:24px;" alt="{{ $item->last_name }}, {{ $item->first_name }}"></td>
                                @endif
                                <td>{{ $item->last_name }}</td>
                                <td>{{ $item->first_name }}</td>
                                <td>{{ $item->middle_name }}</td>
                                <td>{{ $item->status }}</td>
                                <td>{{ $item->mobile_no }}</td>
                                <td>{{ $item->email }}</td>
                                <td><div style="width:150px; white-space: nowrap; text-overflow: ellipsis; overflow:hidden">{{ $item->address_primary }}</div></td>
                                <td>{{ $item->registration_date }}</td>
                                <td style="text-align:center; color:{{{ $item->member_id != null? '#00c0ef': '#EF402A' }}}"> <i class="fa {{{ $item->member_id != null? 'fa-check-circle': 'fa-thumbs-down' }}} fa-fw"></i></td>
                              </tr>
                            @endforeach

                          </tbody>
                        </table>
                        <div class="paging pull-right">
                          {!! stripos($profiles->render(),'profile?q') > 0? str_replace('?page', '&page', $profiles->render()): $profiles->render()  !!}
                        </div>
                      </div>
                    @else
                      <div class="box-body table-responsive">
                        No results found. {{ count($profiles) }}
                      </div>
                    @endif
                    
                </div>
            </div>
        <!-- /.col-lg-12 -->
        </div>
    </section>
@stop

@section('jsaddon')
<script type="text/javascript">
  $(function() {

    var getUrlParameter = function getUrlParameter(sParam) {
            var sPageURL = decodeURIComponent(window.location.search.substring(1)),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');

                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : sParameterName[1];
                }
            }
        };

    var sb =  getUrlParameter('sb');
    if (sb != undefined && sb.length > 0) {
      $('select[name="search-by"]').val(sb);
    }
    

    $( 'button#btn-search' ).on('click', function(e) {
      e.preventDefault();
      var keywords = $( 'input[name="keywords"]' ).val();
      var searchby = $( 'select[name="search-by"]' ).val();
      var searchurl = "{{ url('/profile?q=') }}" + keywords + '&sb='+searchby;
      window.location.href = searchurl;
    });

  });
</script>
@stop


