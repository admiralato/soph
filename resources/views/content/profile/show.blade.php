@extends('layouts.master')
@section('title')
    BMBOC Admin Panel :: {{ isset($pageTitle) ? $pageTitle : 'Profile' }}
@stop
@section('content')
    @include('include.content_header_block')

    <section class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="box box-danger">
                    <div class="box-header with-border">
                        @if ( $item && $item->profile_pic_url && strlen($item->profile_pic_url) > 0 )
                            <div class="pull-left image">
                                <img style="width:100%; max-width:160px;" src="{{ $item->profile_pic_url }}" alt="User profile picture">
                            </div>
                        @else
                            <h3 class="box-title">View</h3>
                        @endif
                    </div>
                    <div class="box-body">
                        @include ('content.profile.form')
                    </div>
                    <div class="box-footer">
                        <a href="{{ Config::get('app.base_url') }}/profile/{{ $item->id }}/edit" title="Edit" class="btn btn-primary btn-normal pull-right">Edit</a>
                        <a href="{{ URL::previous() }}" class="btn btn-success btn-normal">Back</a>
                    </div>
                </div>
            </div>
        </div>
    </section>

@stop