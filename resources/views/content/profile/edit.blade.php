@extends('layouts.master')
@section('title')
    BMBOC Admin Panel :: {{ isset($pageTitle) ? $pageTitle : 'Profile' }}
@stop
@section('content')
    @include('include.content_header_block')

    <section class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="box box-danger">
                    {!! Form::model($item, ['route'=>['profile.update',$item->id],'method'=>'PATCH']) !!}
                    <div class="box-header with-border">
                        
                        @if ( $item && $item->profile_pic_url && strlen($item->profile_pic_url) > 0 )
                            <div class="pull-left image">
                                <img style="width:100%; max-width:160px;" src="{{ $item->profile_pic_url }}" alt="User profile picture">
                                <br>
                                <a class="btn btn-default btn-sm" data-toggle="modal" data-target="#profilePicture" id="profilepix" style="margin-top:8px;"><i class="fa fa-user"></i> Change Profile Picture</a>
                            </div>
                        @else
                            <a class="btn btn-default btn-sm" data-toggle="modal" data-target="#profilePicture" id="profilepix"><i class="fa fa-user"></i> Upload Profile Picture</a>
                        @endif
                        
                        @if ($errors->any())
                            <ul class="alert alert-danger" style="list-style:none; margin-top:10px;">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif
                    </div>
                    
                    <div class="box-body">
                        @include ('content.profile.form')
                    </div>
                    <div class="box-footer">
                        {!! Form::submit('Delete',['name'=>'delete', 'class'=>'btn btn-danger  btn-normal pull-right', 'style'=>'margin-left:5px;']) !!}
                        {!! Form::submit('Update',['name'=>'update', 'class'=>'btn btn-primary  btn-normal pull-right']) !!} 
                        <a href="{{ URL::previous() }}" class="btn btn-success btn-normal">Back</a>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </section>
    @include('widgets.w_profile_pic')
@stop
@section('jsaddon')
<script type="text/javascript">
    
    function getAge(dateString) {
        var today = new Date();
        var birthDate = new Date(dateString);
        var age = today.getFullYear() - birthDate.getFullYear();
        var m = today.getMonth() - birthDate.getMonth();
        if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
            age--;
        }
        return age;
    }

    $(function() {
        $("#date_of_birth").bind("blur", function() { 
            var age = getAge(new Date($("#date_of_birth").val()));
            $("#age").val(age) 
        });

        $("#profilepix").on("click", function(){
            $('.modal-dialog').width(390);

            $("button#savephoto").hide();
            $("#tryagain").hide();
            $("#canvas").hide();
            $("#snap").show();
            $("#video").show();

            // Grab elements, create settings, etc.
            var canvas = document.getElementById("canvas"),
                context = canvas.getContext("2d"),
                video = document.getElementById("video"),
                videoObj = { "video": true },
                errBack = function(error) {
                    console.log("Video capture error: ", error.code); 
                };

            // Put video listeners into place
            if(navigator.getUserMedia) { // Standard
                navigator.getUserMedia(videoObj, function(stream) {
                    video.src = stream;
                    video.play();
                }, errBack);
            } else if(navigator.webkitGetUserMedia) { // WebKit-prefixed
                navigator.webkitGetUserMedia(videoObj, function(stream){
                    video.src = window.webkitURL.createObjectURL(stream);
                    video.play();
                }, errBack);
            }
            else if(navigator.mozGetUserMedia) { // Firefox-prefixed
                navigator.mozGetUserMedia(videoObj, function(stream){
                    video.src = window.URL.createObjectURL(stream);
                    video.play();
                }, errBack);
            }
        });

        $("#snap").on("click", function(){

            if ( $("#snap").is(':visible') ) {
                $("button#snap").hide();
                $("video#video").hide();
                $("#tryagain").show();
                $("#canvas").show();
                $("button#savephoto").show();
            }
            
            
            var canvas = document.getElementById("canvas"),
                context = canvas.getContext("2d"),
                video = document.getElementById("video")
            context.drawImage(video, 0, 0, 360, 280);
            
        });

        $("#tryagain").on("click", function(){
            $("#snap").show();
            $("#video").show();
            $("#tryagain").hide();
            $("#canvas").hide();
            $("button#savephoto").hide();
        });

        $('#savephoto').on("click", function(){ 
            var canvas = document.getElementById("canvas");
            var img    = canvas.toDataURL("image/jpeg");
            $('input[name="profile_pic_url"]').val(img);
            $('#profilePicture').modal('hide');
            //console.log(img);
        });
    });
</script>
@stop