@extends('layouts.master')
@section('title')
    BMBOC Admin Panel :: {{ isset($pageTitle) ? $pageTitle : 'Create Profile' }}
@stop
@section('content')
    @include('include.content_header_block')

    <section class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="box box-danger">
                    {!! Form::open(['url'=>'profile','method'=>'POST','role'=>'form']) !!}
                    <div class="box-header with-border">
                        @if (strpos(Route::current()->getName(), 'create') > 0 ) 
                            <canvas id="preview-canvas" width="160" height="124" style="display:none"></canvas><br>
                            <a class="btn btn-default btn-sm" data-toggle="modal" data-target="#profilePicture" id="profilepix"><i class="fa fa-user"></i> Profile Picture</a>
                        @endif

                        @if ($errors->any())
		                    <ul class="alert alert-danger" style="list-style:none; margin-top:10px;">
		                        @foreach ($errors->all() as $error)
		                            <li>{{ $error }}</li>
		                        @endforeach
		                    </ul>
		                @endif
                    </div>
                    <div class="box-body">
		                @include ('content.profile.form')
                    </div>
                    <div class="box-footer">
	                    {!! Form::submit('Create',['name'=>'create', 'class'=>'btn btn-primary btn-normal pull-right']) !!}
	                    <!--
                        <div class="checkbox pull-right" style="margin-right:20px;">
		                  <label><input type="checkbox" name="load_to_member"> Add to Member</label>
		                </div>
                        -->
	                    <a href="{{ URL::previous() }}" class="btn btn-success btn-normal">Back</a>
	                </div>
	                {!! Form::close() !!}
                </div>
            </div>
        </div>
    </section>
    @include('widgets.w_profile_pic')
@stop
@section('jsaddon')
<script type="text/javascript">
    $(function() {
        $("#date_of_birth").bind("blur", function() { 
            var age = getAge(new Date($("#date_of_birth").val()));
            $("#age").val(age) 
        });

        $("#profilepix").on("click", function(){
            $('.modal-dialog').width(390);

            $("button#savephoto").hide();
            $("#tryagain").hide();
            $("#canvas").hide();
            $("#snap").show();
            $("#video").show();

            // Grab elements, create settings, etc.
            var canvas = document.getElementById("canvas"),
                context = canvas.getContext("2d"),
                video = document.getElementById("video"),
                videoObj = { "video": true },
                errBack = function(error) {
                    console.log("Video capture error: ", error.code); 
                };

            // Put video listeners into place
            if(navigator.getUserMedia) { // Standard
                navigator.getUserMedia(videoObj, function(stream) {
                    video.src = stream;
                    video.play();
                }, errBack);
            } else if(navigator.webkitGetUserMedia) { // WebKit-prefixed
                navigator.webkitGetUserMedia(videoObj, function(stream){
                    video.src = window.webkitURL.createObjectURL(stream);
                    video.play();
                }, errBack);
            }
            else if(navigator.mozGetUserMedia) { // Firefox-prefixed
                navigator.mozGetUserMedia(videoObj, function(stream){
                    video.src = window.URL.createObjectURL(stream);
                    video.play();
                }, errBack);
            }
        });

        $("#snap").on("click", function(){

            if ( $("#snap").is(':visible') ) {
                $("button#snap").hide();
                $("video#video").hide();
                $("#tryagain").show();
                $("#canvas").show();
                $("button#savephoto").show();
            }
            
            
            var canvas = document.getElementById("canvas"),
                context = canvas.getContext("2d"),
                video = document.getElementById("video")
            context.drawImage(video, 0, 0, 360, 280);
            
        });

        $("#tryagain").on("click", function(){
            $("#snap").show();
            $("#video").show();
            $("#tryagain").hide();
            $("#canvas").hide();
            $("button#savephoto").hide();
        });

        $('#savephoto').on("click", function(){ 
            var canvas = document.getElementById("canvas");
            var img    = canvas.toDataURL("image/jpeg");
            $('input[name="profile_pic_url"]').val(img);

            var previewcanvas = document.getElementById("preview-canvas"),
                context = previewcanvas.getContext("2d"),
                video = document.getElementById("video")
            context.drawImage(video, 0, 0, 160, 124);
            $("#preview-canvas").show();
            $('#profilePicture').modal('hide');
            //console.log(img);
        });
    });
    function getAge(dateString) {
        var today = new Date();
        var birthDate = new Date(dateString);
        var age = today.getFullYear() - birthDate.getFullYear();
        var m = today.getMonth() - birthDate.getMonth();
        if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
            age--;
        }
        return age;
    }
</script>
@stop