@extends('layouts.master')
@section('title')
    BMBOC Admin Panel :: {{ isset($pageTitle) ? $pageTitle : 'Dashboard' }}
@stop
@section('content')
    @include('include.content_header_block')

    <section class="content">
        <div class="row">
            @include('widgets.w_summary')
        </div>
    
        <div class="row">
            <div class="col-md-4">
                @include('widgets.w_saleable_package')
            </div>
            <div class="col-md-4">
                @include('widgets.w_members')
            </div>
            <div class="col-md-4">
                @include('widgets.w_products')
            </div>
        </div>
        <!-- Application shortcuts-->
        <div class="row">
            <div class="col-lg-12">
                
            </div>
        </div>
    </section>
@stop

@section('jsaddon')
<script type="text/javascript">
    $(function() {
        var saleablePackages = function getSaleablePackages() { 
            $.get("{{ url('api/data/saleable-package/top') }}", function(html){
                if (html.length > 0) {
                    $(html).appendTo('#saleable-packages');
                }
            });
        }
        saleablePackages(); //Get Top Saleable Packages 

        var activeMembers = function getActiveMembers() { 
            $.get("{{ url('api/data/members/active') }}", function(html){
                if (html.length > 0) {
                    $(html).appendTo('#active-members');
                }
            });
        }
        activeMembers(); //Get Active Members

        var packages = function getPackages() { 
            $.get("{{ url('api/data/packages/active') }}", function(html){
                if (html.length > 0) {
                    $(html).appendTo('#product-list');
                }
            });
        }
        packages();
    });
</script>
@stop