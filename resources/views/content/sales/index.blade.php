@extends('layouts.master')
@section('title')
    BMBOC Admin Panel :: {{ isset($pageTitle) ? $pageTitle : 'Sales' }}
@stop
@section('content')
    @include('include.content_header_block')

    <section class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="box box-danger">
                    <div class="box-header with-border">
                        <h3>Orders</h3>
                        <div class="box-tools" style="margin-top:5px;">
                          <a href="{{ url('/sales') }}" class="btn btn-sm btn-default btn-flat pull-right" style="margin-left:10px;">Reset</a>
                          <div class="input-group input-group-sm pull-right" style="width: 300px;">
                            <span id="col-date-range" style="display:none;">
                              <input type="text" name="fr" class="form-control" placeholder="Fr (YYYY-MM-DD)" style="width:130px; margin-right:7px; padding:5px 10px; height:30px;">
                              <input type="text" name="to" class="form-control pull-right" placeholder="To (YYYY-MM-DD)" style="width:130px; padding:5px 10px; height:30px;">
                            </span>

                            <input type="text" name="keywords" class="form-control pull-right" placeholder="Search">
                            <div class="input-group-btn">
                              <button type="submit" class="btn btn-default" id="btn-search"><i class="fa fa-search"></i></button>
                            </div>
                          </div>
                          <div class="input-group input-group-sm pull-right" style="width: 120px; margin-right:10px;">
                            {!! Form::select('search-by', array('oid'=>'Order ID', 'mid'=>'Member ID', 'name' => 'Name','email' => 'Email','date' => 'Date','stt'=>'Status', 'daterange'=>'Date Range' ), 'oid', ['class'=>'form-control'] ) !!}
                          </div>
                        </div>
                    </div>
                    <div class="box-body">
                        @if ($sales && count($sales) > 0) 

                            @if (Session::has('message'))
                            <div class="alert alert-{{ Session::get('classstyle') }} alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                {{ Session::get('message') }}
                            </div>
                            @endif


                            <table class="table table-bordered">
                              <thead>
                                <tr>
                                  <th style="width:24px;">Action</th>
                                  <th style="width:80px;">Order #</th>
                                  <th style="width:110px;">Member ID</th>
                                  <th style="width:160px;">Name</th>
                                  <th>Email</th>
                                  <th>Purchased On</th>
                                  <th style="text-align:right">Sub Total</th>
                                  <th style="text-align:right">Discount</th>
                                  <th style="text-align:right">Total</th>
                                  <th>Status</th>
                                </tr>
                              </thead>
                              <tbody>
                                @foreach($sales as $item)
                                  <tr>
                                    <td>
                                        <a href="sales/{{ $item->id }}" title="View Order Details"><i class="fa fa-eye fa-fw"></i></a>
                                        <a href="sales/{{ $item->id }}/destroy" title="Delete" id="delete-link" data-id="{{ $item->id }}"><i class="fa fa-trash fa-fw"></i></a>
                                    </td>
                                    <td><a href="sales/{{ $item->id }}" title="View Order Details"><span class="badge bg-maroon-active" style="font-size:1em;">{{ $item->id }}</span></a></td>
                                    <td>{{ $item->member_id }}</td>
                                    <td>{{ $item->name }}</td>
                                    <td>{{ $item->email }}</td>
                                    <td>{{ $item->created_at }}</td>
                                    <th style="text-align:right">{{ $item->sub_total }}</th>
                                    <th style="text-align:right">{{ $item->discount }}</th>
                                    <td style="text-align:right">{{ $item->grand_total }}</td>
                                    <td>{{ $item->status }}</td>
                                  </tr>
                                @endforeach

                              </tbody>
                            </table>
                            <div class="paging pull-right">
                              {!! stripos($sales->render(),'sales?q') > 0? str_replace('?page', '&page', $sales->render()): $sales->render()  !!}
                            </div>
                        @else
                          <div class="box-body table-responsive">
                            No results found. {{ count($sales) }}
                          </div>
                        @endif
                    </div>
                    <div class="box-footer">
                        <a href="{{ URL::previous() }}" class="btn btn-success btn-normal">Back</a>
                    </div>
                </div>
            </div>
        </div>
    </section>

@stop


@section('jsaddon')
{!! Html::script('resources/assets/plugins/date/jquery.plugin.min.js') !!}
{!! Html::script('resources/assets/plugins/date/jquery.dateentry.min.js') !!}
<script type="text/javascript">
  $(function() {

    /* BEGIN DELETE LINK */
    $('a#delete-link').on('click', function(e){
      e.preventDefault();
      var id = $(this).attr('data-id');
      var answer = confirm('Are you sure you want to delete Sale ID ' + id + '?');
      if (answer){
          var deleteurl = $(this).attr('href');
          window.location.href = deleteurl;
      } else{
          return false;
      }
    });
    /* END DELETE LINK */

    $('input[name="fr"]').dateEntry({dateFormat: 'ymd-'});
    $('input[name="to"]').dateEntry({dateFormat: 'ymd-'});

    $('select[name="search-by"]').change(function() {
      var sby = $(this).val();
      //var drange = $('#col-date-range').is(":visible"); 

      $('#col-date-range').hide();
      $('input[name="keywords"]').show();
      $('input[name="keywords"]').attr('placeholder','Search');

      if (sby=='date') $('input[name="keywords"]').attr('placeholder','YYYY-MM-DD');
      if (sby=='daterange') {
        $('.dateEntry-control').css('display','block');
        $('input[name="keywords"]').hide();
        $('#col-date-range').show();
        $('input[name="fr"]').select();
      } else {
        $('input[name="keywords"]').select();  
      }
      
    });

    var getUrlParameter = function getUrlParameter(sParam) {
            var sPageURL = decodeURIComponent(window.location.search.substring(1)),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');

                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : sParameterName[1];
                }
            }
        };

    var sb =  getUrlParameter('sb');
    var q = getUrlParameter('q');
    if (sb != undefined && sb.length > 0) {
      $('select[name="search-by"]').val(sb);
    }

    if (q != undefined && q.length > 0) {
      $('input[name="keywords"]').val(q);
    }
    
    if (sb=='daterange') {
        $('.dateEntry-control').css('display','block');
        $('input[name="keywords"]').hide();
        $('#col-date-range').show();
        var fr = getUrlParameter('fr');
        var to = getUrlParameter('to');
        $('input[name="fr"]').val(fr);
        $('input[name="to"]').val(to);

        $('input[name="fr"]').select();
      } else {
        $('input[name="keywords"]').select(); 
        $('input[name="keywords"]').show();
        $('#col-date-range').hide(); 
      }
    

    $( 'button#btn-search' ).on('click', function(e) {
      e.preventDefault();
      var keywords = $( 'input[name="keywords"]' ).val();
      var searchby = $( 'select[name="search-by"]' ).val();

      var searchurl = "{{ url('/sales?q=') }}" + keywords + '&sb='+searchby;
      if (searchby=='daterange') { 
        var fr = $( 'input[name="fr"]' ).val();
        var to = $( 'input[name="to"]' ).val();

        if (fr.length <=0 | to.length <=0 ) return false;
        searchurl = "{{ url('/sales?fr=') }}" + fr + '&to='+ to +'&sb='+searchby;
      } 
      window.location.href = searchurl;
    });

  });
</script>
@stop

