@extends('layouts.master')
@section('title')
    BMBOC Admin Panel :: {{ isset($pageTitle) ? $pageTitle : 'Order Details' }}
@stop
@section('content')
    @include('include.content_header_block')

    <section class="content">
        <div class="row">
            <div class="col-lg-12" id="order-{{ $item->id }}">
                <h3>
                    <i class="fa fa-shopping-cart fa-fw"></i> ORDER # {{ $item->id }} | {{ $item->created_at }} 
                    <span class="pull-right">
                        <a href="javascript:void(0);" class="add-cart btn btn-danger btn-xs disabled">Send Order Confirmation Email</a>
                    </span>
                </h3>
            </div>
        </div>
        <div class="row">
            <div class="col-md-5">
                <div class="box box-widget widget-user-2">
                    <div class="widget-user-header bg-orange-active">
                        <div class="widget-user-image">
                            <img class="img-circle" src="{{asset('resources/assets/static/images/shoppingbag.png')}}" alt="User Avatar">
                        </div>
                        <!-- /.widget-user-image -->
                        <h3 class="widget-user-username">{{ $item->name }}</h3>
                        <h5 class="widget-user-desc">{{ $item->email }}</h5>
                    </div>
                    <div class="box-footer no-padding">
                      <ul class="nav nav-stacked">
                        <li><a href="#">Order # <span class="pull-right badge bg-blue" style="font-size:1.2em;">{{ $item->id }}</span></a></li>
                        <li><a href="#">Order Date <span class="pull-right badge bg-blue" style="font-size:1.1em;">{{ $item->created_at }}</span></a></li>
                        <li><a href="#">Order Status <span class="pull-right badge bg-green" style="font-size:1.1em;">{{ strtoupper($item->status) }}</span></a></li>
                      </ul>
                    </div>
                </div>
                <div class="box box-widget widget-user-2">
                    <div class="widget-user-header bg-teal">
                        <h2 class="widget-user-username"><i class="fa fa-shopping-cart fa-fw"></i> Order Totals</h2>
                    </div>
                    <div class="box-footer no-padding">
                      <ul class="nav nav-stacked">
                        <li><a href="#">Sub Total <span class="pull-right" style="font-size:1.2em;">{{ number_format($item->sub_total,2) }} Php</span></a></li>
                        <li><a href="#">Discount <span class="pull-right" style="font-size:1.1em;">{{ number_format($item->discount,2) }} Php</span></a></li>
                        <li><a href="#">Grand Total <span class="pull-right badge bg-aqua" style="font-size:1.2em;">{{ number_format($item->grand_total,2) }} PHP</span></a></li>
                      </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-7">
                <div class="box">
                    <div class="box-header">
                      <h3 class="box-title"><i class="fa fa-list-ul fa-fw"></i> Items Ordered</h3>
                      <span id="item-count" class="badge pull-right bg-blue" style="font-size:1.2em;">Count: {{ count($orders) }}</span>
                      <div class="box-tools"></div>
                    </div>
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                  <th>ID</th>
                                  <th>ITEM</th>
                                  <th style="text-align:right;">PRICE</th>
                                </tr>
                            </thead>
                            @foreach($orders as $order)
                                <tr id="{{ $order->id }}" sales-id="{{ $order->sales_order_id }}" package-id="{{ $order->package_id }}" pdc="{{ $order->package_pdc }}">
                                  <td>{{ $order->id }}</td>
                                  <td><span class="badge bg-{{ $order->color }}">Package {{ $order->package_title }}</span></td>
                                  <td style="text-align:right;">{{ $order->price }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <a href="{{ URL::previous() }}" class="btn btn-success pull-right btn-normal">Back</a>
            </div>
        </div>
    </section>

@stop