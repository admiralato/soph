<div class="box box-widget widget-user-2">
  <!-- Add the bg color to the header using any of the bg-* classes -->
  <div class="widget-user-header bg-orange-active">
    
    <h2 class="widget-user-username"><i class="fa fa-dropbox fa-fw"></i> Top Packages</h2>
  </div>
  <div class="box-footer no-padding" id="saleable-packages">
    
  </div>
</div>