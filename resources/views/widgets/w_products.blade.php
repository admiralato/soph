<div class="box box-primary">
  <div class="box-header with-border">
    <h3 class="box-title">Packages/Products</h3>

    <div class="box-tools pull-right">
      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
      </button>
      <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
    </div>
  </div>
  <!-- /.box-header -->
  <div class="box-body" id="product-list">
    
  </div>
  <!-- /.box-body -->
  <div class="box-footer text-center">
    <a href="{{ url('/packages')}}" class="uppercase">View All Products</a>
  </div>
  <!-- /.box-footer -->
</div>