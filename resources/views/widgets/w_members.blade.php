<div class="box box-danger">
  <div class="box-header with-border">
    <h3 class="box-title">Active Members</h3>
    <div class="box-tools pull-right">
      <!--<span class="label label-danger">8 New Members</span>-->
      <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
      <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
    </div>
  </div><!-- /.box-header -->
  <div class="box-body no-padding" id="active-members">
    
  </div><!-- /.box-body -->
  <div class="box-footer text-center">
    <a href="{{ url('/member')}}" class="uppercase">View All Members</a>
  </div><!-- /.box-footer -->
</div>