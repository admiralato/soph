<!-- Modal -->
<div id="profilePicture" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Camera</h4>
      </div>
      <div class="modal-body">
        <video id="video" width="360" height="280" autoplay></video>
        <canvas id="canvas" width="360" height="280"></canvas>
      </div>
      <div class="modal-footer">
        <button id="snap" class="btn btn-default">Capture</button>
        <button id="tryagain" class="btn btn-default">Try again</button>
        <button id="savephoto" class="btn btn-default">Save Photo</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>