<div class="col-md-3 col-sm-6 col-xs-12">
  <div class="info-box">
    <span class="info-box-icon bg-aqua"><i class="fa fa-shopping-cart"></i></span>
    <div class="info-box-content">
      <span class="info-box-text">New Orders</span>
      <span class="info-box-number">{{ $newOrders }}</span>
    </div>
    <!-- /.info-box-content -->
  </div>
  <!-- /.info-box -->
</div>
<!-- /.col -->
<div class="col-md-3 col-sm-6 col-xs-12">
  <div class="info-box">
    <span class="info-box-icon bg-red"><i class="fa fa-star"></i></span>

    <div class="info-box-content">
      <span class="info-box-text">Saleable Package</span>
      <span class="info-box-number">Package {{ $salablePackage }}</span>
    </div>
    <!-- /.info-box-content -->
  </div>
  <!-- /.info-box -->
</div>
<!-- /.col -->

<!-- fix for small devices only -->
<div class="clearfix visible-sm-block"></div>

<div class="col-md-3 col-sm-6 col-xs-12">
  <div class="info-box">
    <span class="info-box-icon bg-green"><i class="fa fa-dollar"></i></span>

    <div class="info-box-content">
      <span class="info-box-text">Sales</span>
        @if(Auth::user()->usertype_id == 1)
            <span class="info-box-number">{{ $totalSales }} Php</span>
        @else
            <span class="info-box-number">0.00 Php</span>
        @endif
    </div>
    <!-- /.info-box-content -->
  </div>
  <!-- /.info-box -->
</div>
<!-- /.col -->
<div class="col-md-3 col-sm-6 col-xs-12">
  <div class="info-box">
    <span class="info-box-icon bg-yellow"><i class="fa fa-group fa-fw"></i></span>

    <div class="info-box-content">
      <span class="info-box-text">Total Members</span>
      <span class="info-box-number">{{ $memberCount }}</span>
    </div>
    <!-- /.info-box-content -->
  </div>
  <!-- /.info-box -->
</div>
        <!-- /.col -->
      