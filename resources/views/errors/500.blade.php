@extends('layouts.master')
@section('title')
    BMBOC Admin Panel :: {{ isset($pageTitle) ? $pageTitle : 'Error 500' }}
@stop
@section('content')
    @include('include.content_header_block')
    <section class="content">
      <div class="error-page">
        <h2 class="headline text-red">500</h2>
        <div class="error-content">
          <h3><i class="fa fa-warning text-red"></i> Oops! Something went wrong.</h3>
          <p>
            We will work on fixing that right away.
            Meanwhile, you may <a href={{ url('/dashboard') }}>return to dashboard</a>.
          </p>
          
        </div>
      </div><!-- /.error-page -->

    </section><!-- /.content -->
@stop