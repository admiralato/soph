<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('packages', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('title');
            $table->decimal('amount', 10, 2);
            $table->decimal('first_fifteen', 10, 2);
            $table->decimal('second_fifteen', 10, 2);
            $table->decimal('third_fifteen', 10, 2);
            $table->string('color')->default('black');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('packages');
    }
}
