<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesdetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('salesdetails', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('sales_order_id')->unsigned();
            $table->integer('package_id')->unsigned();
            $table->string('package_title',255);
            $table->string('package_pdc',255);
            $table->boolean('cheque_issued')->default(false);
            $table->decimal('price', 18, 2);
            $table->string('group_code',10)->default('DEQUODE');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('salesdetails');
    }
}
