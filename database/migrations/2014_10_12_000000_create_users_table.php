<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password', 60);
            $table->integer('profile_id')->unsigned()->nullable();
            $table->integer('usertype_id')->unsigned()->default(1);
            $table->boolean('is_active')->default(1);
            $table->rememberToken()->nullable();
            $table->timestamps();
            
            $table->foreign('usertype_id')->references('id')->on('usertype');
//            $table->foreign('profile_id')->references('id')->on('profile');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
