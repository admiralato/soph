<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChequeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cheque', function(Blueprint $table) {
            $table->increments('id');
            $table->string('cheque_no');
            $table->decimal('value', 10, 2);
            $table->date('date_issued');
            $table->integer('issued_by_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
            
            $table->foreign('issued_by_id')->references('id')->on('profile');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cheque');
    }
}
