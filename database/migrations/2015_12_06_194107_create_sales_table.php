<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('member_id')->unsigned();
            $table->integer('profile_id')->unsigned();
            $table->date('date_purchased');
            $table->string('name',100);
            $table->string('email',100)->nullable();
            $table->boolean('email_sent')->default(false);
            $table->enum('status', ['Pending', 'Cancelled', 'Void', 'Complete', 'Cheque Issued']);
            $table->decimal('sub_total', 18, 2);
            $table->decimal('discount', 18, 2);
            $table->decimal('grand_total', 18, 2);
            $table->string('group_code',10)->default('DEQUODE');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sales');
    }
}
