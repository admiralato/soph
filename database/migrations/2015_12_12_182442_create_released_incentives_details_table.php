<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReleasedIncentivesDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('released_incentives_details', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('released_incentives_id')->unsigned();
            $table->integer('level');
            $table->integer('total_products');
            $table->decimal('incentive', 10, 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('released_incentives_details');
    }
}
