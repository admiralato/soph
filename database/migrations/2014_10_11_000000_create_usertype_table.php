<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsertypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usertype', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('title',225)->unique();
            $table->integer('role_id')->unsigned();
            $table->boolean('is_active')->default(1);
            $table->timestamps();
            
            $table->foreign('role_id')->references('id')->on('userrole');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('usertype');
    }
}
