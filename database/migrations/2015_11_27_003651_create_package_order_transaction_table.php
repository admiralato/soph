<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackageOrderTransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('package_order_transaction', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('member_package_id')->unsigned();
            $table->integer('quantity');
            $table->decimal('sub_total', 10, 2);
            $table->timestamps();
            $table->boolean('is_cancelled');
            $table->integer('attended_by_id')->unsigned();
            
            $table->foreign('member_package_id')->references('id')->on('member_package');
            $table->foreign('attended_by_id')->references('id')->on('profile');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('package_order_transaction');
    }
}
