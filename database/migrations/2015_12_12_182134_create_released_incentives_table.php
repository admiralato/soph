<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReleasedIncentivesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('released_incentives', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('member_id')->unsigned();
            $table->decimal('grand_total', 10, 2);
            $table->integer('released_by_id')->unsigned();
            $table->timestamps();
            
            $table->foreign('released_by_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('released_incentives');
    }
}
