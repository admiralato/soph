<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profile', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('profile_key',255)->unique();
            $table->string('first_name',150);
            $table->string('last_name',150)->nullable();
            $table->string('middle_name',150)->nullable();
            $table->string('nick_name',150)->nullable();
            $table->string('profile_pic_url',500)->default('');
            $table->integer('age');
            $table->date('date_of_birth');
            $table->date('registration_date');
            $table->enum('gender', ['Male', 'Female']);
            $table->enum('status', ['Single', 'Married', 'Separated', 'Divorced', 'Widowed']);
            $table->string('address_primary',500);
            $table->string('address_secondary',500)->nullable();
            $table->string('contact_no',20)->nullable();
            $table->string('mobile_no',20)->nullable();
            $table->string('email',100);
            $table->string('zip',50)->nullable();
            $table->string('country',20);
            $table->timestamps();
            $table->softDeletes();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('profile');
    }
}
