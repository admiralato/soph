<?php

use Illuminate\Database\Seeder;

class UserRoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('userrole')->insert([
            'title' => 'root',
            'route_ids' => 'dashboard,packages,incentives,profile,sales,downlines,member,bonus,settings,usermanager,users,roles,types,preferences',
            'is_active' => 1,
            'created_at' => Carbon\Carbon::now(),
            'updated_at' => Carbon\Carbon::now()
		]);
    }
}
