<?php

use Illuminate\Database\Seeder;

class PackagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('packages')->insert([
            'title' => 1,
            'amount' => 1800,
            'first_fifteen' => 800,
            'second_fifteen' => 800,
            'third_fifteen' => 800,
            'color' => 'blue',
            'created_at' => Carbon\Carbon::now(),
            'updated_at' => Carbon\Carbon::now()
		]);
        
        DB::table('packages')->insert([
            'title' => 3,
            'amount' => 5400,
            'first_fifteen' => 2400,
            'second_fifteen' => 2400,
            'third_fifteen' => 2400,
            'color' => 'purple-active',
            'created_at' => Carbon\Carbon::now(),
            'updated_at' => Carbon\Carbon::now()
		]);
        
        DB::table('packages')->insert([
            'title' => 7,
            'amount' => 12600,
            'first_fifteen' => 5600,
            'second_fifteen' => 5600,
            'third_fifteen' => 5600,
            'color' => 'yellow-active',
            'created_at' => Carbon\Carbon::now(),
            'updated_at' => Carbon\Carbon::now()
		]);
        
        DB::table('packages')->insert([
            'title' => 9,
            'amount' => 16200,
            'first_fifteen' => 7200,
            'second_fifteen' => 7200,
            'third_fifteen' => 7200,
            'color' => 'navy',
            'created_at' => Carbon\Carbon::now(),
            'updated_at' => Carbon\Carbon::now()
		]);
        
        DB::table('packages')->insert([
            'title' => 12,
            'amount' => 21600,
            'first_fifteen' => 9600,
            'second_fifteen' => 9600,
            'third_fifteen' => 9600,
            'color' => 'olive',
            'created_at' => Carbon\Carbon::now(),
            'updated_at' => Carbon\Carbon::now()
		]);
        
        DB::table('packages')->insert([
            'title' => 15,
            'amount' => 27000,
            'first_fifteen' => 12000,
            'second_fifteen' => 12000,
            'third_fifteen' => 12000,
            'color' => 'red-active',
            'created_at' => Carbon\Carbon::now(),
            'updated_at' => Carbon\Carbon::now()
		]);
        
        DB::table('packages')->insert([
            'title' => 20,
            'amount' => 36000,
            'first_fifteen' => 16000,
            'second_fifteen' => 16000,
            'third_fifteen' => 16000,
            'color' => 'green-active',
            'created_at' => Carbon\Carbon::now(),
            'updated_at' => Carbon\Carbon::now()
		]);
        
        DB::table('packages')->insert([
            'title' => 30,
            'amount' => 54000,
            'first_fifteen' => 24000,
            'second_fifteen' => 24000,
            'third_fifteen' => 24000,
            'color' => 'maroon-active',
            'created_at' => Carbon\Carbon::now(),
            'updated_at' => Carbon\Carbon::now()
		]);
        
        DB::table('packages')->insert([
            'title' => 40,
            'amount' => 72000,
            'first_fifteen' => 32000,
            'second_fifteen' => 32000,
            'third_fifteen' => 32000,
            'color' => 'black',
            'created_at' => Carbon\Carbon::now(),
            'updated_at' => Carbon\Carbon::now()
		]);
        
        DB::table('packages')->insert([
            'title' => 50,
            'amount' => 90000,
            'first_fifteen' => 40000,
            'second_fifteen' => 40000,
            'third_fifteen' => 40000,
            'color' => 'teal',
            'created_at' => Carbon\Carbon::now(),
            'updated_at' => Carbon\Carbon::now()
		]);
        
        DB::table('packages')->insert([
            'title' => 100,
            'amount' => 180000,
            'first_fifteen' => 80000,
            'second_fifteen' => 80000,
            'third_fifteen' => 80000,
            'color' => 'orange',
            'created_at' => Carbon\Carbon::now(),
            'updated_at' => Carbon\Carbon::now()
		]);
    }
}
