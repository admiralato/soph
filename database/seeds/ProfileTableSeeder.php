<?php

use Illuminate\Database\Seeder;

class ProfileTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('profile')->insert([
            'profile_key' => 0,
            'first_name' => 'Power',
            'last_name' => 'User',
            'middle_name' => 'Root',
            'age' => 32,
            'gender' => 'Male',
            'status' => 'Single',
            'date_of_birth' => Carbon\Carbon::now(),
            'created_at' => Carbon\Carbon::now()
		]);
    }
}
