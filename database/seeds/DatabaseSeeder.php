<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(ProfileTableSeeder::class);
        $this->call(MemberTableSeeder::class);
        $this->call(UserRoleTableSeeder::class);
        $this->call(UserTypeTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(PackagesTableSeeder::class);
        $this->call(IncentivesTableSeeder::class);
        
        Model::reguard();
    }
}
