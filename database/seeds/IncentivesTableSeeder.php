<?php

use Illuminate\Database\Seeder;

class IncentivesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('incentives')->insert([
            'level' => 1,
            'incentive' => 100,
            'created_at' => Carbon\Carbon::now(),
            'updated_at' => Carbon\Carbon::now()
		]);
        
        DB::table('incentives')->insert([
            'level' => 2,
            'incentive' => 10,
            'created_at' => Carbon\Carbon::now(),
            'updated_at' => Carbon\Carbon::now()
		]);
        
        DB::table('incentives')->insert([
            'level' => 3,
            'incentive' => 10,
            'created_at' => Carbon\Carbon::now(),
            'updated_at' => Carbon\Carbon::now()
		]);
        
        DB::table('incentives')->insert([
            'level' => 4,
            'incentive' => 50,
            'created_at' => Carbon\Carbon::now(),
            'updated_at' => Carbon\Carbon::now()
		]);
        
        DB::table('incentives')->insert([
            'level' => 5,
            'incentive' => 5,
            'created_at' => Carbon\Carbon::now(),
            'updated_at' => Carbon\Carbon::now()
		]);
        
        DB::table('incentives')->insert([
            'level' => 6,
            'incentive' => 5,
            'created_at' => Carbon\Carbon::now(),
            'updated_at' => Carbon\Carbon::now()
		]);
        
        DB::table('incentives')->insert([
            'level' => 7,
            'incentive' => 5,
            'created_at' => Carbon\Carbon::now(),
            'updated_at' => Carbon\Carbon::now()
		]);
        
        DB::table('incentives')->insert([
            'level' => 8,
            'incentive' => 5,
            'created_at' => Carbon\Carbon::now(),
            'updated_at' => Carbon\Carbon::now()
		]);
        
        DB::table('incentives')->insert([
            'level' => 9,
            'incentive' => 5,
            'created_at' => Carbon\Carbon::now(),
            'updated_at' => Carbon\Carbon::now()
		]);
        
        DB::table('incentives')->insert([
            'level' => 10,
            'incentive' => 5,
            'created_at' => Carbon\Carbon::now(),
            'updated_at' => Carbon\Carbon::now()
		]);
    }
}
