<?php

use Illuminate\Database\Seeder;

class MemberTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //DEQUODE getdate()
        //Top Level
        DB::table('member')->insert([
            'parent_id' => 0,
            'profile_id' => 1,
            'group_code' => 'DEQUODE',
            'created_at' => Carbon\Carbon::now()
		]);
    }
}
