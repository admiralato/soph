<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Power User',
            'email' => 'admin@dequode.com',
            'password' => bcrypt('111111'),
            'profile_id' => 1,
            'usertype_id' => 1,
            'created_at' => Carbon\Carbon::now(),
            'updated_at' => Carbon\Carbon::now()
		]);
    }
}
