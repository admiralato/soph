<?php  

namespace App\Helpers;

use Config;

class Util
{
	/*
	|--------------------------------------------------------------------------
	| Get Date Time
	|--------------------------------------------------------------------------
	*/
	public static function getDateTimeFormat($format)
	{
		return date($format);
	}

	/*
	|--------------------------------------------------------------------------
	| Get XML File Info
	|--------------------------------------------------------------------------
	*/
	public static function simpleXmlToObject($xmlFullPath, $title, $child)
	{
		try {
			if( file_exists($xmlFullPath)) {
				$xml = simplexml_load_file($xmlFullPath);
			}
			
		} catch (Exception $e) {
			Log::error($e->getTraceAsString());
			$xml = null;
		}
		
		return $xml;
	}

	public static function getHtmlMenu($arrRoutes)
	{
		$apptreehtml = '';
		$xmlPath = storage_path()."/xml/Modules.xml";

		try {
			$base_url = url();
			$collection = Util::simpleXmlToObject($xmlPath, '', '');

			foreach ($collection as $item => $value) {
				if ($value == null || empty($value)) $apptreehtml = '';
				//Check if this menu item is in the user roles route ids
				if (in_array((string)$value['id'], $arrRoutes, true)) { 
					//Start building menu item
					$active = (string)$value['id'] == 'dashboard' ? '' : '';
					if ($value['lastchild'] != null && intval($value['lastchild']) ==1) { // lastchild
                        if($value['class'] != null && (string)$value['class'] == 'header') {
                            $apptreehtml .= '<li class="'. (string)$value['class'] . '">' . (string)$value['title'] . '</li>';
                        } 
                        else {
                        $apptreehtml .= '<li><a href="'. $base_url . (string)$value['url'] .'" '.  $active  .'><i class="'.(string)$value['icon'].'"></i>'. (string)$value['title']  .'</a></li>';
                        }
					} // --- end of lastchild 
                    else {
						$apptreehtml .= '<li class="treeview"><a href="#"><i class="' . (string)$value['icon'] . '"></i>';
                        $apptreehtml .= '<span>' . (string)$value['title'] . '</span>';
                        $apptreehtml .= '<i class="fa fa-angle-left pull-right"></i></a>';
						$apptreehtml .= '<ul class="treeview-menu">';
						foreach ($value as $item => $child) {
							if (in_array((string)$child['id'], $arrRoutes, true)) { 
								$apptreehtml .= '<li><a href="'. $base_url . (string)$child['url'] .'"><i class="fa fa-circle-o"></i> ' . (string)$child['title']  .'</a></li>';
							}
						}
						$apptreehtml .= '</ul>';
                        $apptreehtml .= '</li>';
					}

				}

				
			}
			
		} catch (Exception $e) {
			
		}

		return $apptreehtml;
	}

	public static function getHtmlPanelMenu($arrRoutes)
	{
		$apppanelhtml = '';
		$xmlPath = storage_path()."/xml/Modules.xml";

		try {
			$base_url = Config::get('app.base_url');
			$panelFormat = '<div class="col-lg-3 col-md-6"><div class="%s"><div class="panel-heading"><div class="row"><div class="col-xs-3"><i class="%s"></i></div><div class="col-xs-9 text-right"><div class="huge">%s</div><div>%s</div></div></div></div><a href="'. $base_url .'%s"><div class="panel-footer"><span class="pull-left">View Details</span><span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span><div class="clearfix"></div></div></a></div></div>';
			$collection = Util::simpleXmlToObject($xmlPath, '', '');

			foreach ($collection as $item => $value) {
				if ($value == null || empty($value)) $panelFormat = '';

				//Check if this menu item is in the user roles route ids
				if (in_array((string)$value['id'], $arrRoutes, true) ) { 
					//Start building menu item
					if ($value['lastchild'] != null && intval($value['lastchild']) ==1 && $value['classpanel'] !=null) {
						$apppanelhtml .= sprintf($panelFormat, (string)$value['classpanel'], str_replace("fa-fw","fa-3x",(string)$value['icon']) , '', (string)$value['title'], (string)$value['url'] );
						
					} else {
						
						foreach ($value as $item => $child) {
							if (in_array((string)$child['id'], $arrRoutes, true) && $child['classpanel'] !=null) { 
								$apppanelhtml .= sprintf($panelFormat, (string)$child['classpanel'], (string)$child['icon'], '', (string)$child['title'], (string)$child['url'] );
							}
						}
					}

				}

				
			}
			
		} catch (Exception $e) {
			
		}

		return $apppanelhtml;
	}
}