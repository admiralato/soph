<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.

Route::get('/', function () {
    return view('welcome');
});

|
*/
// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');

Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController',
]);

/*
|--------------------------------------------------------------------------
| Index / Home Page
|--------------------------------------------------------------------------
*/
Route::get('/', ['middleware' => 'auth', 'uses' => 'AppControllers\IndexController@index']);
Route::get('/home',['middleware' => 'auth', 'uses' => 'AppControllers\IndexController@index']);
Route::get('/dashboard', ['middleware' => 'auth', 'uses' => 'AppControllers\IndexController@index']);

/*
|--------------------------------------------------------------------------
| Index / Profile Manager Page
|--------------------------------------------------------------------------
*/
Route::bind('profile', function($id){
	return App\AppCore\Models\Profile::where('id',$id)->first();
});
$router->resource('profile','AppControllers\ProfileController', [
	'only' => ['index', 'show', 'edit', 'create', 'store', 'update', 'destroy']
]);

/*
|--------------------------------------------------------------------------
| Index / Package Manager Page
|--------------------------------------------------------------------------
*/
Route::bind('packages', function($id){
	return App\AppCore\Models\Package::where('id',$id)->first();
});
$router->resource('packages','AppControllers\PackageController', [
        'only' => ['index', 'show', 'edit', 'create', 'store', 'update', 'destroy']
]);

/*
|--------------------------------------------------------------------------
| Index / Member Manager Page
|--------------------------------------------------------------------------
*/
Route::bind('member', function($id){
	return App\AppCore\Models\Member::where('id',$id)->first();
});
$router->resource('member','AppControllers\MemberController', [
        'only' => ['index', 'show', 'edit', 'create', 'store', 'update', 'destroy']
]);
Route::get('member/create/{parentid?}', 'AppControllers\MemberController@create');
Route::get('member/change-sponsor/{memberid?}', 'AppControllers\MemberController@changeSponsor');
Route::get('member/search/{action}', 'AppControllers\MemberController@profileNameSearch')->where('action', '.*');

Route::post('member/search', 'AppControllers\MemberController@postSearch');
Route::post('member/delete', 'AppControllers\MemberController@destroy');
Route::post('member/updateSponsor', 'AppControllers\MemberController@updateSponsor');

/*
|--------------------------------------------------------------------------
| Index / Incentive Manager Page
|--------------------------------------------------------------------------
*/
Route::bind('incentives', function($id){
	return App\AppCore\Models\Incentive::where('id',$id)->first();
});
$router->resource('incentives','AppControllers\IncentiveController', [
        'only' => ['index', 'show', 'edit', 'create', 'store', 'update', 'destroy']
]);

/*
|--------------------------------------------------------------------------
| Index / Downlines Page
|--------------------------------------------------------------------------
*/
Route::get('/downlines/{action?}', 'AppControllers\DownlinesController@index');

/*
|--------------------------------------------------------------------------
| Ajax Search
|--------------------------------------------------------------------------
*/
Route::get('/api/search/{action}', 'AppControllers\AjaxSearchController@profileNameSearch')->where('action', '.*');
Route::get('/api/downlines/{action?}', 'AppControllers\AjaxSearchController@getDownlines')->where('action', '.*');


/*
|--------------------------------------------------------------------------
| Index / User Role Manager Page
|--------------------------------------------------------------------------
*/
Route::bind('userrole', function($id){
    return App\AppCore\Models\Userrole::where('id', $id)->first();
});
$router->resource('userrole','AppControllers\UserRolesController', [
    'only' => ['index', 'show', 'edit', 'create', 'store', 'update', 'destroy']
]);

/*
|--------------------------------------------------------------------------
| Index / User Type Manager Page
|--------------------------------------------------------------------------
*/
Route::bind('usertype', function($id){
    return App\AppCore\Models\Usertype::where('id',$id)->first();
});
$router->resource('usertype','AppControllers\UserTypeController', [
    'only' => ['index', 'show', 'edit', 'create', 'store', 'update', 'destroy']
]);

/*
|--------------------------------------------------------------------------
| Index / User Accounts Manager Page
|--------------------------------------------------------------------------
*/
Route::bind('useraccounts', function($id){
    return App\User::where('id',$id)->first();
});
$router->resource('useraccounts','AppControllers\AccountController', [
    'only' => ['index', 'show', 'edit', 'create', 'store', 'update', 'destroy']
]);

/*
|--------------------------------------------------------------------------
| Index / Incentives Computation Page
|--------------------------------------------------------------------------
*/
Route::get('/bonus/{action?}', 'AppControllers\BonusController@index');
Route::get('/bonus/search/{action}', 'AppControllers\BonusController@profileNameSearch')->where('action', '.*');
Route::get('/bonus/member/incentive/{action?}', 'AppControllers\BonusController@getDownlines')->where('action', '.*');
Route::get('/bonus/group/{action?}', 'AppControllers\BonusController@getGroupLevel')->where('action', '.*');
Route::get('/bonus/reports/claims', 'AppControllers\BonusController@viewReport');
Route::get('/bonus/reports/weekly_incentive', 'AppControllers\BonusController@viewWeeklyReport');
Route::get('/bonus/referral/print/{action?}', 'AppControllers\BonusController@printPreview')->where('action', '.*');

Route::post('/bonus/release', 'AppControllers\BonusController@store');
Route::post('/bonus/sales_details', 'AppControllers\BonusController@totalPurchases');
Route::post('/bonus/search/referrals', 'AppControllers\BonusController@getDownlinesSequentially');
Route::post('/bonus/search/active_members', 'AppControllers\BonusController@displayActiveMembersWithIncentives');
Route::post('/bonus/get/referrals', 'AppControllers\BonusController@getDownlinePurchases');
Route::post('/bonus/search/remaining-level', 'AppControllers\BonusController@getRemainingDownlines');
Route::post('/bonus/get/remaining-purchases', 'AppControllers\BonusController@getRemainingPurchases');
Route::post('/bonus/compute-member/incentive', 'AppControllers\BonusController@computeTotalIncentivesPerMember');

Route::post('bonus/print/referral-claim', 'AppControllers\BonusController@printReferral');
Route::post('bonus/export/referral-claim', 'AppControllers\BonusController@exportReferral');
/*
|--------------------------------------------------------------------------
| Product Catalog Page
|--------------------------------------------------------------------------
*/
Route::get('/catalog/{action?}', 'AppControllers\ProductCatalogController@index');
Route::post('cart', 'AppControllers\CartController@store');
Route::post('membershort', 'AppControllers\CartController@shortStore');
/*
|--------------------------------------------------------------------------
| Sales
|--------------------------------------------------------------------------
*/
Route::bind('sales', function($id){
    return App\AppCore\Models\Sales::where('id',$id)->first();
});
$router->resource('sales','AppControllers\SalesController', [
        'only' => ['index', 'show', 'edit', 'create', 'store', 'update', 'destroy']
]);
Route::get('sales/{id}/destroy', 'AppControllers\SalesController@delete');
/*
|--------------------------------------------------------------------------
| Ajax Data
|--------------------------------------------------------------------------
*/
Route::get('/api/data/saleable-package/{action}', 'AppControllers\AjaxDataController@getSaleablePackage')->where('action', '.*');
Route::get('/api/data/members/{action}', 'AppControllers\AjaxDataController@getActiveMembers')->where('action', '.*');
Route::get('/api/data/packages/{action}', 'AppControllers\AjaxDataController@getPackages')->where('action', '.*');

Route::get('testing', 'AppControllers\TestingController@index');

/*
|--------------------------------------------------------------------------
| Inquiry: Released Incentives
|--------------------------------------------------------------------------
*/
Route::bind('released_incentives', function($id){
	return App\AppCore\Models\ReleasedIncentives::where('id',$id)->first();
});
$router->resource('released_incentives','AppControllers\ReleasedIncentivesController', [
        'only' => ['index', 'show', 'edit', 'create', 'store', 'update', 'destroy']
]);
Route::post('/released_incentives/sales_details', 'AppControllers\ReleasedIncentivesController@getSalesDetails');
Route::post('/released_incentives/details', 'AppControllers\ReleasedIncentivesController@getPurchases');
Route::post('/inquiry/get/referrals', 'AppControllers\MemberInquiryController@getMemberDownlines');
Route::post('/inquiry/get/remaining-purchases', 'AppControllers\MemberInquiryController@getRemainingDownlines');
Route::post('/released_incentives/delete', 'AppControllers\ReleasedIncentivesController@destroy');

Route::get('/inquiry/{action?}', 'AppControllers\MemberInquiryController@index');
Route::get('/inquiry/purchases/{action?}', 'AppControllers\MemberInquiryController@getAjaxPurchases')->where('action', '.*');
Route::get('/inquiry/incentives/{action?}', 'AppControllers\MemberInquiryController@getAjaxIncentives')->where('action', '.*');
//Route::get('/inquiry/downlines/{action?}', 'AppControllers\MemberInquiryController@getAjaxDownlines')->where('action', '.*');
Route::get('/inquiry/search/{action}', 'AppControllers\MemberInquiryController@profileNameSearch')->where('action', '.*');
$router->resource('member_inquiry','AppControllers\MemberInquiryController', [
        'only' => ['index', 'show', 'edit', 'create', 'store', 'update', 'destroy']
]);


Route::get('/test', function(){
   
	return 'Welcome to Uskoop';

});