<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\RedirectResponse;
use App\User;

class RedirectIfAuthenticated {

	/**
	 * The Guard implementation.
	 *
	 * @var Guard
	 */
	protected $auth;

	/**
	 * Create a new filter instance.
	 *
	 * @param  Guard  $auth
	 * @return void
	 */
	public function __construct(Guard $auth)
	{
		$this->auth = $auth;
	}

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		/*
		if ($this->auth->check())
		{
			return new RedirectResponse(url('/home'));
		}

		return $next($request);
		(*/
		if (($this->auth->check() && $this->auth->user()->is_active==1) or $request->is("auth/login"))
		{
			if ($this->validateAccessRoute(strtolower($request->path())) ==false ){
				return \Response::make('403 Forbidden or No Permission to Access!',403);
			} else {
				return $next($request);	
			}
			
		}
		else if (($request->is("auth/register") && User::get()->count() == 0) | $this->auth->check() )
		{
			return $next($request);
		} else {
			//dd($request->url());
		    return redirect()->guest('auth/login');
		}
	}

	private function validateAccessRoute($requestUrl){
		

		if ($this->auth->user() ==null | strpos($requestUrl,'auth/logout') >-1 | strpos($requestUrl,'auth/login') >-1 | $requestUrl == '/' | $requestUrl == 'home') { return true; }

		$route_ids = $this->auth->user()->getUser($this->auth->user()->id)->route_ids;

		$allowAccess = false;

		if ($this->auth->user()->getUser($this->auth->user()->id)->userrole_id == 1 && (strpos($requestUrl,'userrole') >-1 | strpos($requestUrl,'usertype') >-1)  ) {
			return true;
		}


		if (strlen($route_ids) > 0) {
			$arr = explode(',',$route_ids);
			
			if ( count($arr) > 0) { 

				for ($i=0; $i < count($arr); $i++) { 
					
					$pos = strpos($requestUrl, strtolower($arr[$i]) );
					//dd($requestUrl, $route_ids,$arr[$i],$pos);
    				if ($pos > -1) {
    					$allowAccess = true;
    					break;
    				}

				}

			} else {
				$allowAccess = false;
			}
		}

		return $allowAccess;
	
	}

}
