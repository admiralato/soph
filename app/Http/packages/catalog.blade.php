@extends('layouts.master')
@section('title')
    SOPH Marketing Global Admin Panel :: {{ isset($pageTitle) ? $pageTitle : 'Product Catalogsss' }}
@stop
@section('content')
    @include('include.content_header_block')

    <section class="content">
        <div class="row">
        	
        	@if (Session::has('message'))
        	<div class="col-lg-12">
	            <div class="alert alert-{{ Session::get('classstyle') }} alert-dismissable">
	                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	                {{ Session::get('message') }}
	            </div>
            </div>
            @endif

            <div class="col-lg-8">
                <div class="box box-warning">
                    <div class="box-header with-border">
                    	<h3 class="box-title"><i class="fa fa-dropbox"></i> Products/Packages</h3><span id="member-id" class="badge pull-right bg-purple">{{ ( $packages != null? 'Count: ' . count($packages) : '#' ) }}</span>
                    </div>
                    <div class="box-body">
                    	@include ('content.packages.products')
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
            	{!! Form::open(['url'=>'cart','method'=>'POST','role'=>'form']) !!}
                <div class="box box-success">
                	<div class="box-header with-border">
                    	<h3 class="box-title"><i class="fa fa-user"></i> Member</h3><span id="member-id" class="badge pull-right bg-orange">{{ ( $member != null? '#' . $member->id: '#' ) }}</span>
                    </div>
                    <div class="box-body">
                    	{!! Form::hidden('member_id', ( $member != null? $member->id: null )) !!}
                    	{!! Form::hidden('profile_id', ( $member != null? $member->profile_id: null )) !!}
	                    
		              	<div class="input-group">
			                <div class="input-group-addon">
			                    <i class="fa fa-search"></i>
			                </div>
			                {!! Form::text('member_profile', ( $member != null? $member->name: null ), ['placeholder'=>'Search member name', 'class'=>'form-control', 'onkeyup'=>'up(this)', 'onkeydown'=>'down(this)', 'autocomplete'=>'off']) !!}
			            </div>
			            <ul id="member-profile" class="dropdown-menu" style="top:83px;">
					    </ul>
                    </div>
                </div>
                <div class="box box-info">
                    <div class="box-header with-border">
                    	<h3 class="box-title"><i class="fa fa-shopping-cart"></i> Cart</h3><span id="cart-count" class="badge pull-right bg-navy">0</span>
                    	@if ($errors->any())
                            <ul class="alert alert-danger" style="list-style:none">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif
                    </div>
		            @include ('content.packages.cart')
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </section>
@stop
@section('jsaddon')
{!! Html::script('resources/assets/plugins/date/jquery.plugin.min.js') !!}
{!! Html::script('resources/assets/plugins/date/jquery.dateentry.min.js') !!}
<script type="text/javascript">
	$(function() {

		$('input[name="created_at"]').dateEntry({dateFormat: 'ymd-'});

		$( 'a.add-cart' ).on('click', function(e) {
		    var id = $( this ).attr('id');
		    var sku = $( this ).attr('data-sku');
		    var amount = $( this ).attr('data-amount');
		    var packages = $('input[name="packages"]').val();
		    /*
			<tr id="package-id-1" data-amount="220">
		        <td><a href="#" title="Remove" class="x-cart-item" id="1"><i class="fa fa-close fa-fw"></i></a></td>
		        <td>Package 1</td>
		        <td>12,000.00</td>
		      </tr>
		    */
		    var html = '<tr id="package-id-'+ id +'" data-amount="'+ amount +'">';
		    html += '<td><a href="#" title="Remove" class="x-cart-item" id="'+ id +'"><i class="fa fa-close fa-fw"></i></a></td>';
		    html += '<td>Package '+ sku +'</td>';
		    html += '<td>'+ amount +'</td>';
		    html += '</tr>';

		    $( html ).appendTo('table#cart-items > tbody');
		    $( '#place-order').removeClass('disabled');
		    updateCart();
		    alert('Package '+ sku +' was successfully added in the cart.');
		});

		$(document).on('click', 'a.x-cart-item', function(){
			var id = $( this ).attr('id');			
			$('#package-id-'+id).remove();
			updateCart();
		});
	
		var member = $('input[name="member_id"]').val();
		if (member && parseInt(member)>-1){
			$( '#place-order').removeClass('disabled');
		}

	});

	function updateCart(){
		var total =0;
		var packages = '';
		var count =0;
		$('table#cart-items > tbody tr').each(function( index ) {
			if ($( this ).attr('id') != undefined) {
				var id = $( this ).attr('id');
				var amount = $( this ).attr('data-amount');
				amount = amount.replace(',','');
				console.log(amount);
				console.log(parseFloat(amount));
				total = parseFloat(total) + parseFloat(amount);
				id = id.replace("package-id-", "")
				packages += (packages.length > 0? ',': '') + id;
				count +=1;
				//console.log( index + ": " + id  + " : " + amount );
			}
		});

		$('input[name="packages"]').val(packages);
		$('#grand-total').html(numberWithCommas(total));
		$('#cart-count').html(count);

		if (total <= 0){
			$( '#place-order').addClass('disabled');
		}

		//console.log("packages: " + packages  + " : " + total );
	}

	function numberWithCommas(x) {
	    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	}

	var keyTimer;

	function up(obj) {
		keyTimer = setTimeout(function() {
			var keywords = $(obj).val();
			keywords = encodeURI(keywords);
			if (keywords.length > 1){
				$.get("{{ url('api/search/member') }}?q=" + keywords + "&catalog=1", function(data){
					if (data.length > 0) {
					    $('#member-profile').html(data);
					    $('#member-profile').dropdown().toggle();   
					}
				});
			}
		}, 300);
	}

	function down(obj){
		clearTimeout(keyTimer);
	}


	function loadUrl(url){
		window.location.href = url;
		return false;
	}
</script>
@stop