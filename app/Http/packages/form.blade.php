<div class="box-body">
    <div class="form-group">
        {!! Form::label('packageno', 'Package') !!}
        {!! Form::number('packageno', $item->title, ['id'=>'packageno', 'placeholder'=>'Product packages', 'class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('amount', 'Amount') !!}
        {!! Form::text('amount', $item->amount, ['id'=>'amount', 'placeholder'=>'Enter amount', 'class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('first_fifteen', '15 Days') !!}
        {!! Form::text('first_fifteen', $item->first_fifteen, ['id'=>'first_fifteen', 'placeholder'=>'Enter amount', 'class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('second_fifteen', '15 Days') !!}
        {!! Form::text('second_fifteen', $item->second_fifteen, ['id'=>'second_fifteen', 'placeholder'=>'Enter amount', 'class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('third_fifteen', '15 Days') !!}
        {!! Form::text('third_fifteen', $item->third_fifteen, ['id'=>'third_fifteen', 'placeholder'=>'Enter amount', 'class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('color', 'Color') !!}
        {!! Form::select('color', array('maroon-active'=>'maroon-active', 'purple-active' => 'purple-active','orange' => 'orange','black' => 'black','fuchsia' => 'fuchsia','olive' => 'olive','blue' => 'blue','teal' => 'teal','navy' => 'navy','yellow-active' => 'yellow-active','red-active' => 'red-active','gray-active' => 'gray-active','green' => 'green','green-active' => 'green-active','yellow' => 'yellow'  ), $item->color, ['class'=>'form-control'] ) !!}
    </div>
</div><!-- /.box-body -->