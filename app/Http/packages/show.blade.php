@extends('layouts.master')
@section('title')
    SOPH Marketing Global Admin Panel :: {{ isset($pageTitle) ? $pageTitle : 'Packages' }}
@stop
@section('content')
    @include('include.content_header_block')
<section class="content">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Packages</h3>
                </div><!-- /.box-header -->
                @if ($errors->any())
                    <ul class="alert alert-danger" style="list-style:none">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                @endif
                <div class="box-body">
                    <div class="form-group">
                        {!! Form::label('packageno', 'Package') !!}
                        {!! Form::number('packageno', $item->title, ['id'=>'packageno', 'placeholder'=>'Package number', 'class'=>'form-control', 'disabled']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('amount', 'Amount') !!}
                        {!! Form::text('amount', $item->amount, ['id'=>'amount', 'placeholder'=>'Enter amount', 'class'=>'form-control', 'disabled']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('first_fifteen', '15 Days') !!}
                        {!! Form::text('first_fifteen', $item->first_fifteen, ['id'=>'first_fifteen', 'placeholder'=>'Enter amount', 'class'=>'form-control', 'disabled']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('second_fifteen', '15 Days') !!}
                        {!! Form::text('second_fifteen', $item->second_fifteen, ['id'=>'second_fifteen', 'placeholder'=>'Enter amount', 'class'=>'form-control', 'disabled']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('third_fifteen', '15 Days') !!}
                        {!! Form::text('third_fifteen', $item->third_fifteen, ['id'=>'third_fifteen', 'placeholder'=>'Enter amount', 'class'=>'form-control', 'disabled']) !!}
                    </div>
                </div><!-- /.box-body -->
                <div class="box-footer">
                    <a href="{{ URL::previous() }}" class="btn btn-success btn-sm">Back</a>
                </div>
            </div>
        </div>
    </div>
</section>
@stop