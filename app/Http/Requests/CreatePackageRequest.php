<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CreatePackageRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'packageno' => 'unique:packages,title|required|numeric|min:1',
            'amount' => 'required|numeric|min:1',
            'first_fifteen' => 'required|numeric|min:1',
            'second_fifteen' => 'required|numeric|min:1',
            'third_fifteen' => 'required|numeric|min:1'
        ];
    }
}
