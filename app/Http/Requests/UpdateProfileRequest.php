<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UpdateProfileRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        //dd(Request::get('id'));
        return [
            'first_name'=>'required|min:2|max:150', 
            'last_name'=>'required|min:2|max:150', 
            'date_of_birth'=>'required|date_format:"Y-m-d"|before:"now"', 
            'registration_date'=>'required|date_format:"Y-m-d"', 
            'age'=>'required|numeric|min:18', 
            'gender'=>'required', 
            'email' => 'email',
            'contact_no'=>'required', 
            'mobile_no'=>'required',
            'address_primary'=>'required|min:6',            
        ];
    }
}
