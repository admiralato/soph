<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CreateIncentiveRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'level' => 'unique:incentives|required|min:1|numeric',
            'incentive' => 'required|min:1|numeric',
        ];
    }
}
