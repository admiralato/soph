<?php

namespace App\Http\Controllers\AppControllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\AppCore\Models\Incentive;

class IncentiveController extends Controller
{
    private $incentive;
    
    public function __construct(Incentive $item)
    {
        $this->incentive = $item;
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!Auth::check()) return view('auth.login');
        
        $pageTitle = 'Incentive Manager';
        try {
            $incentives = Incentive::all(); // doesn't include deleted_at != null
            return view('content.incentives.index', compact('pageTitle', 'incentives'));
        } catch (Exception $e) {
            Error.log($e);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(!Auth::check()) return view('auth.login');
        
        $pageTitle = 'Create Incentive';
        $item = new Incentive;
        return view('content.incentives.create', compact('pageTitle', 'item'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\CreateIncentiveRequest $request)
    {
        if(!Auth::check()) return view('auth.login');
        
        DB::beginTransaction();
        try
        {
            $incentive = new Incentive;
            $incentive->level = $request->level;
            $incentive->incentive = $request->incentive;
            $incentive->save();  

            DB::commit();
            $response = 'Record was successfully saved!';
        } catch(Exception $e) {
            $response = $e->getMessage();
            DB::rollback();
        }
        return redirect('incentives');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Incentive $item)
    {
        if(!Auth::check()) return view('auth.login');
        
        $pageTitle = 'View Incentive';
        try {
            if (!empty($item) && $item != null){
                return view('content.incentives.show', compact('item','pageTitle'));
            }
        } catch (Exception $e) {
            Error.log($e);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Incentive $item)
    {
        if(!Auth::check()) return view('auth.login');
        
        $pageTitle = 'Edit Incentive';
        try {
            if (!empty($item) && $item != null){
                return view('content.incentives.edit', compact('item','pageTitle'));
            }
        } catch (Exception $e) {
            Error.log($e);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Incentive $model, Requests\UpdateIncentiveRequest $request)
    {
        if(!Auth::check()) return view('auth.login');
        $response = 'Null';
        $classstyle = 'danger';
        
        if (!empty($request->get('delete')) && !empty($model))
        {
            # code to delete selected item
            return $this->destroy($model);
        } else if(!empty($request) && !empty($model)) {
            # code to update selected item
            DB::beginTransaction();
            try {

                $model->level = $request->level;
                $model->incentive = $request->incentive;
                $model->save();
                DB::commit();

                $response = 'Record was successfuly updated...';
                $classstyle = 'success';
            } catch(Exception $e) {
                $response = $e->getMessage();
                DB::rollback();
            }
        }

        return redirect('incentives')->with('message',$response)->with('classstyle',$classstyle);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Incentive $item)
    {
        if(!Auth::check()) return view('auth.login');
        Incentive::destroy($item->id);

        $response = 'Incentive level '. $item->level .' was successfuly deleted...';
        $classstyle = 'success';
        return redirect('incentives')->with('message',$response)->with('classstyle',$classstyle);
    }
}
