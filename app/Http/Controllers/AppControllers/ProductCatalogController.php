<?php

namespace App\Http\Controllers\AppControllers;

use Illuminate\Http\Request;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;

use App\AppCore\Models\Package;

class ProductCatalogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(!Auth::check()) return view('auth.login');
        
        $pageTitle = 'Product Catalog';
        $memberId = -1;
        $member = null; $packages = null;
        try {
            if ($request && $request->get('id') != null && is_numeric($request->get('id'))) {
                $memberId = $request->get('id');
                $query = "SELECT m.id AS id, m.parent_id, m.profile_id, CONCAT(p.first_name,' ',p.last_name) AS 'name' FROM member m LEFT JOIN profile p ON m.profile_id = p.id WHERE p.deleted_at IS NULL AND m.id = $memberId";
                $member = DB::select($query);
                
                if ($member != null && count($member)>0) 
                    $member = $member[0];
            }

            $query = "SELECT id, title, FORMAT(amount,2) AS 'amount', FORMAT(first_fifteen,2) AS 'first_fifteen', FORMAT(second_fifteen,2) AS 'second_fifteen', FORMAT(third_fifteen,2) AS 'third_fifteen', color   FROM packages WHERE deleted_at IS NULL order by packages.title";
            $packages = DB::select($query);
            
        } catch (Exception $e) {
            Error.log($e);
        } 

        return view('content.packages.catalog', compact('pageTitle','member','packages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
