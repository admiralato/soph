<?php

namespace App\Http\Controllers\AppControllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use App\AppCore\Models\Sales;
use App\AppCore\Models\Salesdetails;
use App\AppCore\Models\Profile;
use App\AppCore\Models\Member;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!Auth::check()) return view('auth.login');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(!Auth::check()) return view('auth.login');
    }


    private function is_date($date){

        if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$date))
        {
            return true;
        }else{
            return false;
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!Auth::check()) return view('auth.login');
        try {

            $created_at = date("Y-m-d H:i:s");
            
            if ($request->created_at !=null && strlen($request->created_at)> 0) {
                
                $created_at = $request->created_at;
                $valid_date = $this->is_date($created_at);
                if ($valid_date === true){
                    $t=time(); 
                    $time = date('h:i:s',$t);
                    $created_at = $created_at . ' '. $time; 
                    //dd($created_at);
                } else {
                    $created_at = date("Y-m-d H:i:s");
                }
            }
            
            if ($request !=null 
                && $request->create !=null 
                && $request->create == 'Place Order'
                && $request->packages != null
                && $request->member_id !=null
                && $request->profile_id !=null) {

                //Member
                $memberId = $request->member_id;
                $query = "SELECT m.id AS id, m.parent_id, m.profile_id, CONCAT(p.first_name,' ',p.last_name) AS 'name', p.email FROM member m LEFT JOIN profile p ON m.profile_id = p.id WHERE m.id=$memberId";
                $member = DB::select($query);

                if ($member !=null && count($member)==1) {
                    $member = $member[0];
                } else {
                    throw new Exception("Invalid member.");
                }

                //Packages
                $packageIds = $request->packages;  
                $query = "SELECT id, title, amount, first_fifteen, second_fifteen, third_fifteen FROM packages WHERE id IN ($packageIds)";
                $packages = DB::select($query);
                
                if ($packages == null) {
                    throw new Exception("Invalid packages.");
                } 

                $subTotal =0;
                $discount =0;
                
                foreach ($packages as $key => $item) {
                    $subTotal += floatval($item->amount);
                }

                $grandTotal = $subTotal - $discount;
                
                DB::beginTransaction();
                try {
                    
                    $sales = new Sales;
                    $sales->member_id = $member->id;
                    $sales->profile_id = $member->profile_id;
                    $sales->name = $member->name;
                    $sales->email = $member->email;
                    $sales->status = 'Complete';
                    $sales->sub_total = $subTotal;
                    $sales->discount =$discount;
                    $sales->grand_total = $grandTotal;
                    $sales->created_at = date("Y-m-d H:i:s", strtotime($created_at));
                    $sales->save();

                    $salesOrderId = $sales->id;

                    foreach ($packages as $key => $item) {
                        $pdc = '[ '. $item->first_fifteen .', '. $item->second_fifteen .','. $item->third_fifteen .' ]';
                        $details = new Salesdetails;
                        $details->sales_order_id = $salesOrderId;
                        $details->package_id = $item->id;
                        $details->package_title = $item->title;
                        $details->package_pdc = $pdc;
                        $details->price = floatval($item->amount);
                        $details->created_at = date("Y-m-d H:i:s", strtotime($created_at));
                        $details->save();
                        $details = null;
                    }

                    DB::commit();
                    $response = $member->name .'\'s Cart transaction was successfully saved...';
                    $classstyle = 'success';
                } catch (Exception $e) {
                    throw new Exception($e->getMessage());
                    $classstyle = 'danger';
                    DB::rollback();
                }


            } else {
                $response = 'Something went wrong. Invalid Cart data...';
                $classstyle = 'danger';
            }

        } catch (Exception $e) {
            $response = $e->getMessage();
        }
        
        //dd($response);
        return redirect('catalog')->with('message',$response)->with('classstyle',$classstyle);

    }

    public function shortStore(Request $request){
        
        $member_id = 0; 
        try {
            $created_at = date("Y-m-d H:i:s");
            
            if ($request->created_at !=null && strlen($request->created_at)> 0) {
                
                $created_at = $request->created_at;
                $valid_date = $this->is_date($created_at);
                if ($valid_date === true){
                    $t=time(); 
                    $time = date('h:i:s',$t);
                    $created_at = $created_at . ' '. $time; 
                    //dd($created_at);
                } else {
                    $created_at = date("Y-m-d H:i:s");
                }
            }

            if ($request !=null 
                && $request->create !=null 
                && $request->create == 'Place Order'
                && $request->packages != null
                && $request->first_name !=null
                && $request->last_name !=null
                && $request->parent_id !=null
                && $this->is_date($request->member_since)) {

                //dd($request);
                

                DB::beginTransaction();
                try {

                    $slug = str_slug($request->first_name . '-' . $request->last_name);
                    $profile = new Profile;
                    $profile->profile_key = $slug;
                    $profile->first_name = $request->first_name;
                    $profile->last_name = $request->last_name;
                    $profile->registration_date = $request->member_since;
                    $profile->age = 1;
                    $profile->date_of_birth = $created_at; 
                    $profile->gender = 'Male';
                    $profile->status = 'Single';
                    $profile->address_primary = '';
                    $profile->email = 'email@dummy.com';
                    $profile->country = 'Philippines';
                    $profile->save();  
                    $profile_id = $profile->id;
                    
                    $member = new Member;
                    $member->parent_id = $request->parent_id;
                    $member->profile_id = $profile_id;
                    $member->group_code = 'DEQUODE';
                    $member->created_at = $request->member_since;
                    $member->save();  
                    $member_id = $member->id;

                    //Packages
                    $packageIds = $request->packages;  
                    $query = "SELECT id, title, amount, first_fifteen, second_fifteen, third_fifteen FROM packages WHERE id IN ($packageIds)";
                    $packages = DB::select($query);
                    
                    if ($packages == null) {
                        throw new Exception("Invalid packages.");
                    } 

                    $subTotal =0;
                    $discount =0;
                    
                    foreach ($packages as $key => $item) {
                        $subTotal += floatval($item->amount);
                    }

                    $grandTotal = $subTotal - $discount;
                    $name = $request->first_name . ' ' . $request->last_name;

                    $sales = new Sales;
                    $sales->member_id = $member_id;
                    $sales->profile_id = $profile_id;
                    $sales->name = $name;
                    $sales->email = 'email@dummy.com';
                    $sales->status = 'Complete';
                    $sales->sub_total = $subTotal;
                    $sales->discount =$discount;
                    $sales->grand_total = $grandTotal;
                    $sales->created_at = date("Y-m-d H:i:s", strtotime($created_at));
                    $sales->save();

                    $salesOrderId = $sales->id;

                    foreach ($packages as $key => $item) {
                        $pdc = '[ '. $item->first_fifteen .', '. $item->second_fifteen .','. $item->third_fifteen .' ]';
                        $details = new Salesdetails;
                        $details->sales_order_id = $salesOrderId;
                        $details->package_id = $item->id;
                        $details->package_title = $item->title;
                        $details->package_pdc = $pdc;
                        $details->price = floatval($item->amount);
                        $details->created_at = date("Y-m-d H:i:s", strtotime($created_at));
                        $details->save();
                        $details = null;
                    }


                    DB::commit();
                    $response = $name .'\'s Cart transaction was successfully saved...';
                    $classstyle = 'success';
                } catch (Exception $e) {
                    throw new Exception($e->getMessage());
                    $classstyle = 'danger';
                    DB::rollback();
                }

                //dd($classstyle);
            } else {
                $response = 'Something went wrong. Invalid data...';
                $classstyle = 'danger';
            }

        } catch (Exception $e) {
            $response = $e->getMessage();
        }

        return redirect('member/create/'. $request->parent_id . '?shortcut=1&newid='. $member_id)->with('message',$response)->with('classstyle',$classstyle);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
