<?php

namespace App\Http\Controllers\AppControllers;

use Illuminate\Http\Request;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;

use App\AppCore\Models\Sales;

class SalesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(!Auth::check()) return view('auth.login');
        
        $pageTitle = 'Sales';
        try {
            
            $q = NULL; $sb = NULL; $fr = null; $to=null;

            

            if ($request != null && $request->get('q') != null && $request->get('sb') !=null
                && strlen($request->get('q')) > 0 && strlen($request->get('sb')) > 0) {
                $q = $request->get('q'); $sb = $request->get('sb');

                $sales = DB::table('sales')
                    ->select(
                        'id',
                        'member_id',
                        'profile_id',
                        'name',
                        'email',
                        'created_at',
                        DB::raw('IFNULL(FORMAT(sub_total,2),0) as sub_total'),
                        DB::raw('IFNULL(FORMAT(discount,2),0) as discount'),
                        DB::raw('IFNULL(FORMAT(grand_total,2),0) as grand_total'),
                        'status'
                        )
                    ->where( 
                        ( $sb=='mid'? 'member_id' : ($sb =='oid'? 'id': ($sb =='email'? 'email': ($sb =='date'? 'created_at': ($sb =='stt'? 'status': 'name' ))))  ),
                        ( $sb=='mid'? '=' : ($sb =='oid'? '=': 'LIKE' ) ),
                        ( $sb=='mid'? $q : ($sb =='oid'? $q: '%'.$q.'%') )
                        )   
                    ->orderBy('created_at', 'desc')
                    ->paginate(8);
                $sales->setPath("sales?q=$q&sb=$sb");
            } else if ($request != null && $request->get('fr') != null && $request->get('to') != null && $request->get('sb') !=null
                && strlen($request->get('fr')) > 0 && strlen($request->get('to')) > 0 && strlen($request->get('sb')) > 0 && $request->get('sb') =='daterange' ) {

                $fr = $request->get('fr'); 
                $to= $request->get('to') ;

                //dd($fr, $to);

                $sales = DB::table('sales')
                    ->select(
                        'id',
                        'member_id',
                        'profile_id',
                        'name',
                        'email',
                        'created_at',
                        DB::raw('IFNULL(FORMAT(sub_total,2),0) as sub_total'),
                        DB::raw('IFNULL(FORMAT(discount,2),0) as discount'),
                        DB::raw('IFNULL(FORMAT(grand_total,2),0) as grand_total'),
                        'status'
                        )
                    ->whereBetween(
                        DB::raw('DATE(created_at)'), [$fr, $to]
                        )   
                    ->orderBy('created_at', 'desc')
                    ->paginate(8);
                $sales->setPath("sales?fr=$fr&to=$to&sb=$sb");

            } else {

                $sales = DB::table('sales')
                    ->select(
                        'id',
                        'member_id',
                        'profile_id',
                        'name',
                        'email',
                        'created_at',
                        DB::raw('IFNULL(FORMAT(sub_total,2),0) as sub_total'),
                        DB::raw('IFNULL(FORMAT(discount,2),0) as discount'),
                        DB::raw('IFNULL(FORMAT(grand_total,2),0) as grand_total'),
                        'status'
                        )
                    ->orderBy('created_at', 'desc')
                    ->paginate(8);
                $sales->setPath('sales');

            }
                
            //dd($sales);
            return view('content.sales.index', compact('pageTitle','sales'));
        } catch (Exception $e) {
            Error.log($e);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Sales $item)
    {
        if(!Auth::check()) return view('auth.login');
        $pageTitle = 'Order Details';
        try {
            
            if (!empty($item) && $item != null){

                $orders = DB::table('salesdetails')
                    ->leftJoin('packages', 'package_id', '=', 'packages.id')
                    ->select(
                        'salesdetails.*', 
                        DB::raw('IFNULL(packages.color,"navy") as color')
                        )
                    ->where('sales_order_id','=',$item->id)
                    ->orderBy('created_at', 'desc')
                    ->get();

                //dd($item,$orders);
                return view('content.sales.show', compact('item','orders','pageTitle'));
            }
            
        } catch (Exception $e) {
            Error.log($e);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function delete($id)
    {
     
        $isValueValid = is_numeric($id);
        if ($isValueValid) {

            $sale = DB::table('sales')->where('id','=',$id)->get();
            if ($sale != null && count($sale) ==1) {
                DB::beginTransaction();
                try {
                    DB::table('sales')->where('id','=',$id)->delete();
                    DB::table('salesdetails')->where('sales_order_id','=',$id)->delete();
                    DB::commit();
                    $response = 'Sale ID '. $id .' from '. $sale[0]->name .' was successfully deleted!';
                    $classstyle = 'success';
                } catch (Exception $e) {
                    $response = $e->getMessage();
                    $classstyle = 'danger';
                    DB::rollback();
                }
            } else {
                $response = 'No record found...';
                $classstyle = 'warning';
            }
        } else {
            $response = 'Invalid Sale ID '. $id;
            $classstyle = 'warning';
        }

        //dd($response);
        
        return redirect('sales')->with('message',$response)->with('classstyle',$classstyle);
    }
}
