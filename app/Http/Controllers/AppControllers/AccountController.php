<?php

namespace App\Http\Controllers\AppControllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use AppCore\Models\Usertype;
use DB;
use Auth;

class AccountController extends Controller
{
    private $_appUsers = null;
    
    public function __construct()
	{
		$appUsers = new User;
		$this->_appUsers = $appUsers->getUsers();
	}
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!Auth::check()) return view('auth.login');
        
        $pageTitle = 'User Accounts';
		$users = $this->_appUsers;
		return view('content.users.index', compact('pageTitle','users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(!Auth::check()) return view('auth.login');
        
        $pageTitle = 'User Account';
		$usertype = DB::table('usertype')->where('is_active',1)->lists('title','id'); 
		return view('content.users.create', compact('pageTitle','usertype'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(User $user, Requests\CreateUserAccountsRequest $request)
    {
        if(!Auth::check()) return view('auth.login');
        
        DB::beginTransaction();
		try
		{
			$object = new User;
			$object->name = $request->get('name');
			$object->email = $request->get('email');
			$object->password = \Hash::make($request->get('password'));
			$object->usertype_id = $request->get('usertype_id');
			$object->is_active = 1;
			$object->save();
			DB::commit();
			$response = 'Record was successfuly saved...';
			$classstyle = 'success';
		}catch (Exception $ex){
			$response = $e->getMessage();
			$classstyle = 'danger';
			DB::rollback();
		}

		return redirect('useraccounts')->with('message',$response)->with('classstyle',$classstyle);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        if(!Auth::check()) return view('auth.login');
        
        $pageTitle = 'User Account';
		try {
			if (!empty($user) && $user != null && $user->exists == true){
				$usertype = DB::table('usertype')->where('is_active',1)->lists('title','id'); 
				return view('content.users.edit', compact('user', 'usertype','pageTitle'));
			} else {
				$response = 'No record found...';
				$classstyle = 'danger';
				return redirect('useraccounts')->with('message',$response)->with('classstyle',$classstyle);
			}
			
		} catch (Exception $e) {
			Error.log($e);
		}
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(User $user, Requests\UpdateUserAccountsRequest $request)
    {
        if(!Auth::check()) return view('auth.login');
        
        # code to check if the request object is null
		if (!isset($request)) return 'Error';
		
		if (!empty($request->get('delete')))
		{
			# code to delete selected item
			return $this->destroy($user);
		}
		elseif (!empty($request->get('update'))) {
			# code to update selected item
			try
			{
				$user->name = $request->get('name');
				$user->email = $request->get('email');
				$user->password = \Hash::make($request->get('password'));
				$user->usertype_id = $request->get('usertype_id');
				$user->is_active = 1;
				$user->save();
				$response = 'Record was successfuly updated...';
				$classstyle = 'success';
			}catch (Exception $ex){
				$response = $e->getMessage();
				$classstyle = 'danger';
			}
			return redirect('useraccounts')->with('message',$response)->with('classstyle',$classstyle);
		}
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        try
		{
            if(!Auth::check()) return view('auth.login');
            
			$user->is_active = 0;
			$user->save();
			$response = 'Record was successfuly deleted...';
			$classstyle = 'success';
		}catch (Exception $ex){
			$response = $e->getMessage();
			$classstyle = 'danger';
		}
		return redirect('useraccounts')->with('message',$response)->with('classstyle',$classstyle);
    }
}
