<?php

namespace App\Http\Controllers\AppControllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Helpers\Util;
use App\AppCore\Models\Userrole;
use DB;
use Auth;

class UserRolesController extends Controller
{
    private $_htmlAppTree;
    
    public function __construct()
	{
		$this->_htmlAppTree = $this->getHtmlAppTree();
	}
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Userrole $object)
    {
        try {
            if(!Auth::check()) return view('auth.login');
            
			$pageTitle = 'User Roles';
			$userrole = $object->where('is_active',1)->get(); 
			return view('content.userrole.index', compact('userrole','pageTitle'));

		} catch (Exception $e) {
			//return $e->getMessage();
		}
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(!Auth::check()) return view('auth.login');
        $pageTitle = 'Role';
		$appsManager = $this->_htmlAppTree;
		return view('content.userrole.create',compact('pageTitle','appsManager'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\CreateUserRoleRequest $request)
    {
        if(!Auth::check()) return view('auth.login');
        DB::beginTransaction();
		try
		{
			$object = new Userrole;
			$object->title = $request->get('title');
			$object->route_ids = $request->get('route_ids');
			$object->is_active = 1;
			$object->save();
			DB::commit();
			$response = 'Record was successfuly saved...';
			$classstyle = 'success';
		}catch (Exception $ex){
			$response = $e->getMessage();
			$classstyle = 'danger';
			DB::rollback();
		}

		return redirect()->route('userrole.index')->with('message',$response)->with('classstyle',$classstyle);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Userrole $userrole)
    {
        if(!Auth::check()) return view('auth.login');
        $pageTitle = 'Role';
		try {
			if (!empty($userrole) && $userrole != null){
				$appsManager = $this->_htmlAppTree;
				return view('content.userrole.show', compact('userrole','pageTitle','appsManager'));
			}
			
		} catch (Exception $e) {
			Error.log($e);
		}
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Userrole $userrole)
    {
        if(!Auth::check()) return view('auth.login');
        $pageTitle = 'Role';
		$appsManager = $this->_htmlAppTree;
		return view('content.userrole.edit', compact('userrole','pageTitle','appsManager'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Userrole $userrole, Requests\UpdateUserRoleRequest $request)
    {
        if(!Auth::check()) return view('auth.login');
        # code to check if the request object is null
		if (!isset($request)) return 'Error';
		
		if (!empty($request->get('delete')))
		{
			# code to delete selected item
			return $this->destroy($userrole);
		}
		elseif (!empty($request->get('update'))) {
			# code to update selected item
			try
			{
				$userrole->title = $request->get('title');
				$userrole->route_ids = $request->get('route_ids');
				$userrole->save();
				$response = 'Record was successfuly updated...';
				$classstyle = 'success';
			}catch (Exception $ex){
				$response = $e->getMessage();
				$classstyle = 'danger';
			}
			return redirect('userrole')->with('message',$response)->with('classstyle',$classstyle);
		}
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Userrole $userrole)
    {
        try
		{
            if(!Auth::check()) return view('auth.login');
            
			$userrole->is_active = 0;
			$userrole->save();
			$response = 'Record was successfuly deleted...';
			$classstyle = 'success';
		} catch (Exception $ex){
			$response = $e->getMessage();
			$classstyle = 'danger';
		}
		return redirect('userrole')->with('message',$response)->with('classstyle',$classstyle);
    }
    
    private function getHtmlAppTree()
	{
        if(!Auth::check()) return view('auth.login');
		$apptreehtml = '';
		$xmlPath = storage_path()."/xml/Modules.xml";

		try {

			$collection = Util::simpleXmlToObject($xmlPath, '', '');

			foreach ($collection as $item => $value) {
				if ($value == null || empty($value)) $apptreehtml = '';

				if ($value['lastchild'] != null && intval($value['lastchild']) ==1) {
					$apptreehtml .= '<div><label><input type="checkbox" value="" id="'. $value['id'] .'" class="roles-resources"> '. $value['title'] .'</label></div>';
				} else {
					$apptreehtml .= '<div><label><input type="checkbox" value="" id="'. $value['id'] .'" class="roles-resources" has-child="1"> '. $value['title'] .'</label></div>';
					foreach ($value as $item => $child) {
						$apptreehtml .= '<div class="tree-l2"><label><input type="checkbox" value="" id="'. $child['id'] .'" parent-id="'. $value['id'] .'" class="roles-resources"> '. $child['title'] .'</label></div>';
					}
				}
			}
			
		} catch (Exception $e) {
			
		}

		return $apptreehtml;
	}
}
