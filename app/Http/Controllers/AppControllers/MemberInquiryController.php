<?php

namespace App\Http\Controllers\AppControllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use Carbon\Carbon;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\AppCore\Models\Member;

class MemberInquiryController extends Controller
{
    private $root_parent_id = 0;
    private $level = 0;
    private $parents = null;
    private $memberCollection = array();
    
    private function setParents($value)
    {
        $this->parents = $value;
    }
    
    private function getParents()
    {
        return $this->parents;
    }
    
    private function setLevel($value){
        $this->level = $value;
    }

    public function getLevel(){
        return $this->level;
    }

    private function setMemberCollection($value){
        $this->memberCollection = $value;
    }

    public function getMemberCollection(){
        return $this->memberCollection;
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    { 
        if(!Auth::check()) return view('auth.login');
        
        $pageTitle = "Member's Inquiry";
        $memberId = -1;
        $profile = null;
        $lastPurchaseDate = date("Y-m-d");
        $isMemberActive = false;
        $background = 'red';
        $totalPurchase = 0;
        $purchaseDetails = null;
        $releasedIncentives = null;
        $groups = null;
        $member = null;
        $parent = null;
        $json = '[]'; // do not alter
        
        try {
            if ($request && $request->get('id') != null && is_numeric($request->get('id'))) {
                $memberId = $request->get('id');
                $this->root_parent_id = $memberId;
                
                $lastPurchaseDate = $this->getLastPurchaseDate($memberId);
//                dd($lastPurchaseDate);
                $purchases = $this->getPurchases($memberId);
                if($purchases) {
                    foreach($purchases as $key => $val) {
                        $totalPurchase += $val->grand_total;
                    }
                }
                $firstDayofThisMonth = Carbon::now()->startOfMonth();
                $lastDayofThisMonth = Carbon::now()->endOfMonth();
                $lastPurchaseDate = array_filter($lastPurchaseDate);
                if(!empty($lastPurchaseDate)) {
                    $isMemberActive = strtotime($lastPurchaseDate[0]->created_at) >= strtotime($firstDayofThisMonth) && strtotime($lastPurchaseDate[0]->created_at) <= strtotime($lastDayofThisMonth);
                } else {
                    $isMemberActive = false;
                }
                
                if($isMemberActive) $background = 'green';
                
                $profile = DB::table('profile')
                    ->join('member', 'profile.id', '=', 'member.profile_id')
                    ->where('member.id', '=', $memberId)->first();
                
                if(!empty($lastPurchaseDate)) {
                    $lastPurchaseDate = date_format(date_create($lastPurchaseDate[0]->created_at), 'm-d-Y');
                } else {
                    $lastPurchaseDate = '';
                }
                
                // TODO: Need to be paginated
                $purchaseDetails = $this->getPurchaseDetails($memberId);
                
                // TODO: Need to be paginated
                $releasedIncentives = $this->getReleasedIncentives($memberId);
                
                // Get Downlines
                $incentives = $this->getUnilevels(); // doesn't include deleted_at != null
                $this->setMemberCollection($this->getMembers($memberId)); // first levels
                
                $member = $this->getSelectedMember($memberId); // current selected member
                $parent = $this->getSelectedMember($member[0]->parent_id);
                $parent = array_filter($parent);
                if(!empty($parent)) {
                    $parent = $parent[0]->name;
                } else $parent = '';
                
                $lastIncentiveDate = $this->getLastIncentiveDate($memberId);
                if(empty(array_filter($lastIncentiveDate))) $lastIncentiveDate = null;

                $tree = $this->buildUnilevelTree($this->getMembers($memberId), $memberId);
//                dd($tree);
                $groups = array_filter($this->groupPerlevel($tree));
//                dd($groups);
                if(!empty($groups))
                {
                    $json = '[';
                    foreach($incentives as $incentive) {
                        $memberDetail = '';
                        $json .= '{"level":"'.$incentive->level;

                        foreach($groups as $key => $val) {
                            foreach($val as $group) {
                                if($incentive->level == $group->unilevel) {
                                    $memberActive = false;
                                    $purchaseDate = $this->getLastPurchaseDate($group->member_id);
                                    if($purchaseDate == null) {
                                        $memberActive = false;
                                    } else {
                                        $memberActive = strtotime($purchaseDate[0]->created_at) >= strtotime($firstDayofThisMonth) && strtotime($purchaseDate[0]->created_at) <= strtotime($lastDayofThisMonth);
                                    }
                                    $memberDetail .= '{"name":"'. addslashes($group->name).'", "status":"'.  ($memberActive ? 'true' : 'false') . '"},';
                                }
                            }
                        }

                        $memberDetail = substr_replace($memberDetail, '', -1);
                        $json .= '", "detail":['.$memberDetail.']';
                        $json .= '},';
                    }
                    
                    $json = substr_replace($json, '', -1);
                    $json .= ']';
                }
//                dd($json);
            }
//            dd($parent);
            return view('content.member.inquiry', compact('pageTitle', 'memberId', 'profile', 'lastPurchaseDate', 'isMemberActive', 'totalPurchase', 'background', 'purchaseDetails', 'releasedIncentives', 'parent', 'json'));
        } catch(Exception $e) {
            Error.log($e);
        }
    }
    
    public function profileNameSearch(Request $request)
    {
        $data = null;
        
        if ($request) {
            $q = $request->get('q');
            //dd($request->get('member') );
            try {
                $query = null;
                $query = "SELECT m.id AS id, m.parent_id, m.profile_id, CONCAT(p.first_name,' ',p.last_name) AS 'name' FROM member m LEFT JOIN profile p ON m.profile_id = p.id WHERE m.deleted_at IS NULL AND CONCAT(p.first_name, ' ', p.last_name) LIKE '%$q%' LIMIT 10";
                $searchBy = 'INQUIRY';

                if ($query != null)
                    $data = DB::select($query);
            
            } catch (Exception $e) {
                Error.log($e);
            }
        }
        return view('ajax.a_profile_name_dropdown', compact('data','searchBy'));
    }
    
    private function getLastPurchaseDate($member_id)
    {
        $collection = null;
        
        try {
            if($member_id != null && is_numeric($member_id)) {
                $query = "SELECT ";
                $query .= "sales.created_at ";
                $query .= "FROM sales ";
                $query .= "WHERE sales.member_id = $member_id ";
                $query .= " ORDER BY sales.created_at DESC";
                $collection = DB::select(DB::raw($query));
            }
        } catch(Exception $e) {
            Error.log($e);
        }
        
        return $collection;
    }
    
    private function getPurchases($member_id)
    {
        $collection = null;
        
        try {
            if($member_id != null && is_numeric($member_id)) {
                $query = "SELECT ";
                $query .= "sales.id, sales.grand_total, sales.created_at ";
                $query .= "FROM sales ";
                $query .= "WHERE sales.member_id = $member_id ";
                $query .= " ORDER BY sales.created_at";
                $collection = DB::select(DB::raw($query));
            }
        } catch(Exception $e) {
            Error.log($e);
        }
        
        return $collection;
    }
    
    private function getPurchaseDetails($member_id)
    {
        $collection = null;
        
        try {
            if($member_id != null && is_numeric($member_id)) {
                $query = "SELECT ";
                $query .= "salesdetails.package_title, salesdetails.price, sales.created_at ";
                $query .= "FROM salesdetails ";
                $query .= "INNER JOIN sales ON (sales.id = salesdetails.sales_order_id) ";
                $query .= "WHERE sales.member_id = $member_id ";
                $query .= " ORDER BY sales.created_at";
                
                $collection = DB::select(DB::raw($query));
            }
        } catch(Exception $e) {
            Error.log($e);
        }
        
        return $collection;
    }
    
    private function getReleasedIncentives($member_id)
    {
        $collection = null;
        
        try {
            if($member_id != null && is_numeric($member_id)) {
                $query = "SELECT ";
                $query .= "member_id, grand_total, created_at ";
                $query .= "FROM released_incentives ";
                $query .= "WHERE released_incentives.member_id = $member_id ";
                $query .= " AND deleted_at IS NULL ";
                $query .= " ORDER BY released_incentives.created_at";
                $collection = DB::select(DB::raw($query));
            }
        } catch(Exception $e) {
            Error.log($e);
        }
        
        return $collection;
    }
    
    private function getLastIncentiveDate($member_id)
    {
        $collection = null;
        
        try {
            if($member_id != null && is_numeric($member_id)) {
                $query = "SELECT ";
                $query .= "released_incentives.created_at ";
                $query .= "FROM released_incentives ";
                $query .= "WHERE member_id = $member_id ";
                $query .= " AND deleted_at IS NULL ";
                $query .= "ORDER BY ";
                $query .= "created_at DESC";
                $collection = DB::select($query);
            }
        } catch(Exception $e) {
            Error.log($e);
        }
        
        return $collection;
    }
    
    private function getMembers($parent_id) 
    {
        $collection = null;

        try {
            if ($parent_id != null && is_numeric($parent_id)) {
                $collection = DB::select("SELECT m.id AS 'member_id', m.parent_id, m.profile_id, CONCAT(p.first_name, ' ',p.last_name) AS 'name', p.gender FROM member m LEFT JOIN profile p ON m.profile_id = p.id WHERE parent_id = $parent_id AND m.deleted_at is null");
            } else {
                $collection = DB::select("SELECT m.id AS 'member_id', m.parent_id, m.profile_id, CONCAT(p.first_name, ' ',p.last_name) AS 'name', p.gender FROM member m LEFT JOIN profile p ON m.profile_id = p.id WHERE m.deleted_at is null");
            }
        } catch (Exception $e) {
            Error.log($e);
        }
        return $collection;
    }
    
    private function buildUnilevelTree(array $elements, $parentId = 0) 
    {
        $branch = array();
        $purchases = null;

        foreach ($elements as $key => $node) {
            $this->setLevel(0);
            
            if ($node->parent_id == $parentId) {
                // Get unilevel
                $this->memberTree($node->member_id, $this->root_parent_id);
                $node->unilevel = $this->getLevel();
                //-- end get unilevel
                
                $children = $this->buildUnilevelTree($elements, $node->member_id);
//                $children = $this->buildUnilevelTree($this->getMembers($node->member_id), $node->member_id);
//                dd($this->getMembers($node->member_id));
                if ($children) {
                    // Get unilevel
                    $this->memberTree($children[0]->member_id, $this->root_parent_id);
                    $children[0]->unilevel = $this->getLevel();
                    //-- end get unilevel
                    $branch = array_merge($children, $branch);
                }
                
                $branch[] = $node;
            }
        }
        return $branch;
    }
    
    private function groupPerlevel(array $items)
    {
        $templevel = 1;
        $newkey = 0;
        $grouparr[$templevel] = null;
        foreach($items as $key => $val) {
            if($templevel == $val->unilevel) {
                $grouparr[$templevel][$newkey] = $val;
            } else {
                $grouparr[$val->unilevel][$newkey] = $val;
            }
            $newkey++;
        }
        return $grouparr;
    }
    
    private function memberTree($active_id, $root_id)
    {
        $_level =$this->getLevel();

        $node = $this->arraySearch($this->getMembers(null),'member_id', $active_id);
        

        if ($node==null) {
            $this->setLevel($_level);
            return;
        } else {
            if ($node[0]->member_id == $root_id) return;
            $_level = $_level + 1;
            $this->setLevel($_level);
            $this->memberTree($node[0]->parent_id, $root_id);
        }
        
    }
    
    private function arraySearch($array, $index, $value) 
    {
        $collection = array();
        foreach($array as $arrayInf) {
            if($arrayInf->{$index} == $value) {
                array_push($collection, $arrayInf);
            }
        }
        return $collection;
    }
    
    private function getSelectedMember($memberId)
    {
        $collection = $this->getMembers(null);
        if($collection == null) {
            return null;
        } else {
            $node = $this->arraySearch($collection,'member_id', $memberId);
            return $node;
        }
        
        return;
    }
    
    private function getUnilevels() 
    {
        $collection = null;
        try {
            $collection = DB::select("SELECT level, incentive FROM soph.incentives where deleted_at is null");
        } catch (Exception $e) {
            Error.log($e);
        }
        return $collection;
    }
    
    // TODO: Need to be paginated
    public function getAjaxPurchases(Request $request)
    {
        $purchaseDetails = null;
        $lastPurchaseDate = null;
        $memberId = $request->get('id');
        $html = '';
        $member_name = '';
        $parent = '';
        $isMemberActive = false;
        $totalPurchase = 0;
        
        try {
            $member = $this->getSelectedMember($memberId); // current selected member
            $member = array_filter($member);
            if(!empty($member)) {
                $member_name = $member[0]->name;
                $parent = $this->getSelectedMember($member[0]->parent_id);
                
                $parent = array_filter($parent);
                if(!empty($parent)) {
                    $parent = $parent[0]->name;
                } else $parent = '';
            }
            
            $lastPurchaseDate = $this->getLastPurchaseDate($memberId);
            $purchases = $this->getPurchases($memberId);
            if($purchases) {
                foreach($purchases as $key => $val) {
                    $totalPurchase += $val->grand_total;
                }
            }
            $firstDayofThisMonth = Carbon::now()->startOfMonth();
            $lastDayofThisMonth = Carbon::now()->endOfMonth();
            $lastPurchaseDate = array_filter($lastPurchaseDate);
            if(!empty($lastPurchaseDate)) {
                $isMemberActive = strtotime($lastPurchaseDate[0]->created_at) >= strtotime($firstDayofThisMonth) && strtotime($lastPurchaseDate[0]->created_at) <= strtotime($lastDayofThisMonth);
//                dd(Carbon::now()->endOfMonth());
                $lastPurchaseDate = date_format(date_create($lastPurchaseDate[0]->created_at), 'm-d-Y');
            } else {
                $isMemberActive = false;
                $lastPurchaseDate = null;
            }
            $purchaseDetails = $this->getPurchaseDetails($memberId);
            foreach($purchaseDetails as $key => $val) {
                $html .= '<tr><td>'.$val->created_at.'</td>';
                $html .= '<td>Package '.$val->package_title.'</td>';
                $html .= '<td>'.number_format($val->price, 2).' Php</td>';
                $html .= '</tr>';
            }
            

            return response()->json([ 'details' => $html, 'parent' => $parent, 'member' => $member_name, 'status' => $isMemberActive, 'totalPurchase' => number_format($totalPurchase, 2), 'lastPurchaseDate' => $lastPurchaseDate, 'memberId' => $memberId ]);
        } catch (Exception $e) {
            Error.log($e);
        }
    }
    
    private function getMemberRegistrationDateTime($member_id)
    {
        $collection = null;
        
        try {
            if($member_id != null && is_numeric($member_id)) {
                $query = "SELECT ";
                $query .= "profile.registration_date as 'created_at' ";
                $query .= "FROM profile ";
                $query .= "INNER JOIN member ON (member.profile_id = profile.id) ";
                $query .= "WHERE member.id = $member_id ";
                $query .= " ORDER BY member.created_at DESC";
                $collection = DB::select(DB::raw($query));
            }
        } catch(Exception $e) {
            Error.log($e);
        }
        
        return $collection;
    }
    
    // TODO: Need to be paginated
    public function getAjaxIncentives(Request $request)
    {
        $releasedIncentives = null;
        $memberId = $request->get('id');
        $html = '';
        
        try {
            $releasedIncentives = $this->getReleasedIncentives($memberId);
            foreach($releasedIncentives as $key => $val) {
                $html .= '<tr><td>'.$val->created_at.'</td>';
                $html .= '<td>'.number_format($val->grand_total, 2).' Php</td>';
                $html .= '</tr>';
            }

            return response()->json([ 'details' => $html ]);
        } catch (Exception $e) {
            Error.log($e);
        }
    }
    
    public function getAjaxDownlines(Request $request)
    {
        $json = '[]';
        $memberId = $request->get('id');
        $this->root_parent_id = $memberId;
        $member_name = '';
        
        $firstDayofThisMonth = Carbon::now()->startOfMonth();
        $lastDayofThisMonth = Carbon::now()->endOfMonth();
        try {
            $lastPurchaseDate = $this->getLastPurchaseDate($memberId);
            
            // Get Downlines
            $incentives = $this->getUnilevels(); // doesn't include deleted_at != null
            $this->setMemberCollection($this->getMembers($memberId)); // first levels
          
            $lastIncentiveDate = $this->getLastIncentiveDate($memberId);
            if(empty(array_filter($lastIncentiveDate))) $lastIncentiveDate = null;

            $tree = $this->buildUnilevelTree($this->getMembers(null), $memberId);
//            dd($tree);
            $groups = array_filter($this->groupPerlevel($tree));
//            dd($groups);
            if(!empty($groups))
            {
                $json = '[';
                foreach($incentives as $incentive) {
                    $memberDetail = '';
                    $json .= '{"level":"'.$incentive->level;

                    foreach($groups as $key => $val) {
                        foreach($val as $group) {
                            if($incentive->level == $group->unilevel) {
                                $memberActive = false;
                                $registrationDate = $this->getMemberRegistrationDateTime($group->member_id);
                                $purchaseDate = $this->getLastPurchaseDate($group->member_id);
                                if($purchaseDate == null) {
                                    $memberActive = false;
                                } else {
                                    $memberActive = strtotime($purchaseDate[0]->created_at) >= strtotime($firstDayofThisMonth) && strtotime($purchaseDate[0]->created_at) <= strtotime($lastDayofThisMonth);
                                }
                                $memberDetail .= '{"name":"'. addslashes($group->name).'", "status":"'.$memberActive.'", "registration":"'.$registrationDate[0]->created_at.'"},';
                            }
                        }
                    }

                    $memberDetail = substr_replace($memberDetail, '', -1);
                    $json .= '", "detail":['.$memberDetail.']';
                    $json .= '},';
                }

                $json = substr_replace($json, '', -1);
                $json .= ']';
            }
            
            return response()->json([ 'details' => $json ]);
        } catch(Exception $e) {
            Error.log($e);
        }
    }
    
    public function getMemberDownlines(Request $request)
    {
        if(!Auth::check()) return view('auth.login');
        
        $memberid = null;
        $parentid = null;
        $sponsor = null;
        $html = "";
        $level = 1;
        $maxLevel = 1;
        $isMemberActive = false;
        
        try {
            if($request->ajax()) {
                $memberid = $_POST['pMemberId'];
                $level = $_POST['pLevel'];
                $parents = $_POST['pParents'];
                
                $this->root_parent_id = $memberid;
                $this->setMemberCollection($this->getMembers(null));
                $member = $this->getMemberProfile($memberid);
                $sponsor = $this->getParent($memberid);
                
                if( $this->getMemberCollection() && count($this->getMemberCollection()) > 0 ) {
                    $tree = $this->buildSequentialTree($this->getMemberCollection(), $parents);
                    if($tree != null) {
                        $incentives = $this->getUnilevels(); // doesn't include deleted_at != null
                        $maxLevel = count($incentives);
                        
                        foreach($incentives as $incentive) {
                            if($incentive->level == $level) {
                                foreach($tree as $node) {
                                    $regDate = $this->getMemberRegistrationDateTime($node->member_id);
                                    if(empty(array_filter($regDate))) $regDate[0]->created_at = '';
                                    $html .= '<tr>';
                                    $html .= '<td>'.$node->name.'</td>';
                                    $html .= '<td>'.$regDate[0]->created_at.'</td>';
                                    
                                    //-- is member active/inactive
                                    $purchaseDate = $this->getLastPurchaseDate($node->member_id);
                                    if(empty(array_filter($purchaseDate))) $purchaseDate = null;
                                    $firstDayofThisMonth = Carbon::now()->startOfMonth();
                                    $lastDayofThisMonth = Carbon::now()->endOfMonth();

                                    if($purchaseDate != null) {
                                        $isMemberActive = strtotime($purchaseDate[0]->created_at) >= strtotime($firstDayofThisMonth) && strtotime($purchaseDate[0]->created_at) <= strtotime($lastDayofThisMonth);
                                    }
                                    //-- end of checking whether member is active/inactive
                                    
                                    if($isMemberActive) {
                                        $html .= '<td style="vertical-align:inherit;"><span class="label label-success">ACTIVE</span></td>';
                                    } else {
                                        $html .= '<td style="vertical-align:inherit;"><span class="label label-danger">INACTIVE</span></td>';
                                    }

                                    $html .= '</tr>';
                                }
                            }
                        }
                    }
                }
                
                return response()->json([
                    'html' => $html, 'member' => $member, 
                    'sponsor' => $sponsor, 'maxLevel' => $maxLevel, 
                    'level' => $level, 'parents' => $this->getParents()]);
            }
        } catch(Exception $e) {
            Error.log($e);
        }
    }
    
    public function getRemainingDownlines(Request $request)
    {
        $html = "";
        
        try {
            if($request->ajax()) {
                $level = $_POST['pLevel'];
                $parents = $_POST['pParents'];
                
                $this->setMemberCollection($this->getMembers(null));

                if( $this->getMemberCollection() && count($this->getMemberCollection()) > 0 ) {
                    $tree = $this->buildSequentialTree($this->getMemberCollection(), $parents);
                    if($tree != null) {
                        $incentives = $this->getUnilevels($level); // doesn't include deleted_at != null
                        foreach($incentives as $incentive) {
                            if($incentive->level == $level) {
                                foreach($tree as $node) {
                                    $regDate = $this->getMemberRegistrationDateTime($node->member_id);
                                    if(empty(array_filter($regDate))) $regDate[0]->created_at = '';
                                    $html .= '<tr>';
                                    $html .= '<td>'.$node->name.'</td>';
                                    $html .= '<td>'.$regDate[0]->created_at.'</td>';
                                    
                                    //-- is member active/inactive
                                    $purchaseDate = $this->getLastPurchaseDate($node->member_id);
                                    if(empty(array_filter($purchaseDate))) $purchaseDate = null;
                                    $firstDayofThisMonth = Carbon::now()->startOfMonth();
                                    $lastDayofThisMonth = Carbon::now()->endOfMonth();

                                    if($purchaseDate != null) {
                                        $isMemberActive = strtotime($purchaseDate[0]->created_at) >= strtotime($firstDayofThisMonth) && strtotime($purchaseDate[0]->created_at) <= strtotime($lastDayofThisMonth);
                                    }
                                    //-- end of checking whether member is active/inactive
                                    
                                    if($isMemberActive) {
                                        $html .= '<td style="vertical-align:inherit;"><span class="label label-success">ACTIVE</span></td>';
                                    } else {
                                        $html .= '<td style="vertical-align:inherit;"><span class="label label-danger">INACTIVE</span></td>';
                                    }

                                    $html .= '</tr>';
                                }
                            }
                        }
                    }
                }
                
                return response()->json([
                    'html' => $html, 'level' => $level, 'parents' => $this->getParents()]);
            }
        } catch(Exception $e) {
            Error.log($e);
        }
    }
    
    private function buildSequentialTree(array $elements, $parentIds)
    {
        $parentIdString = "";
        $purchases = null;
        $children = null;
        try {
            if(isset($parentIds)) {
                $children = $this->getChildren($parentIds);
                if(isset($children)) {
                    foreach($children as $child) {
                        $parentIdString .= $child->member_id.",";
                    }
                    $parentIdString = substr_replace($parentIdString, '', -1);
                    $this->setParents($parentIdString);
                }
            }
        } catch(Exception $e) {
            Error.log($e);
        }
        return $children;
    }
    
    private function getMemberProfile($id)
    {
        $profile = null;
        try {
            $profile = DB::table('profile')
                ->join('member', 'member.profile_id', '=', 'profile.id')
                ->where('member.id', '=', $id)
                ->first();
        } catch(Exception $e) {
            Error.log($e);
        }
        return $profile;
    }
    
    private function getParent($memberid)
    {
        $profile = null;
        try {
            $member = DB::table('member')
                ->where('member.id', '=', $memberid)
                ->first();
            if($member) {
                $profile = DB::table('profile')
                    ->join('member', 'member.profile_id', '=', 'profile.id')
                    ->where('member.id', '=', $member->parent_id)
                    ->first();
            }
        } catch(Exception $e) {
            Error.log($e);
        }
        return $profile;
    }
    
    private function getChildren($parent_ids) 
    {
        $collection = null;

        try {
            if ($parent_ids != null) {
                $query = "SELECT ";
                $query .= "m.id AS 'member_id', m.parent_id, m.profile_id, ";
                $query .= "CONCAT(p.first_name, ' ',p.last_name) AS 'name',p.gender ";
                $query .= "FROM ";
                $query .= "member m LEFT JOIN profile p ON m.profile_id = p.id ";
                $query .= "WHERE parent_id IN ($parent_ids) AND m.deleted_at is null ";
                $collection = DB::select($query);
            }
        } catch (Exception $e) {
            Error.log($e);
        }
        return $collection;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
