<?php

namespace App\Http\Controllers\AppControllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;

class AjaxDataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function getSaleablePackage(Request $request)
    {
        $html = '';
        if ($request !=null){

            try {
                /* Get saleable package*/
                $data = DB::table('salesdetails')
                    ->select(
                        'package_id',
                        'package_title',
                        DB::raw('IFNULL(COUNT(package_id),0) as top'),
                        DB::raw('SUM(IFNULL(price,0)) as total')
                        )
                    ->groupBy('package_id')
                    ->orderBy('top', 'desc')
                    ->take(10)
                    ->get();
                
                if ($data != null && count($data) >0) {
                    $html .= '<ul class="nav nav-stacked">';
                    $color = array(0 => 'bg-maroon', 1 => 'bg-blue', 2 => 'bg-aqua', 3=>'bg-yellow' );
                    $inx = 0;
                    foreach ($data as $key => $item) {

                        $html .= '<li>';    
                        $html .= '<a href="#">';  
                        $html .= 'Package '.$item->package_title.' '; 
                        $html .= '<span class="pull-right badge '. ($inx <= 3? $color[$inx]: $color[3] ).'">'; 
                        $html .= $item->top; 
                        $html .= '</span>'; 
                        $html .= '</a>'; 
                        $html .= '</li>';
                        $inx +=1;
                    }
                    $html .= '</ul>';
                } else {
                    $html .= 'No record found...';
                }                  
            } catch (Exception $e) { 
                $html .= 'Something went wrong. Please contact your System Administrator.';
            }

        }
        return $html;
    }

    public function getActiveMembers(Request $request)
    {
        $html = '';
        if ($request !=null){

            try {
                
                /* Get saleable package*/
                $data = DB::table('member')
                    ->leftJoin('profile', 'profile_id', '=', 'profile.id')
                    ->select(
                        'member.id',
                        'member.profile_id',
                        DB::raw('CONCAT(profile.first_name," ",profile.last_name) as name'),
                        'profile.gender',
                        DB::raw('DATE_FORMAT(member.created_at,"%b %d") as created_at')
                        )
                    ->whereNull('member.deleted_at')
                    ->orderBy('member.created_at', 'desc') 
                    ->take(8)
                    ->get();
                

                if ($data != null && count($data) >0) {
                    $html .= '<ul class="users-list clearfix">';
                    foreach ($data as $key => $item) {
                        $html .= '<li>';    
                        $html .= '<a href="'. url() .'/catalog/member?id='.$item->id .'"><img src="'.url().'/resources/assets/static/images/flat-'.$item->gender.'.png" class="user-image" alt="'. $item->name .'" /></a>';  
                        $html .= '<a class="users-list-name" href="'. url() .'/catalog/member?id='.$item->id .'" id="'.$item->id.'" profile-id="'.$item->profile_id.'">'. $item->name .'</a>'; 
                        $html .= '<span class="users-list-date">'. $item->created_at .'</span>'; 
                        $html .= '</li>';
                    }
                    $html .= '</ul>';
                } else {
                    $html .= 'No record found...';
                }

            } catch (Exception $e) { 
                $html .= 'Something went wrong. Please contact your System Administrator.';
            }
        }
        return $html;
    }

    public function getPackages(Request $request){
        $html = '';
        if ($request !=null){

            try {
                
                /* Get saleable package*/
                $data = DB::table('packages')
                    ->select(
                        'id',
                        'title',
                        'color',
                        DB::raw('IFNULL(FORMAT(amount,2),0) as amount'),
                        DB::raw('IFNULL(FORMAT(first_fifteen,2),0) as first_fifteen'),
                        DB::raw('IFNULL(FORMAT(second_fifteen,2),0) as second_fifteen'),
                        DB::raw('IFNULL(FORMAT(third_fifteen,2),0) as third_fifteen')
                        )
                    ->whereNull('deleted_at')
                    ->orderBy('id', 'asc') 
                    ->take(4)
                    ->get();

                if ($data != null && count($data) >0) {
                    $html .= '<ul class="products-list product-list-in-box">';
                    foreach ($data as $key => $item) {
                        $html .= '<li class="item">';
                        $html .= '<div class="product-img">'; 
                        $html .= '<img src="'.url().'/resources/assets/static/images/flaticon8.png" class="user-image" alt="Package '. $item->id.'">'; 
                        $html .= '</div>'; 
                        $html .= '<div class="product-info">'; 
                        $html .= '<a href="'.url().'/packages/'. $item->id.'" class="product-title">Package '. $item->id.'</a>'; 
                        $html .= '<span class="badge bg-'.$item->color.' pull-right">'.$item->amount.' Php</span>'; 
                        $html .= '<span class="product-description">'; 
                        $html .= 'PDC Cheques - '.$item->first_fifteen.' (1st), '.$item->second_fifteen.' (2nd), '.$item->third_fifteen.' 3rd'; 
                        $html .= '</span>'; 
                        $html .= ' </div>'; 
                        $html .= '</li>';
                    }
                    $html .= '</ul>';
                } else {
                    $html .= 'No record found...';
                }

            } catch (Exception $e) { 
                $html .= 'Something went wrong. Please contact your System Administrator.';
            }

        }
        return $html;
    }
}
