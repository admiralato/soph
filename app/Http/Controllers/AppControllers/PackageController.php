<?php

namespace App\Http\Controllers\AppControllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Auth;

use App\AppCore\Models\Package;

class PackageController extends Controller
{
    private $package;
    
    public function __construct(Package $item)
    {
        $this->package = $item;
    }
    
    public function index() {
        if(!Auth::check()) return view('auth.login');
        
        $pageTitle = 'Package Manager';
        try {
            $package = Package::all()->sortBy('title');
            return view('content.packages.index', compact('pageTitle', 'package'));
        } catch (Exception $e) {
            Error.log($e);
        }
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(!Auth::check()) return view('auth.login');
        
        $pageTitle = 'Create Package';
        $item = new Package;
        return view('content.packages.create', compact('pageTitle', 'item'));
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\CreatePackageRequest $request)
    {
        if(!Auth::check()) return view('auth.login');
        //TODO Validate requests
        //TODO Make sure package number is NOT less than 0
        DB::beginTransaction();
        try
        {
            $package = new Package;
            $package->title = $request->packageno;
            $package->amount = $request->amount;
            $package->first_fifteen = $request->first_fifteen;
            $package->second_fifteen = $request->second_fifteen;
            $package->third_fifteen = $request->third_fifteen;
            $package->color = $request->color;
            $package->save();  
            
            DB::commit();
            $response = 'Record was successfully saved!';
        } catch(Exception $e) {
            $response = $e->getMessage();
            DB::rollback();
        }
        return redirect('packages');
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Package $item)
    {
        if(!Auth::check()) return view('auth.login');
        $pageTitle = 'View Package';
        try {
            
            if (!empty($item) && $item != null){
                return view('content.packages.show', compact('item','pageTitle'));
            }
            
        } catch (Exception $e) {
            Error.log($e);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Package $item)
    {
        if(!Auth::check()) return view('auth.login');
        $pageTitle = 'Edit Package';
        try {
            
            if (!empty($item) && $item != null){
                return view('content.packages.edit', compact('item','pageTitle'));
            }
            
        } catch (Exception $e) {
            Error.log($e);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Package $model, Requests\UpdatePackageRequest $request)
    {
        if(!Auth::check()) return view('auth.login');
        $response = 'Null';
        $classstyle = 'danger';

        if (!empty($request->get('delete')) && !empty($model))
        {
            # code to delete selected item
            return $this->destroy($model);
        } else if(!empty($request) && !empty($model)) {
            # code to update selected item
            DB::beginTransaction();
            try {
                
                $model->title = $request->packageno;
                $model->amount = $request->amount;
                $model->first_fifteen = $request->first_fifteen;
                $model->second_fifteen = $request->second_fifteen;
                $model->third_fifteen = $request->third_fifteen;
                $model->color = $request->color;
                $model->save();
                DB::commit();

                $response = 'Record was successfuly updated...';
                $classstyle = 'success';
            } catch(Exception $e) {
                $response = $e->getMessage();
                DB::rollback();
            }
        }

        return redirect('packages')->with('message',$response)->with('classstyle',$classstyle);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Package $item)
    {
        if(!Auth::check()) return view('auth.login');
        Package::destroy($item->id);
        
        $response = 'Package '. $item->title .' was successfuly deleted...';
        $classstyle = 'success';
        return redirect('packages')->with('message',$response)->with('classstyle',$classstyle);
    }
}
