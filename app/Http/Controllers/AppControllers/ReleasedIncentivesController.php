<?php

namespace App\Http\Controllers\AppControllers;

use Illuminate\Http\Request;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;

use App\AppCore\Models\ReleasedIncentives;

class ReleasedIncentivesController extends Controller
{
    private $released_incentive;
    
    public function __construct(ReleasedIncentives $item)
    {
        $this->released_incentive = $item;
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(!Auth::check()) return view('auth.login');
        
        $pageTitle = 'Relased Incentives';
        try
        {
            if ($request != null && $request->get('q') != null && $request->get('sb') !=null
                && strlen($request->get('q')) > 0 && strlen($request->get('sb')) > 0) {
                $q = $request->get('q'); $sb = $request->get('sb');
                
                $released = DB::table('profile')
                    ->select('first_name', 'last_name', 'middle_name', 
                                'released_incentives.grand_total', 'released_incentives.id AS ri_id',
                                'released_incentives.created_at AS released_date', 
                                'users.name AS released_by')
                    ->join('member', 'profile.id', '=', 'member.profile_id')
                    ->join('released_incentives', 'member.id', '=', 'released_incentives.member_id')
                    ->join('users', 'released_incentives.released_by_id', '=', 'users.id')
                    ->where( 
                            ( $sb=='pid'? 'profile.id' : ($sb =='email'? 'email': ($sb =='mb'? 'mobile_no': ( $sb =='ln'? 'profile.last_name': 'profile.first_name') ) )  ),
                            ( $sb=='pid'? '=' : 'LIKE' ),
                            ( $sb=='pid'? $q : '%'.$q.'%' )
                            )
                    ->whereNull('released_incentives.deleted_at')
                    ->orderBy('released_incentives.created_at', 'desc')
                    ->paginate(10);
            }
            else {
                $released = DB::table('profile')
                    ->select('first_name', 'last_name', 'middle_name', 
                                'released_incentives.grand_total', 'released_incentives.id AS ri_id',
                                'released_incentives.created_at AS released_date', 
                                'users.name AS released_by')
                    ->join('member', 'profile.id', '=', 'member.profile_id')
                    ->join('released_incentives', 'member.id', '=', 'released_incentives.member_id')
                    ->join('users', 'released_incentives.released_by_id', '=', 'users.id')
                    ->whereNull('released_incentives.deleted_at')
                    ->orderBy('released_incentives.created_at', 'desc')
                    ->paginate(10);
            }
            $released->setPath('released_incentives');
            return view('content.released_incentives.index', compact('pageTitle','released'));
        } catch(Exception $e) {
            Error.log($e);
        }
    }
    
    public function getSalesDetails(Request $request)
    {
        if(!Auth::check()) return view('auth.login');
        
        try {
            if($request->ajax()) {
                $salesData = $_POST['pSales'];
                $salesids = "(";
                foreach($salesData as $sales) {
                    $salesids .= $sales.',';
                }
                $salesids = substr_replace($salesids, '', -1);
                $salesids .= ")";
                
                $salesDetails = DB::table('sales')->whereIn('id', $salesData)->get();
                
                $data = [
                    'sales' => $salesDetails,
                    'success' => TRUE
                ];
            } else {
                $data = [
                    'success' => FALSE
                ];
            }
            return response()->json($data, 200, [], JSON_PRETTY_PRINT); 
        } catch(Exception $e) {
            Error.log($e);
        }
    }
    
    public function getPurchases(Request $request)
    {
        if(!Auth::check()) return view('auth.login');
        
        try {
            if($request->ajax()) {
                $salesid = $_POST['pMemberId'];
                $salesDetails = DB::table('salesdetails')->where('sales_order_id', '=', $salesid)->get();
                $data = [
                    'sales' => $salesDetails,
                    'success' => TRUE
                ];
            } else {
                $data = [
                    'success' => FALSE
                ];
            }
            
            return response()->json($data, 200, [], JSON_PRETTY_PRINT); 
        } catch(Exception $e) {
            Error.log($e);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(ReleasedIncentives $item)
    {
        if(!Auth::check()) return view('auth.login');
        $pageTitle = 'Released Incentives';
        try {
            $released = DB::table('released_incentives')
                ->select('released_incentives.id', 'released_incentives.member_id', 
                         'first_name', 'profile.last_name', 'profile.middle_name', 'profile.registration_date', 
                         'released_incentives.grand_total', 'released_incentives.sales_ids',
                         'released_incentives.from', 'released_incentives.to',
                         'released_incentives.created_at AS released_date', 
                         'users.name AS released_by')
                ->join('member', 'member.id', '=', 'released_incentives.member_id')
                ->join('profile', 'profile.id', '=', 'member.profile_id')
                ->join('users', 'released_incentives.released_by_id', '=', 'users.id')
                ->where('released_incentives.id', '=', $item->id)
                ->orderBy('released_incentives.created_at', 'desc')->first();
            if (!empty($item) && $item != null){
                return view('content.released_incentives.show', compact('released','pageTitle'));
            }
            
        } catch (Exception $e) {
            Error.log($e);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        if(!Auth::check()) return view('auth.login');
        
        try {
            if($request->ajax()) {
                $id = $_POST['released_incentives_id'];
                if(isset($id)) {
                    ReleasedIncentives::destroy($id);
                    
                    $response = 'Released incentive record was successfuly deleted...';
                    return response()->json(['response' => $response]);
                }
            }
        } catch(Exception $e) {
            Error.log($e);
        }
    }
}
