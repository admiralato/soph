<?php

namespace App\Http\Controllers\AppControllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;

class TestingController extends Controller
{
    private $level = 0;
    private function setLevel($value){
        $this->level = $value;
    }

    public function getLevel(){
        return $this->level;
    }

    private $memberCollection = array();

    private function setMemberCollection($value){
        $this->memberCollection = $value;
    }

    public function getMemberCollection(){
        return $this->memberCollection;
    }

    public function index(Request $request)
    {
        $activeRoot = $request->root_id; 
        $memberid = $request->id;
        $this->setMemberCollection($this->getMembers(null));
        //dd($this->getMemberCollection());
        $this->memberTree($memberid, $activeRoot);
        dd($this->getLevel());
    }

    private function memberTree($active_id, $root_id){
        $_level =$this->getLevel();

        $node = $this->arraySearch($this->getMemberCollection(),'member_id', $active_id);
        

        if ($node==null) {
            $this->setLevel($_level);
            return;
        } else {
            if ($node[0]->member_id == $root_id) return;
            $_level = $_level + 1;
            $this->setLevel($_level);
            $this->memberTree($node[0]->parent_id, $root_id);
        }
        
    }

    private function getMembers($parent_id){
        $collection = null;

        try {
            if ($parent_id != null && is_numeric($parent_id)) {
                $collection = DB::select("SELECT m.id AS 'member_id', m.parent_id, m.profile_id, CONCAT(p.first_name, ' ',p.last_name) AS 'name', p.gender FROM member m LEFT JOIN profile p ON m.profile_id = p.id WHERE parent_id = $parent_id AND m.deleted_at is null");
            } else {
                $collection = DB::select("SELECT m.id AS 'member_id', m.parent_id, m.profile_id, CONCAT(p.first_name, ' ',p.last_name) AS 'name', p.gender FROM member m LEFT JOIN profile p ON m.profile_id = p.id WHERE m.deleted_at is null");
            }
        } catch (Exception $e) {
            Error.log($e);
        }
        return $collection;
    }

//    private function memberTreeOne($active_id){
//        $_level =$this->getLevel();
//        $nodes = $this->arraySearch($this->getMemberCollection(),'member_id', $active_id);
//        
//
//        if (count($nodes) >0) {
//
//
//
//            $node = $this->arraySearch($this->getMemberCollection(),'member_id', $nodes[0]->parent_id);
//
//            if ($node==null) {
//                $this->setLevel($_level);
//                return;
//            } else {
//                $_level = $_level + 1;
//                $this->setLevel($_level);
//                $this->createTree($node->parent_id);
//            }
//        } else {
//            return;
//        }
//        
//    }

    private function arraySearch($array, $index, $value) {
        $collection = array();
        foreach($array as $arrayInf) {
            if($arrayInf->{$index} == $value) {
                array_push($collection, $arrayInf);
            }
        }
        return $collection;
    }
}
