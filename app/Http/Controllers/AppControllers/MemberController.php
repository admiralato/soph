<?php

namespace App\Http\Controllers\AppControllers;

use Illuminate\Http\Request;

use Crypt;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Carbon\Carbon;
use App\AppCore\Models\Member;

class MemberController extends Controller
{
    private $member;


    public function __construct(Member $item)
    {
        $this->member = $item;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(!Auth::check()) return view('auth.login');
        
        $pageTitle = 'Member Manager';
        try {
            $q = NULL; $sb = NULL;

            if ($request != null && $request->get('q') != null && $request->get('sb') !=null
                && strlen($request->get('q')) > 0 && strlen($request->get('sb')) > 0) {
                $q = $request->get('q'); $sb = $request->get('sb');
                
                $members = DB::table('member')
                    ->leftJoin('profile', 'profile_id', '=', 'profile.id')
                    ->leftJoin('sales', 'member.id', '=', 'sales.member_id')
                    ->select(
                        'member.id', 
                        DB::raw('IFNULL(member.parent_id,0) as parent_id'),
                        'member.profile_id',
                        'member.created_at',
                        'profile.last_name', 
                        'profile.first_name',
                        'profile.gender',
                        DB::raw('SUM(IFNULL(sales.grand_total,0)) as sum_grand_total'),
                        DB::raw('IFNULL(sales.created_at,"-") as last_purchase_date')
                        )
                    ->whereNull('member.deleted_at')    
                    ->where( 
                        ($sb=='mid'? 'member.id' : ($sb =='sid'? 'parent_id': ( $sb =='ln'? 'profile.last_name': 'profile.first_name') )  ),
                        ($sb=='mid'? '=' : ($sb =='sid'? '=': ( $sb =='ln'? 'LIKE': 'LIKE') )  ),
                        ($sb=='mid'? $q : ($sb =='sid'? $q: ( $sb =='ln'? '%'.$q.'%': '%'.$q.'%') )  )
                        )    
                    ->groupBy('member.id')
                    ->orderBy('sales.created_at', 'desc')
                    ->paginate(8);

                $members->setPath("member?q=$q&sb=$sb");
            } else {
                $members = DB::table('member')
                    ->leftJoin('profile', 'profile_id', '=', 'profile.id')
                    ->leftJoin('sales', 'member.id', '=', 'sales.member_id')
                    ->select(
                        'member.id', 
                        DB::raw('IFNULL(member.parent_id,0) as parent_id'),
                        'member.profile_id',
                        'member.created_at',
                        'profile.last_name', 
                        'profile.first_name',
                        'profile.gender',
                        DB::raw('SUM(IFNULL(sales.grand_total,0)) as sum_grand_total'),
                        DB::raw('IFNULL(sales.created_at,"-") as last_purchase_date')
                        )
                    ->whereNull('member.deleted_at')    
                    ->groupBy('member.id')
                    ->orderBy('sales.created_at', 'desc')
                    ->paginate(8);

                $members->setPath('member');
            }

            
            
            /*
            $members = DB::table('member')
                ->leftJoin('profile', 'profile_id', '=', 'profile.id')
                ->select(
                    'member.*', 
                    'profile.last_name', 
                    'profile.first_name',
                    'profile.gender')
                ->whereNull('member.deleted_at')
                ->orderBy('member.created_at', 'desc')
                ->paginate(4);
            */
        
            return view('content.member.index', compact('pageTitle','members'));
        } catch (Exception $e) {
            Error.log($e);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($parentid, Request  $request)
    {
        if(!Auth::check()) return view('auth.login');
        
        $pageTitle = 'Add New Member';

        if ($parentid=="0") {
             $item = new Member();
             $item->id = 0;
             $item->parent_id = 0;
             $item->first_name = 'Parent';
             $item->last_name = '';
        } else {
            /*
            $item = DB::table('member')
                ->leftJoin('profile', 'profile_id', '=', 'profile.id')
                ->select(
                    'member.*', 
                    'CONCAT(profile.first_name,` `,profile.last_name) AS name')
                ->where('member.id','=',$parentid)
                ->first();
            */
            $item = DB::select("SELECT m.*, CONCAT(p.first_name,' ',p.last_name) AS 'name' FROM member m LEFT JOIN profile p ON m.profile_id = p.id WHERE m.deleted_at IS NULL AND m.id=$parentid");
            if ($item && count($item)>0) $item = $item[0];
        }

        $packages = DB::table('packages')
            ->select(
            'id',
            'title',
            'color',
            DB::raw('IFNULL(FORMAT(amount,2),0) as amount'),
            DB::raw('IFNULL(FORMAT(first_fifteen,2),0) as first_fifteen'),
            DB::raw('IFNULL(FORMAT(second_fifteen,2),0) as second_fifteen'),
            DB::raw('IFNULL(FORMAT(third_fifteen,2),0) as third_fifteen')
            )
            ->get();
            

        //dd($packages);
        if ($request && $request->shortcut && $request->shortcut === "1") {
            $pageTitle = 'Add New Member (Shortcut Version)';
            $view = 'content.member.create_shortcut';
        } else {
            $view = 'content.member.create';
        }
        
        return view($view, compact('item','packages','pageTitle'));    
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\CreateMemberRequest  $request)
    {
        if(!Auth::check()) return view('auth.login');
        
        DB::beginTransaction();
        $newid = -1;
        try
        {
            
            $model = new Member;
            $model->parent_id = $request->parent_id;
            $model->profile_id = $request->profile_id;
            $model->group_code = 'DEQUODE';
            $model->save();  
           
            DB::commit();
            $newid = $model->id;
            $response = 'New member was successfully added!';
            $classstyle = 'success';
        } catch(Exception $e) {
            $response = $e->getMessage();
            $classstyle = 'danger';
            DB::rollback();
        }
        
        return redirect('member/create/'. $request->parent_id . '?newid='. $newid)->with('message',$response)->with('classstyle',$classstyle);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        if(!Auth::check()) return view('auth.login');
        
        try {
            $memberId = $request->pMemberId;
            if($memberId) {
                Member::destroy($memberId);
                $response = 'Member ID: '. $memberId .' was successfuly deleted...';
                return response()->json(['response' => $response]);
            }
        } catch(Exception $e) {
            Error.log($e);
        }
    }
    
    public function profileNameSearch(Request $request)
    {
        $data = null;
        
        if ($request) {
            $q = $request->get('q');
            //dd($request->get('member') );
            try {
                $query = null;
                $query = "SELECT m.id AS id, m.parent_id, m.profile_id, CONCAT(p.first_name,' ',p.last_name) AS 'name' FROM member m LEFT JOIN profile p ON m.profile_id = p.id WHERE m.deleted_at IS NULL AND CONCAT(p.first_name, ' ', p.last_name) LIKE '%$q%' LIMIT 10";
                $searchBy = 'CHANGE_SPONSOR';

                if ($query != null)
                    $data = DB::select($query);
            
            } catch (Exception $e) {
                Error.log($e);
            }
        }
        return view('ajax.a_profile_name_dropdown', compact('data','searchBy'));
    }

    public function postSearch(Request $request){
        //dd($request);
    }
    
    public function changeSponsor($memberid, Request $request)
    {
        if(!Auth::check()) return view('auth.login');
        
        $pageTitle = 'Change Sponsor';
        $childProfile = null;
        $parentProfile = null;
        $memberId = $memberid;
        $currentParentId = null;
        try {
            if($memberid != null && is_numeric($memberId)) {
                $member = DB::table('member')
                    ->where('id', '=', $memberId)
                    ->whereNull('deleted_at')->first();
                if($member) {
                    $currentParentId = $member->parent_id;
                    // Child
                    $childProfile = DB::table('profile')
                        ->where('id', '=', $member->profile_id)
                        ->whereNull('deleted_at')->first();
                    
                    // Parent
                    $memberParent = DB::table('member')
                        ->where('id', '=', $member->parent_id)
                        ->whereNull('deleted_at')->first();
                    if(isset($memberParent)) {
                        $parentProfile = DB::table('profile')
                            ->where('id', '=', $memberParent->profile_id)
                            ->whereNull('deleted_at')->first();
                    }
                    
                }
                return view('content.member.change_sponsor', compact('pageTitle', 'memberId', 'currentParentId', 'childProfile', 'parentProfile'));
            }
        } catch (Exception $e) {
            Error.log($e);
        }
    }
    
    public function updateSponsor(Request $request) {
        if(!Auth::check()) return view('auth.login');
        $response = 'Null';
        $classstyle = 'danger';
        
        DB::beginTransaction();
        try {
            if($request) {
                DB::table('member')
                    ->where('id', $request->member_id)
                    ->update(['parent_id' => $request->new_sponsor_id]);
                DB::commit();
                
                // Log this activity
                $this->updateLogs('Member '.$request->member_id.' : parent_id was changed from '.$request->old_sponsor_id.' to '.$request->new_sponsor_id);
                
                $response = 'Member sponsor was successfully updated...';
                $classstyle = 'success';
            }
            
            return redirect('member')->with('message',$response)->with('classstyle',$classstyle);
        } catch (Exception $e) {
            Error.log($e);
            DB::rollback();
        }
    }
    
    private function updateLogs($description)
    {
        DB::beginTransaction();
        
        try {
            if($description) {
                DB::table('logs')->insert(
                    [
                        'description' => $description, 
                        'user_id' => Auth::user()->id,
                        'created_at' => Carbon::now()
                    ]
                );
                DB::commit();
            }
        } catch (Exception $e) {
            Error.log($e);
            DB::rollback();
        }
    }
}
