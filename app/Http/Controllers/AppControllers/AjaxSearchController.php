<?php

namespace App\Http\Controllers\AppControllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;

use App\AppCore\Models\Member;

class AjaxSearchController extends Controller
{
    private $memberCollection = array();

    private function setMemberCollection($value){
        $this->memberCollection = $value;
    }

    public function getMemberCollection(){
        return $this->memberCollection;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function profileNameSearch(Request $request)
    {
        $data = null;
        
        if ($request) {
            $q = $request->get('q');
            //dd($request->get('member') );
            try {
                $query = null;
                if ($request->get('profile') != null) {
                    //$data = DB::select("select id, first_name, last_name from profile WHERE deleted_at IS NULL AND (first_name LIKE '%$q%' OR last_name LIKE '%$q%') LIMIT 5");
                    $query = "select id, CONCAT(first_name,' ',last_name) AS 'name' from profile WHERE deleted_at IS NULL AND CONCAT(first_name, ' ', last_name) LIKE '%$q%' LIMIT 10";
                    $searchBy = 'PROFILE';
                } else if ($request->get('sponsor') != null){
                    $query = "SELECT m.id AS id, m.parent_id, m.profile_id, CONCAT(p.first_name,' ',p.last_name) AS 'name' FROM member m LEFT JOIN profile p ON m.profile_id = p.id WHERE m.deleted_at IS NULL AND CONCAT(p.first_name, ' ', p.last_name) LIKE '%$q%' LIMIT 10";
                    $searchBy = 'SPONSOR';
                    if ($request->shortcut && $request->shortcut==="1")
                        $searchBy = 'SPONSORSHORTCUT';
                } else if ($request->get('catalog') != null){
                    $query = "SELECT m.id AS id, m.parent_id, m.profile_id, CONCAT(p.first_name,' ',p.last_name) AS 'name' FROM member m LEFT JOIN profile p ON m.profile_id = p.id WHERE m.deleted_at IS NULL AND CONCAT(p.first_name, ' ', p.last_name) LIKE '%$q%' LIMIT 10";
                    $searchBy = 'CATALOG';
                } else {
                    $query = "SELECT m.id AS id, m.parent_id, m.profile_id, CONCAT(p.first_name,' ',p.last_name) AS 'name' FROM member m LEFT JOIN profile p ON m.profile_id = p.id WHERE m.deleted_at IS NULL AND CONCAT(p.first_name, ' ', p.last_name) LIKE '%$q%' LIMIT 10";
                    $searchBy = 'MEMBER';
                }

                if ($query != null)
                    $data = DB::select($query);

            
            } catch (Exception $e) {
                Error.log($e);
            }
        }
        return view('ajax.a_profile_name_dropdown', compact('data','searchBy'));
    }

    public function memberNameSearch(Request $request)
    {
        $data = null;
        $searchBy = 'MEMBER';
        if ($request) {
            $q = $request->get('q');

            try {
                //$data = DB::select("select id, first_name, last_name from profile WHERE deleted_at IS NULL AND (first_name LIKE '%$q%' OR last_name LIKE '%$q%') LIMIT 5");
                $data = DB::select("SELECT m.id AS id, m.parent_id, m.profile_id, CONCAT(p.first_name,' ',p.last_name) AS 'name' FROM member m LEFT JOIN profile p ON m.profile_id = p.id WHERE m.deleted_at IS NULL AND CONCAT(p.first_name, ' ', p.last_name) LIKE '%q%' LIMIT 10");
            } catch (Exception $e) {
                Error.log($e);
            }
        }
        return view('ajax.a_profile_name_dropdown', compact('data','searchBy'));
    }

    public function getDownlines(Request $request)
    {
        $jsonArray = array();
        $parentid = null;
        $memberid = 0;
        if ($request && is_numeric($request->memberid)) $memberid = (int)$request->memberid;

        $html = '';
        if ($request && is_null($request->parentid)) {
            //$this->$_memberCollection = $this->getMembers(null);
            $this->setMemberCollection($this->getMembers(null));
            $parentid = 0;
        } else {
            $parentid = $request->parentid;
//            $this->setMemberCollection($this->getMembers($parentid));
        }
        
        if( $this->getMemberCollection() && count($this->getMemberCollection()) > 0 ) {
            $nodes = $this->arraySearch($this->getMemberCollection(),'parent_id', $parentid);
            
            if ($nodes && count($nodes) > 0) {
                $html .= '<ul class="treeview">';

                //<i class="fa fa-user"></i>
                //<i class="fa fa-user"></i>

                foreach ($nodes as $key => $node) {
                    $new = '';
                    $selected_attr = ''; if ($memberid === $node->member_id) { $selected_attr = 'class="active"'; $new ='<small class="label pull-right bg-green">new</small>'; }
                    
                    $nonde_icon = '<i class="fa fa-user"></i>'; $children = $this->arraySearch($this->getMemberCollection(),'parent_id', $node->member_id);
                    if (count($children) > 0 ) {
                        $nonde_icon = '<i class="fa fa-users"></i>'; 
                    } 
                    
                    $html .= '<li id="'.$node->member_id.'" parentid="'. $node->parent_id .'" '.$selected_attr.'>';
                    $html .= "<span class='child-title'>$nonde_icon $node->name</span>";
                    $html .= $new;
                    $html .= $this->createTree($node->member_id, $memberid);
                    $html .= '</li>';
                }
                $html .= '</ul>';
            }    
        }

        return $html;
        //$treeview = $html; 
        //return view('ajax.a_profile_name_dropdown', compact('treeview','memberid'));
            
    }
    
    private function arraySearch($array, $index, $value) {
        $collection = array();
        foreach($array as $arrayInf) {
            if($arrayInf->{$index} == $value) {
                array_push($collection, $arrayInf);
                //return $arrayInf; 
            }
        }
        return $collection;
    }

    private function createTree($parent_id, $selected_id){
        $html = '';
        $selected_attr = '';
        $nodes = $this->arraySearch($this->getMemberCollection(),'parent_id', $parent_id);
        //dd(count($nodes));
        if (count($nodes) >0) {
            $html .= '<ul>';
            foreach ($nodes as $key => $node) {
                $new = '';
                $selected_attr = ''; if ($selected_id === $node->member_id) { $selected_attr = 'class="active"'; $new ='<small class="label pull-right bg-green">new</small>'; }
                $nonde_icon = '<i class="fa fa-user"></i>'; $children = $this->arraySearch($this->getMemberCollection(),'parent_id', $node->member_id);
                if (count($children) > 0 ) {
                    $nonde_icon = '<i class="fa fa-users"></i>'; 
                } 

                $html .= '<li id="'.$node->member_id.'" parentid="'. $node->parent_id .'" '.$selected_attr.'>';
                $html .= "<span class='child-title'>$nonde_icon $node->name</span>";
                $html .= $new;
                $html .= $this->createTree($node->member_id, $selected_id);
                $html .= '</li>';
            }
            $html .= '</ul>';
        }
        return $html;
    }

    private function getMembers($parent_id){
        $collection = null;

        try {
            if ($parent_id != null && is_numeric($parent_id)) {
                $collection = DB::select("SELECT m.id AS 'member_id', m.parent_id, m.profile_id, CONCAT(p.first_name, ' ',p.last_name) AS 'name', p.gender FROM member m LEFT JOIN profile p ON m.profile_id = p.id WHERE parent_id = $parent_id AND m.deleted_at is null");
            } else {
                $collection = DB::select("SELECT m.id AS 'member_id', m.parent_id, m.profile_id, CONCAT(p.first_name, ' ',p.last_name) AS 'name', p.gender FROM member m LEFT JOIN profile p ON m.profile_id = p.id WHERE m.deleted_at is null");
            }
        } catch (Exception $e) {
            Error.log($e);
        }
        return $collection;
    }

    
}
