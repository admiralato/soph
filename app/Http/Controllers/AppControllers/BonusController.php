<?php

namespace App\Http\Controllers\AppControllers;

use Illuminate\Http\Request;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Carbon;
use App\AppCore\Models\Incentive;
use App\AppCore\Models\ReleasedIncentives;
use App\AppCore\Models\ReleasedIncentivesDetails;
use Excel;

class BonusController extends Controller
{
    private $root_parent_id = 0;
    private $level = 0;
    private $parents = null;
    private $memberCollection = array();
    private $mTree = array();
    
    private function setParents($value)
    {
        $this->parents = $value;
    }
    
    private function getParents()
    {
        return $this->parents;
    }
    
    private function setLevel($value){
        $this->level = $value;
    }

    public function getLevel(){
        return $this->level;
    }

    private function setMemberCollection($value){
        $this->memberCollection = $value;
    }

    public function getMemberCollection(){
        return $this->memberCollection;
    }
    
    private function setTree($value) {
        $this->mTree = $value;
    }
    
    private function getTree() {
        return $this->mTree;
    }
    
    public function getDownlines(Request $request) 
    {
        if(!Auth::check()) return view('auth.login');
        
        $html = "";
        $json = "";
        $grandTotalHtml = "";
        $parentid = null;
        $memberid = 0;
        $member = null;
        $lastPurchaseDate = null;
        $isMemberActive = true;
        $lastIncentiveDate = null;
        $coverageFrom = null;
        $coverageTo = null;
        $allSales = "[";
        
        try {
            
            if ($request && is_numeric($request->id)) {
                $memberid = (int)$request->id;
                $this->root_parent_id = $memberid;
            }

            if ($request && is_null($request->id)) {
                //$this->setMemberCollection($this->getMembers(null));
                //$parentid = 0;
            } else {
                $parentid = $request->id;
                $this->setMemberCollection($this->getMembers($parentid));
                $member = $this->getSelectedMember($parentid);
                
                // Get last purchase date and see if it's within the current month
                $lastPurchaseDate = $this->getLastPurchaseDate($parentid);
                if(empty(array_filter($lastPurchaseDate))) $lastPurchaseDate = null;
                $firstDayofThisMonth = Carbon\Carbon::now()->startOfMonth();
                $lastDayofThisMonth = Carbon\Carbon::now()->endOfMonth();

                if($lastPurchaseDate != null) {
                    // A member is active if he made a purchase within a new month (i.e between Jan 01-31), otherwise he will be treated as inactive even if he purchases at the last day of the previous month!
                    /*
                    #1 Get current month
                    #2 If $lastPurchaseDate is within current month then $isMemberActive = TRUE else FALSE
                    */
                    // ENABLED TO VERIFY IS ACTIVE/INACTIVE
                    $isMemberActive = strtotime($lastPurchaseDate[0]->created_at) >= strtotime($firstDayofThisMonth) && strtotime($lastPurchaseDate[0]->created_at) <= strtotime($lastDayofThisMonth);
                }
            }

            if( $this->getMemberCollection() && count($this->getMemberCollection()) > 0 ) {
                // Get last incentive date
                $lastIncentiveDate = $this->getLastIncentiveDate($parentid);
                // If member did not get any incentives yet, set $lastIncentiveDate to his date of registration;
                if(empty(array_filter($lastIncentiveDate))) {
                    $lastIncentiveDate = $this->getMemberRegistrationDateTime($parentid);
                    $lastIncentiveDate[0]->created_at = $lastIncentiveDate[0]->created_at.' 00:00:00';
                } 
                
                $coverageFrom = $lastIncentiveDate[0]->created_at;
                $coverageTo = Carbon\Carbon::now()->toDateTimeString();
                
                $tree = $this->buildUnilevelTree($this->getMembers(null), $parentid, $lastIncentiveDate);
                if($tree != null) {
                    $incentives = $this->getUnilevels(); // doesn't include deleted_at != null
                    $groups = $this->groupPerlevel($tree);
                    $grandTotal = 0;
                    $totalProduct = 0;
                    // Loop through each incentive
                    $json = '[';
                    foreach($incentives as $incentive) {
                        $totalProductPerLevel = 0;
                        $totalIncentivePerLevel = 0;
                        // loop through each unilevel group from $groups
                        // total everything
                        /* FORMAT
                            <tr>
                                <td>1</td>
                                <td>12</td>
                                <td>{{ number_format('1200', 2) }}</td>
                            </tr>
                        */
                        $memberDetail = '';
                        $html .= '<tr>';
                        $html .= '<td>'.$incentive->level.'</td>';
                        $json .= '{"level":'.$incentive->level;
//                        dd($groups);
                        foreach($groups as $key => $val) {
                            foreach($val as $group) {
                                if($incentive->level == $group->unilevel) {
                                    $incentiveDate = null;
                                    $sales_ids = "[";
                                    foreach($group->sales_ids as $sales)
                                    {
                                        $allSales .= $sales.",";
                                        $sales_ids .= $sales.",";
                                    }
                                    $sales_ids = substr_replace($sales_ids, '', -1);
                                    $sales_ids .= "]";
                                    
                                    if($lastIncentiveDate != null) {
                                        $incentiveDate = addslashes($lastIncentiveDate[0]->created_at);
                                    }
                                    $memberDetail .= '{"name":"'. addslashes($group->name).'", "purchases":"'. addslashes($group->purchases).'", "memberid":"'.addslashes($group->member_id).'", "lastIncentiveDate":"'.$incentiveDate.'", "sales":"'.$sales_ids.'"},';
                                    $totalProductPerLevel += $group->purchases;
                                }
                            }
                        }
                        
                        
                        $memberDetail = substr_replace($memberDetail, '', -1);
                        $totalProduct += $totalProductPerLevel;
                        $totalIncentivePerLevel = $incentive->incentive * $totalProductPerLevel;
                        $grandTotal += $totalIncentivePerLevel;
                        $html .= '<td>'.$totalProductPerLevel.'</td>';
                        $html .= '<td>'.number_format($totalIncentivePerLevel, 2).'</td>';
                        $html .= '</tr>';
                        
                        $json .= ', "products":'.$totalProductPerLevel;
                        $json .= ', "incentive":"'.number_format($totalIncentivePerLevel, 2);
                        $json .= '", "detail":['.$memberDetail.']';
                        $json .= '},';
                    }
                    $allSales = substr_replace($allSales, '', -1);
                    $allSales .= "]";
//                    dd($allSales);
                    
                    $json = substr_replace($json, '', -1);
                    $json .= ']';
//                    dd($json);
                    /*
                        <tr>
                            <td><strong>TOTAL</strong></td>
                            <td>17</td>
                            <td><strong>{{ number_format('1250', 2) }}</strong></td>
                        </tr>
                    */
                    $grandTotalHtml = '<tr>';
                    $grandTotalHtml .= '<td><strong>TOTAL</strong></td>';
                    $grandTotalHtml .= '<td>'.$totalProduct.'</td>';
                    $grandTotalHtml .= '<td>'.number_format($grandTotal, 2).'</td>';
                    $grandTotalHtml .= '</tr>';
                }
                
                return response()->json([ 'dataSet' => $json, 'incentive' => $html, 'grandTotalHtml' => $grandTotalHtml, 'grandTotal' => number_format($grandTotal, 2), 'memberName' => $member[0]->name, 'coverageFrom' => $coverageFrom, 'coverageTo' => $coverageTo, 'sales_ids' => $allSales, 'status' => $isMemberActive]);
            }
        } catch(Exception $e) {
            Error.log($e);
        }
    }
    
    public function getAjaxDownlines(Request $request) 
    {
        if(!Auth::check()) return view('auth.login');
        
        $memberid = null;
        $parentid = null;
        $sponsor = null;
        $html = "";
        $level = 1;
        $maxLevel = 1;
        $totalIncentives = 0;
        
        try {
            if($request->ajax()) {
                $memberid = $_POST['pMemberId'];
                $salesFrom = $_POST['pSalesFrom'];
                $salesTo = $_POST['pSalesTo'];
//                $level = $_POST['pLevel'];
                
                $salesFrom = date("Y-m-d", strtotime($salesFrom));
                $salesTo = date("Y-m-d", strtotime($salesTo));
                
                $this->root_parent_id = $memberid;
                
                $this->setMemberCollection($this->getMembers(null));
                $member = $this->getMemberProfile($memberid);
                $sponsor = $this->getParent($memberid);

                if( $this->getMemberCollection() && count($this->getMemberCollection()) > 0 ) {
                    $tree = $this->buildDownlineTree($this->getMemberCollection(), $memberid, $salesFrom, $salesTo);
                    if($tree != null) {
                        $incentives = $this->getUnilevels(); // doesn't include deleted_at != null
                        $maxLevel = count($incentives);
                        $groups = $this->groupPerlevel($tree);
//                        dd($groups);

                        foreach($incentives as $incentive) {
//                            if($incentive->level == $level) {
//                                $html .= '<tr>';
//                                $html .= '<td colspan="5" style="font-weight:bold;">LEVEL '.$incentive->level.'</td>';
//                                $html .= '</tr>';

                                foreach($groups as $key => $val) {
                                    foreach($val as $group) {
                                        if($incentive->level == $group->unilevel) {
                                            $html .= '<tr>';
                                            $html .= '<td colspan="5">'.$group->name.'</td>';
                                            $purchases = $this->getTotalProductPurchased2($group->member_id, $salesFrom, $salesTo);
                                            if($purchases != null) {
                                                foreach($purchases as $purchase) {
                                                    $totalIncentives += ($incentive->incentive * $purchase->package);
                                                    $html .= '<tr>';
                                                    $html .= '<td colspan="2" style="text-align:center;">'.$purchase->sales_datetime.'</td>';
                                                    $html .= '<td>Package '.$purchase->package.'</td>';
                                                    $html .= '<td>'.number_format($purchase->price, 2).'</td>';
                                                    $html .= '<td>'.number_format($incentive->incentive * $purchase->package, 2).'</td>';
                                                    $html .= '</tr>';
                                                }
                                            }
                                            $html .= '</tr>';
                                        }
                                    }
                                }
//                            }
                        }
                    }
                }
                
                return response()->json(['html' => $html, 'from' => $salesFrom, 'to' => $salesTo, 'maxLevel' => $maxLevel, 'level' => $level, 'totalIncentives' => $totalIncentives]);
            }
        } catch(Exception $e) {
            Error.log($e);
        }
    }
    
    private function groupPerlevel(array $items)
    {
        $templevel = 1;
        $newkey = 0;
        $grouparr[$templevel] = null;
        foreach($items as $key => $val) {
            if($templevel == $val->unilevel) {
                $grouparr[$templevel][$newkey] = $val;
            } else {
                $grouparr[$val->unilevel][$newkey] = $val;
            }
            $newkey++;
        }
        return $grouparr;
    }
    
    private function buildSequentialTree(array $elements, $parentIds, $salesFrom = null, $salesTo = null)
    {
        $parentIdString = "";
        $purchases = null;
        $children = null;
        try {
            if(isset($parentIds)) {
                $children = $this->getChildren($parentIds);
                if(isset($children)) {
                    foreach($children as $child) {
                        $parentIdString .= $child->member_id.",";
                    }
                    $parentIdString = substr_replace($parentIdString, '', -1);
                    $this->setParents($parentIdString);
                }
            }
        } catch(Exception $e) {
            Error.log($e);
        }
        return $children;
    }
    
    private function getAllDownlines($parentIds)
    {
        $parentIdString = "";
        $children = null;
        try {
            if(isset($parentIds)) {
                $children = $this->getChildren($parentIds);
                if(isset($children)) {
                    array_push($this->mTree, $children);
                    foreach($children as $child) {
                        $parentIdString .= $child->member_id.",";
                    }
                    $parentIdString = substr_replace($parentIdString, '', -1);
                    $this->getAllDownlines($parentIdString);
                }
            }
        } catch(Exception $e) {
            Error.log($e);
        }
        return $children;
    }
    
    public function getDownlinesSequentially(Request $request) 
    {
        if(!Auth::check()) return view('auth.login');
        
        $memberid = null;
        $parentid = null;
        $sponsor = null;
        $html = "";
        $level = 1;
        $maxLevel = 1;
        $totalIncentives = 0;
        
        try {
            if($request->ajax()) {
                $memberid = $_POST['pMemberId'];
                $salesFrom = $_POST['pSalesFrom'];
                $salesTo = $_POST['pSalesTo'];
                $level = $_POST['pLevel'];
                $parents = $_POST['pParents'];
                
                
                $salesFrom = date("Y-m-d", strtotime($salesFrom));
                $salesTo = date("Y-m-d", strtotime($salesTo));
                
                $this->root_parent_id = $memberid;
                
                $this->setMemberCollection($this->getMembers(null));
                $member = $this->getMemberProfile($memberid);
                $sponsor = $this->getParent($memberid);

                if( $this->getMemberCollection() && count($this->getMemberCollection()) > 0 ) {
                    $tree = $this->buildSequentialTree($this->getMemberCollection(), $parents, $salesFrom, $salesTo);
                    if($tree != null) {
                        $incentives = $this->getUnilevels(); // doesn't include deleted_at != null
                        $maxLevel = count($incentives);

                        foreach($incentives as $incentive) {
                            if($incentive->level == $level) {
                                foreach($tree as $node) {
                                    $html .= '<tr>';
                                    $purchases = $this->getTotalProductPurchased2($node->member_id, $salesFrom, $salesTo);
                                    if($purchases != null) {
                                        $html .= '<td colspan="6" style="line-height:1px;">'.$node->name.'</td>';
                                        foreach($purchases as $purchase) {
                                            $taxDeduction = (($incentive->incentive * $purchase->package) * 10) / 100;
                                            $total = ($incentive->incentive * $purchase->package) - $taxDeduction;
                                            $totalIncentives += $total;
                                            
                                            $html .= '<tr>';
                                            $html .= '<td colspan="3" style="text-align:center;line-height:1px;">'.$purchase->sales_datetime.'</td>';
                                            $html .= '<td style="line-height:1px;">Package '.$purchase->package.'</td>';
                                            $html .= '<td style="line-height:1px;">'.number_format($incentive->incentive * $purchase->package, 2).'</td>';
                                            $html .= '<td style="line-height:1px;">'.number_format($taxDeduction, 2).'</td>';
                                            $html .= '<td style="line-height:1px;">'.number_format($total, 2).'</td>';
                                            $html .= '</tr>';
                                        }
                                    }
                                    $html .= '</tr>';
                                }
                            }
                        }
                    }
                }
                
                return response()->json(['html' => $html, 'member' => $member, 'sponsor' => $sponsor, 'from' => $salesFrom, 'to' => $salesTo, 'maxLevel' => $maxLevel, 'level' => $level, 'totalIncentives' => $totalIncentives, 'parents' => $this->getParents()]);
            }
        } catch(Exception $e) {
            Error.log($e);
        }
    }
    
    //--------------------------------------FOR COMPUTE INCENTIVES--------------------------------------
    public function getDownlinePurchases(Request $request)
    {
        if(!Auth::check()) return view('auth.login');
        
        $memberid = null;
        $parentid = null;
        $sponsor = null;
        $html = "";
        $level = 1;
        $maxLevel = 1;
        $totalIncentives = 0;
        $allSales = "";
        
        $isMemberActive = true;
        $lastPurchaseDate = null;
        $salesIdCollection = array();
        
        try {
            if($request->ajax()) {
                $memberid = $_POST['pMemberId'];
                $salesFrom = $_POST['pSalesFrom'];
                $salesTo = $_POST['pSalesTo'];
                $level = $_POST['pLevel'];
                $parents = $_POST['pParents'];
                
                
                $salesFrom = date("Y-m-d", strtotime($salesFrom));
                $salesTo = date("Y-m-d", strtotime($salesTo));
                
                $this->root_parent_id = $memberid;
                
                $this->setMemberCollection($this->getMembers(null));
                $member = $this->getMemberProfile($memberid);
                $sponsor = $this->getParent($memberid);
                
                //-- is member active/inactive
                $lastPurchaseDate = $this->getLastPurchaseDate($memberid);
                if(empty(array_filter($lastPurchaseDate))) $lastPurchaseDate = null;
                $firstDayofThisMonth = Carbon\Carbon::now()->startOfMonth();
                $lastDayofThisMonth = Carbon\Carbon::now()->endOfMonth();
                
                if($lastPurchaseDate != null) {
                    $isMemberActive = strtotime($lastPurchaseDate[0]->created_at) >= strtotime($firstDayofThisMonth) && strtotime($lastPurchaseDate[0]->created_at) <= strtotime($lastDayofThisMonth);
                }
                //-- end of checking whether member is active/inactive
                
                $salesIds = $this->getProcessedIncentives($memberid);
//                dd($salesIds);
                if($salesIds != "" && $salesIds != null) {
                    $salesIdCollection = explode(",", $salesIds);
                }
                
                if( $this->getMemberCollection() && count($this->getMemberCollection()) > 0 ) {
                    $tree = $this->buildSequentialTree($this->getMemberCollection(), $parents, $salesFrom, $salesTo);
                    if($tree != null) {
                        $incentives = $this->getUnilevels(); // doesn't include deleted_at != null
                        $maxLevel = count($incentives);

                        foreach($incentives as $incentive) {
                            if($incentive->level == $level) {
                                foreach($tree as $node) {
                                    $html .= '<tr>';
                                    $html .= '<td colspan="6">'.$node->name.'</td>';
                                    // Do not include sales id's that are already in the previous released incentive
                                    $purchases = $this->getTotalProductPurchased2($node->member_id, $salesFrom, $salesTo);
                                    if($purchases != null) {
                                        foreach($purchases as $purchase) {
                                            $html .= '<tr>';
                                            $html .= '<td colspan="2" style="text-align:center;">'.$purchase->sales_datetime.'</td>';
                                            $html .= '<td>Package '.$purchase->package.'</td>';
                                            $html .= '<td>'.number_format($purchase->price, 2).'</td>';
                                            $html .= '<td>'.number_format($incentive->incentive * $purchase->package, 2).'</td>';
                                            if(!empty(array_filter($salesIdCollection)) && 
                                               in_array($purchase->sales_id, $salesIdCollection)) {
                                                $html .= '<td style="vertical-align:inherit;"><span class="label label-danger">RELEASED</span></td>';
                                            } else {
                                                if($purchase->sales_id != "" && isset($purchase->sales_id)) {
                                                    $allSales .= $purchase->sales_id.",";
                                                }
                                                $html .= '<td style="vertical-align:inherit;"><span class="label label-success">FOR RELEASING</span></td>';
                                                $totalIncentives += ($incentive->incentive * $purchase->package);
                                            }
                                            $html .= '</tr>';
                                        }
                                    }
                                    $html .= '</tr>';
                                }
                            }
                        }
                        $allSales = substr_replace($allSales, '', -1);
                    }
                }
                
                return response()->json([
                    'html' => $html, 'member' => $member, 
                    'sponsor' => $sponsor, 'from' => $salesFrom, 
                    'to' => $salesTo, 'maxLevel' => $maxLevel, 
                    'level' => $level, 'totalIncentives' => $totalIncentives, 
                    'parents' => $this->getParents(), 'status' => $isMemberActive,
                    'sales_ids' => $allSales, 'processedSales' => $salesIdCollection]);
            }
        } catch(Exception $e) {
            Error.log($e);
        }
    }
    
    public function getRemainingPurchases(Request $request)
    {
        $html = "";
        $totalIncentives = 0;
        $processedSales = null;
        $salesIdCollection = array();
        $allSales = "";
        try {
            if($request->ajax()) {
                $salesFrom = $_POST['pSalesFrom'];
                $salesTo = $_POST['pSalesTo'];
                $level = $_POST['pLevel'];
                $parents = $_POST['pParents'];
                if(isset($_POST['pProcessedSales'])) $processedSales = $_POST['pProcessedSales'];
                
                $salesFrom = date("Y-m-d", strtotime($salesFrom));
                $salesTo = date("Y-m-d", strtotime($salesTo));
                $this->setMemberCollection($this->getMembers(null));
                
                if($processedSales != "" && $processedSales != null) {
                    $salesIdCollection = $processedSales;
                }

                if( $this->getMemberCollection() && count($this->getMemberCollection()) > 0 ) {
                    $tree = $this->buildSequentialTree($this->getMemberCollection(), $parents, $salesFrom, $salesTo);
                    if($tree != null) {
                        $incentives = $this->getUnilevels($level); // doesn't include deleted_at != null
                        foreach($incentives as $incentive) {
                            foreach($tree as $node) {
                                $html .= '<tr>';
                                $html .= '<td colspan="6">'.$node->name.'</td>';
                                $purchases = $this->getTotalProductPurchased2($node->member_id, $salesFrom, $salesTo);
                                if($purchases != null) {
                                    foreach($purchases as $purchase) {
                                        $html .= '<tr>';
                                        $html .= '<td colspan="2" style="text-align:center;">'.$purchase->sales_datetime.'</td>';
                                        $html .= '<td>Package '.$purchase->package.'</td>';
                                        $html .= '<td>'.number_format($purchase->price, 2).'</td>';
                                        $html .= '<td>'.number_format($incentive->incentive * $purchase->package, 2).'</td>';
                                        if(!empty(array_filter($salesIdCollection)) && 
                                           in_array($purchase->sales_id, $salesIdCollection)) {
                                            $html .= '<td style="vertical-align:inherit;"><span class="label label-danger">RELEASED</span></td>';
                                        } else {
                                            if($purchase->sales_id != "" && isset($purchase->sales_id)) {
                                                $allSales .= $purchase->sales_id.",";
                                            }
                                            
                                            $html .= '<td style="vertical-align:inherit;"><span class="label label-success">FOR RELEASING</span></td>';
                                            $totalIncentives += ($incentive->incentive * $purchase->package);
                                        }
                                        $html .= '</tr>';
                                    }
                                }
                                $html .= '</tr>';
                            }
                        }
                        $allSales = substr_replace($allSales, '', -1);
                    }
                }
                
                return response()->json(['html' => $html, 'from' => $salesFrom, 
                                         'to' => $salesTo, 'level' => $level, 
                                         'totalIncentives' => $totalIncentives, 
                                         'parents' => $this->getParents(),
                                         'sales_ids' => $allSales,
                                         'processedSales' => $processedSales]);
            }
        } catch(Exception $e) {
            Error.log($e);
        }
    }
    //----------------------------------------------END-------------------------------------------------
    
    public function getRemainingDownlines(Request $request)
    {
        $html = "";
        $totalIncentives = 0;
        try {
            if($request->ajax()) {
                $salesFrom = $_POST['pSalesFrom'];
                $salesTo = $_POST['pSalesTo'];
                $level = $_POST['pLevel'];
                $parents = $_POST['pParents'];
                
                $salesFrom = date("Y-m-d", strtotime($salesFrom));
                $salesTo = date("Y-m-d", strtotime($salesTo));
                $this->setMemberCollection($this->getMembers(null));

                if( $this->getMemberCollection() && count($this->getMemberCollection()) > 0 ) {
                    $tree = $this->buildSequentialTree($this->getMemberCollection(), $parents, $salesFrom, $salesTo);
                    if($tree != null) {
                        $incentives = $this->getUnilevels($level); // doesn't include deleted_at != null
                        foreach($incentives as $incentive) {
                            foreach($tree as $node) {
                                $html .= '<tr>';
                                $purchases = $this->getTotalProductPurchased2($node->member_id, $salesFrom, $salesTo);
                                if($purchases != null) {
                                    $html .= '<td colspan="6" style="line-height:1px;">'.$node->name.'</td>';
                                    foreach($purchases as $purchase) {
                                        $taxDeduction = (($incentive->incentive * $purchase->package) * 10) / 100;
                                        $total = ($incentive->incentive * $purchase->package) - $taxDeduction;
                                        $totalIncentives += $total;
                                        
                                        $html .= '<tr>';
                                        $html .= '<td colspan="2" style="text-align:center;line-height:1px;">'.$purchase->sales_datetime.'</td>';
                                        $html .= '<td style="line-height:1px;">Package '.$purchase->package.'</td>';
                                        $html .= '<td style="line-height:1px;">'.number_format($incentive->incentive * $purchase->package, 2).'</td>';
                                        $html .= '<td style="line-height:1px;">'.number_format($taxDeduction, 2).'</td>';
                                        $html .= '<td style="line-height:1px;">'.number_format($total, 2).'</td>';
                                        $html .= '</tr>';
                                    }
                                }
                                $html .= '</tr>';
                            }
                        }
                    }
                }
                
                return response()->json(['html' => $html, 'from' => $salesFrom, 'to' => $salesTo, 'level' => $level, 'totalIncentives' => $totalIncentives, 'parents' => $this->getParents()]);
            }
        } catch(Exception $e) {
            Error.log($e);
        }
    }
    
    private function getChildren($parent_ids) 
    {
        $collection = null;

        try {
            if ($parent_ids != null) {
                $query = "SELECT ";
                $query .= "m.id AS 'member_id', m.parent_id, m.profile_id, ";
                $query .= "CONCAT(p.first_name, ' ',p.last_name) AS 'name',p.gender ";
                $query .= "FROM ";
                $query .= "member m LEFT JOIN profile p ON m.profile_id = p.id ";
                $query .= "WHERE parent_id IN ($parent_ids) AND m.deleted_at is null ";
                $collection = DB::select($query);
            }
        } catch (Exception $e) {
            Error.log($e);
        }
        return $collection;
    }
    
    private function buildUnilevelTree(array $elements, $parentId = 0, $lastIncentiveDate = null) 
    {
        $branch = array();
        $purchases = null;
        $salesids = array();

        foreach ($elements as $key => $node) {
            $this->setLevel(0);
            $totalPurchases = 0;
            
            if ($node->parent_id == $parentId) {
                // Get number of purchases
                $purchases = $this->getTotalProductPurchased($node->member_id, $lastIncentiveDate);

                if($purchases != null) {
                    $salesids = array();
                    foreach($purchases as $key => $pack) {
                        $totalPurchases += $pack->package;
                        array_push($salesids, $pack->sales_id);
                    }
                }
                $node->purchases = $totalPurchases;
                $node->sales_ids = $salesids;
                //-- end get number of purchases
                
                // Get unilevel
                $this->memberTree($node->member_id, $this->root_parent_id);
                $node->unilevel = $this->getLevel();
                //-- end get unilevel
                
                $children = $this->buildUnilevelTree($elements, $node->member_id, $lastIncentiveDate);
                if ($children) {
                    // Get unilevel
                    $this->memberTree($children[0]->member_id, $this->root_parent_id);
                    $children[0]->unilevel = $this->getLevel();
                    //-- end get unilevel
                    $branch = array_merge($children, $branch);
                }
                
                $branch[] = $node;
            }
            $totalPurchases = 0;
        }
        return $branch;
    }
    
    private function buildDownlineTree(array $elements, $parentId = 0, $salesFrom, $salesTo) 
    {
        $branch = array();
        $purchases = null;
        $salesids = array();

        foreach ($elements as $key => $node) {
            $this->setLevel(0);
            $totalPurchases = 0;
            
            if ($node->parent_id == $parentId) {
                // Get number of purchases
                $purchases = $this->getTotalProductPurchased2($node->member_id, $salesFrom, $salesTo);

                if($purchases != null) {
                    $salesids = array();
                    foreach($purchases as $key => $pack) {
                        $totalPurchases += $pack->package;
                        array_push($salesids, $pack->sales_id);
                    }
                }
                $node->purchases = $totalPurchases;
                $node->sales_ids = $salesids;
                //-- end get number of purchases
                
                // Get unilevel
                $this->memberTree($node->member_id, $this->root_parent_id);
                $node->unilevel = $this->getLevel();
                //-- end get unilevel
                
                $children = $this->buildDownlineTree($elements, $node->member_id, $salesFrom, $salesTo);
                if ($children) {
                    // Get unilevel
                    $this->memberTree($children[0]->member_id, $this->root_parent_id);
                    $children[0]->unilevel = $this->getLevel();
                    //-- end get unilevel
                    $branch = array_merge($children, $branch);
                }
                
                $branch[] = $node;
            }
            $totalPurchases = 0;
        }
        return $branch;
    }
    
    private function memberTree($active_id, $root_id)
    {
        $_level =$this->getLevel();

        $node = $this->arraySearch($this->getMembers(null),'member_id', $active_id);
        

        if ($node==null) {
            $this->setLevel($_level);
            return;
        } else {
            if ($node[0]->member_id == $root_id) return;
            $_level = $_level + 1;
            $this->setLevel($_level);
            $this->memberTree($node[0]->parent_id, $root_id);
        }
        
    }
    
    private function arraySearch($array, $index, $value) 
    {
        $collection = array();
        foreach($array as $arrayInf) {
            if($arrayInf->{$index} == $value) {
                array_push($collection, $arrayInf);
            }
        }
        return $collection;
    }
    
    private function getUnilevels($level = null) 
    {
        $collection = null;
        try {
            if($level == null) {
                $collection = DB::select("SELECT level, incentive FROM soph.incentives where deleted_at is null");
            } else {
                $collection = DB::select("SELECT level, incentive FROM soph.incentives where level = $level AND deleted_at is null");
            }
            
        } catch (Exception $e) {
            Error.log($e);
        }
        return $collection;
    }
    
    private function getMemberProfile($id)
    {
        $profile = null;
        try {
            $profile = DB::table('profile')
                ->join('member', 'member.profile_id', '=', 'profile.id')
                ->where('member.id', '=', $id)
                ->first();
        } catch(Exception $e) {
            Error.log($e);
        }
        return $profile;
    }
    
    private function getSelectedMember($memberId)
    {
        $collection = $this->getMembers(null);
        if($collection == null) {
            return null;
        } else {
            $node = $this->arraySearch($collection,'member_id', $memberId);
            return $node;
        }
        
        return;
    }
    
    private function getMembers($parent_id) 
    {
        $collection = null;

        try {
            if ($parent_id != null && is_numeric($parent_id)) {
                $collection = DB::select("SELECT m.id AS 'member_id', m.parent_id, m.profile_id, CONCAT(p.first_name, ' ',p.last_name) AS 'name', p.gender FROM member m LEFT JOIN profile p ON m.profile_id = p.id WHERE parent_id = $parent_id AND m.deleted_at is null");
            } else {
                $collection = DB::select("SELECT m.id AS 'member_id', m.parent_id, m.profile_id, CONCAT(p.last_name, ', ',p.first_name) AS 'name', p.gender FROM member m LEFT JOIN profile p ON m.profile_id = p.id WHERE m.deleted_at is null");
            }
        } catch (Exception $e) {
            Error.log($e);
        }
        return $collection;
    }
    
    private function getParent($memberid)
    {
        $profile = null;
        try {
            $member = DB::table('member')
                ->where('member.id', '=', $memberid)
                ->first();
            if($member) {
                $profile = DB::table('profile')
                    ->join('member', 'member.profile_id', '=', 'profile.id')
                    ->where('member.id', '=', $member->parent_id)
                    ->first();
            }
        } catch(Exception $e) {
            Error.log($e);
        }
        return $profile;
    }
    
    private function getTotalProductPurchased($member_id, $lastIncentiveDate = null)
    {
        $purchases = null;
        try {
            if($member_id != null && is_numeric($member_id) && $lastIncentiveDate != null) {
                $query = "SELECT ";
                $query .= "packages.title as 'package', ";
                $query .= "sales.id as 'sales_id', ";
                $query .= "sales.created_at as 'sales_datetime', ";
                $query .= "salesdetails.price ";
                $query .= "FROM ";
                $query .= "salesdetails ";
                $query .= "INNER JOIN sales ON (salesdetails.sales_order_id = sales.id) ";
                $query .= "INNER JOIN packages ON (salesdetails.package_id = packages.id) ";
                $query .= "WHERE sales.member_id = $member_id ";
                $query .= " AND sales.created_at BETWEEN '". Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $lastIncentiveDate[0]->created_at)->toDateTimeString();
                $query .= "' AND '". Carbon\Carbon::now()."'";
                $purchases = DB::select($query);
            }
            if($lastIncentiveDate == null) {
                $query = "SELECT ";
                $query .= "packages.title as 'package', ";
                $query .= "sales.id as 'sales_id', ";
                $query .= "sales.created_at as 'sales_datetime', ";
                $query .= "salesdetails.price ";
                $query .= "FROM ";
                $query .= "salesdetails ";
                $query .= "INNER JOIN sales ON (salesdetails.sales_order_id = sales.id) ";
                $query .= "INNER JOIN packages ON (salesdetails.package_id = packages.id) ";
                $query .= "WHERE sales.member_id = $member_id ";
                $purchases = DB::select($query);
            }
        } catch(Exception $e) {
            Error.log($e);
        }
        return $purchases;
    }
    
    private function getTotalProductPurchased2($member_id, $salesFrom, $salesTo)
    {
        $purchases = null;
        try {
            if($member_id != null && is_numeric($member_id) && $salesFrom != null && $salesTo != null) {
                $query = "SELECT ";
                $query .= "packages.title as 'package', ";
                $query .= "sales.id as 'sales_id', ";
                $query .= "sales.created_at as 'sales_datetime', ";
                $query .= "salesdetails.price, salesdetails.package_id ";
                $query .= "FROM ";
                $query .= "salesdetails ";
                $query .= "INNER JOIN sales ON (salesdetails.sales_order_id = sales.id) ";
                $query .= "INNER JOIN packages ON (salesdetails.package_id = packages.id) ";
                $query .= "WHERE sales.member_id = $member_id ";
                $query .= " AND sales.created_at BETWEEN '". $salesFrom;
                $query .= " 00:00:00' AND '". $salesTo." 23:59:59'";
                $purchases = DB::select($query);
            }
        } catch(Exception $e) {
            Error.log($e);
        }
        return $purchases;
    }
    
    public function totalPurchases(Request $request)
    {
        $purchases = null;
        $html = "";
        try {
            if($request->ajax()) {
                $member_id = stripcslashes($_POST['pMemberId']);
                $date = stripcslashes($_POST['pDate']);

                if($member_id != null && is_numeric($member_id) && $date != "null") {
                    $query = "SELECT ";
                    $query .= "packages.title as 'package', ";
                    $query .= "sales.id as 'sales_id', ";
                    $query .= "sales.created_at as 'sales_datetime', ";
                    $query .= "salesdetails.price ";
                    $query .= "FROM ";
                    $query .= "salesdetails ";
                    $query .= "INNER JOIN sales ON (salesdetails.sales_order_id = sales.id) ";
                    $query .= "INNER JOIN packages ON (salesdetails.package_id = packages.id) ";
                    $query .= "WHERE sales.member_id = $member_id ";
                    $query .= " AND sales.created_at BETWEEN '". Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date);
                    $query .= "' AND '". Carbon\Carbon::now()."'";
                    $query .= " ORDER BY sales.created_at DESC ";
                    $purchases = DB::select($query);
    //                $purchases->setPath('sales_details');
                }
                if($date == "null") {
                    $query = "SELECT ";
                    $query .= "packages.title as 'package', ";
                    $query .= "sales.id as 'sales_id', ";
                    $query .= "sales.created_at as 'sales_datetime', ";
                    $query .= "salesdetails.price ";
                    $query .= "FROM ";
                    $query .= "salesdetails ";
                    $query .= "INNER JOIN sales ON (salesdetails.sales_order_id = sales.id) ";
                    $query .= "INNER JOIN packages ON (salesdetails.package_id = packages.id) ";
                    $query .= "WHERE sales.member_id = $member_id ";
                    $query .= " ORDER BY sales.created_at DESC ";
                    $purchases = DB::select($query);
    //                $purchases->setPath('sales_details');
                }
            }
            
            if($purchases != null) {
                foreach($purchases as $key => $val) {
                    $html .= '<tr>';
                    $html .= '<td>'.$val->sales_datetime.'</td>';
                    $html .= '<td>Package '.$val->package.'</td>';
                    $html .= '<td>Php '.number_format($val->price).'</td>';
                    $html .= '</tr>';
                }
            }
            
            $data = [ 'data' => $html ];
            
            return response()->json($data, 200, [], JSON_PRETTY_PRINT);
        } catch(Exception $e) {
            Error.log($e);
        }
        return $purchases;
    }
    
    private function getLastPurchaseDate($member_id)
    {
        $collection = null;
        
        try {
            if($member_id != null && is_numeric($member_id)) {
                $query = "SELECT ";
                $query .= "sales.created_at ";
                $query .= "FROM sales ";
                $query .= "WHERE sales.member_id = $member_id ";
                $query .= " ORDER BY sales.created_at DESC";
                $collection = DB::select(DB::raw($query));
            }
        } catch(Exception $e) {
            Error.log($e);
        }
        
        return $collection;
    }
    
    private function getLastIncentiveDate($member_id)
    {
        $collection = null;
        
        try {
            if($member_id != null && is_numeric($member_id)) {
                $query = "SELECT ";
                $query .= "released_incentives.sales_ids, ";
                $query .= "released_incentives.created_at ";
                $query .= "FROM released_incentives ";
                $query .= "WHERE member_id = $member_id ";
                $query .= " AND deleted_at is null ";
                $query .= "ORDER BY ";
                $query .= "created_at DESC";
                $collection = DB::select($query);
            }
        } catch(Exception $e) {
            Error.log($e);
        }
        
        return $collection;
    }
    
    private function getMemberRegistrationDateTime($member_id)
    {
        $collection = null;
        
        try {
            if($member_id != null && is_numeric($member_id)) {
                $query = "SELECT ";
                $query .= "profile.registration_date as 'created_at' ";
                $query .= "FROM profile ";
                $query .= "INNER JOIN member ON (member.profile_id = profile.id) ";
                $query .= "WHERE member.id = $member_id ";
                $query .= " ORDER BY member.created_at DESC";
                $collection = DB::select(DB::raw($query));
            }
        } catch(Exception $e) {
            Error.log($e);
        }
        
        return $collection;
    }
    
    public function printPreview(Request $request)
    {
        $html = $_GET['pContent'];
        
        $pageTitle = 'Print Preview';
        try {
            return view('content.bonus.print', compact('pageTitle', 'html'))->render();
//            return response()->json(array('success' => true, 'html' => $html));
        } catch(Exception $e) {
            Error.log($e);
        }
    }
    
    public function printReferral(Request $request)
    {
        $pageTitle = 'Print Preview';
        $html = $request->html_data;
        try {
            if($html != null && $html != "") {
                return view('content.bonus.print', compact('pageTitle', 'html'))->render();
            }
        } catch(Exception $e) {
            Error.log($e);
        }
        
    }
    
    public function exportReferral(Request $request)
    {
        $html = $request->excel_data;
        
        try {
            Excel::create('Referral Claim '.strtotime(Carbon\Carbon::now()), function($excel) {
                // Set the title
                $excel->setTitle('no title');
                $excel->setCreator('no no creator')->setCompany('no company');
                $excel->setDescription('report file');

                $excel->sheet('sheet1', function($sheet) {
                    $data = array(
                        array('Downline Name', 'Purchase Date','Package','Amount','Less 10% Tax','Total'),
                        array('data1', 'data2', 300, 400, 500, 0, 100),
                        array('data1', 'data2', 300, 400, 500, 0, 100),
                        array('data1', 'data2', 300, 400, 500, 0, 100),
                        array('data1', 'data2', 300, 400, 500, 0, 100),
                        array('data1', 'data2', 300, 400, 500, 0, 100),
                        array('data1', 'data2', 300, 400, 500, 0, 100)
                    );
                    $sheet->fromArray($data, null, 'A1', false, false);
                    $sheet->cells('A1:G1', function($cells) {
                        $cells->setBackground('#AAAAFF');
                    });
                });
            })->download('xlsx');
        } catch(Exception $e) {
            Error.log($e);
        }
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function profileNameSearch(Request $request)
    {
        $data = null;
        
        if ($request) {
            $q = $request->get('q');
            //dd($request->get('member') );
            try {
                $query = null;
                $query = "SELECT m.id AS id, m.parent_id, m.profile_id, CONCAT(p.first_name,' ',p.last_name) AS 'name' FROM member m LEFT JOIN profile p ON m.profile_id = p.id WHERE m.deleted_at IS NULL AND CONCAT(p.first_name, ' ', p.last_name) LIKE '%$q%' LIMIT 10";
                $searchBy = 'INCENTIVES';

                if ($query != null)
                    $data = DB::select($query);
            
            } catch (Exception $e) {
                Error.log($e);
            }
        }
        return view('ajax.a_profile_name_dropdown', compact('data','searchBy'));
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(!Auth::check()) return view('auth.login');
        
        $this->setMemberCollection($this->getMembers(null));
        
        $pageTitle = 'Compute Incentives';
        $memberId = -1;
        if ($request && $request->get('id') != null && is_numeric($request->get('id'))) {
            $memberId = $request->get('id');
        }
        return view("content.bonus.compute", compact('pageTitle', 'memberId'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */ 
    public function store(Request $request)
    {
        if(!Auth::check()) return view('auth.login');
        
        try {
            DB::beginTransaction();
            
            if($request->ajax()) {
//                $tableData = stripcslashes($_POST['pTableData']);
//                $tableData = json_decode($tableData, TRUE);
                
                $memberId = stripcslashes($_POST['pMemberId']);
                $grandTotal = stripcslashes($_POST['pTotal']);
                $from = stripcslashes($_POST['pCoverageFrom']);
                $to = stripcslashes($_POST['pCoverageTo']);
                $sales_ids = stripcslashes($_POST['pSalesIds']);
//                dd($sales_ids);

                if(isset($memberId) && isset($grandTotal)) {
                    $releasedIncentive = new ReleasedIncentives();
                    $releasedIncentive->member_id = $memberId;
                    $releasedIncentive->from = $from;
                    $releasedIncentive->to = $to;
                    $releasedIncentive->sales_ids = '['.$sales_ids.']';
                    $releasedIncentive->grand_total = floatval(str_replace(",","",$grandTotal));
                    $releasedIncentive->released_by_id = Auth::user()->id;
//                    if($releasedIncentive->save()) {
//                        foreach($tableData as $key => $node) {
//                            $detail = new ReleasedIncentivesDetails();
//                            $detail->released_incentives_id = $releasedIncentive->id;
//                            $detail->level = $node['level'];
//                            $detail->total_products = $node['productTotal'];
//                            $detail->incentive = floatval(str_replace(",","",$node['incentive']));
//                            $detail->save();
//                        }
//                    }
                    $releasedIncentive->save();
                    DB::commit();
                    
                    $data = [
                        'success' => TRUE,
                        'message' => 'Incentives for this member has been released!'
                    ];
                } else {
                    $data = [
                        'success' => FALSE,
                        'message' => 'There was an error processing your request.'
                    ];
                }
                return response()->json($data, 200, [], JSON_PRETTY_PRINT);
            } else {
                // redirect to dashboard?
            }
        } catch(Exception $e) {
            DB::rollback();
            Error.log($e);
        }
    }
    
    public function viewReport()
    {
        if(!Auth::check()) return view('auth.login');
        
        $pageTitle = 'Referral Claims Report';
        try {
            return view("content.bonus.report", compact('pageTitle'));
        } catch(Exception $e) {
            Error.log($e);
        }
    }
    
    public function viewWeeklyReport()
    {
        if(!Auth::check()) return view('auth.login');
        
        $pageTitle = 'Weekly Claims Report';
        try {
            return view("content.bonus.releasing_report", compact('pageTitle'));
        } catch(Exception $e) {
            Error.log($e);
        }
    }
    
    private function getProcessedIncentives($memberid) 
    {
        // Processed Incentives
        // All records in Released Incentives with NULL/NOT NULL sales_ids
        // of a particular memberid
        $salesIdCollection = "";
        try 
        {
            // Get last incentive date
            $lastIncentiveDate = $this->getLastIncentiveDate($memberid);
            // Get previous released incentives from sales
            foreach($lastIncentiveDate as $incentive) {
                if(isset($incentive->sales_ids)) {
                    $sid = str_replace('[', '', $incentive->sales_ids);
                    $sid = str_replace(']', '', $sid);
                    if($sid != '' && $sid != null) {
                        if($salesIdCollection == null) {
                            $salesIdCollection = $sid . ",";
                        }
                        else {
                            $salesIdCollection .= ",".$sid;
                        }
                    }
                }
            }
            $salesIdCollection = substr_replace($salesIdCollection, '', -1);
        } catch(Exception $e) {
            Error.log($e);
        }
        return $salesIdCollection;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    /*
     * Report to display all members that are eligible for
     * incentives based on a given date range.
     * 
     * 1. Get all members that are ACTIVE
     * 2. Build the tree of each members from #1
     * 3. Get the total sales from their downlines
    */
    public function displayActiveMembersWithIncentives(Request $request)
    {
        if(!Auth::check()) return view('auth.login');
        
        $html = "";
        $isMemberActive = false;
        $lastPurchaseDate = null;
        $salesIdCollection = array();
        $activeMembersCollection = array();
        
        try {
            if($request->ajax()) {
                $salesFrom = $_POST['pSalesFrom'];
                $salesTo = $_POST['pSalesTo'];
                
                $salesFrom = date("Y-m-d", strtotime($salesFrom));
                $dateFrom = strtotime($salesFrom);
                $month = date("F",$dateFrom);
                $year = date("Y",$dateFrom);
                $firstDayofThisMonth = Carbon\Carbon::parse('first day of '.$month.' '.$year);
                
                $salesTo = date("Y-m-d", strtotime($salesTo));
                $dateTo = strtotime($salesTo);
                $month = date("F",$dateTo);
                $year = date("Y",$dateTo);
                $lastDayofThisMonth = Carbon\Carbon::parse('last day of '.$month.' '.$year);
                
                $this->setMemberCollection($this->getMembers(null));  
                
                if( $this->getMemberCollection() && count($this->getMemberCollection()) > 0 ) {
                    // Get all ACTIVE MEMBERS
                    foreach($this->getMemberCollection() as $member)
                    {
                        if(isset($member)) {
                            $lastPurchaseDate = $this->getLastPurchaseDate($member->member_id);
                            if(empty(array_filter($lastPurchaseDate))) $lastPurchaseDate = null;
                            if($lastPurchaseDate != null) {
                                $isMemberActive = strtotime($lastPurchaseDate[0]->created_at) >= strtotime($firstDayofThisMonth) && strtotime($lastPurchaseDate[0]->created_at) <= strtotime($lastDayofThisMonth);
                            }
                            
                            if($isMemberActive) array_push($activeMembersCollection, $member);
                        }
                    }    
                    if(!empty(array_filter($activeMembersCollection))) usort($activeMembersCollection, array($this, "sortMembers"));

                    foreach($activeMembersCollection as $member) {
                        $html .= '<tr id='.$member->member_id.'>';
                        $html .= '<td>'.$member->name.'</td>';
                        $html .= '<td id = dequode_'.$member->member_id.'><a class="btn btn-sm btn-info btn-flat" href="javascript(0);" onclick="calculateIncentive('.$member->member_id.'); return false;">Compute</a></td>';
                        $html .= '<td></td>';
                        $html .= '</tr>';
                    }
                }
                
                return response()->json([
                    'html' => $html, 'from' => $salesFrom, 'to' => $salesTo]);
            }
        } catch(Exception $e) {
            Error.log($e);
        }
    }
    
    /*
     * #1 Get released incentives of PARENT member
     * #2 Get the member's downlines upto total incentive level
     * #3 Get the purchases of each of the downline
     * #4 During computation, do not include purchases that were already released,
     *     look at RELEASED_INCENTIVES.SALES_IDS column for verification
    */    
    public function computeTotalIncentivesPerMember(Request $request)
    {
        $memberid = null;
        $salesFrom = null;
        $salesTo = null;
        
        try {
            $memberid = $_POST['pMemberId'];
            $salesFrom = $_POST['pSalesFrom'];
            $salesTo = $_POST['pSalesTo'];
            
            if(isset($memberid) && isset($salesFrom) && isset($salesTo)) {
                // Step #1
                $releasedIncentivesArray = $this->getReleasedIncentivesSalesIDArray($memberid);
                
                // Get all downlines?
                $tree = $this->buildUnilevelTree($this->getMembers(null), $memberid);
//                dd($tree);
                
                $salesFrom = date("Y-m-d", strtotime($salesFrom));
                $salesTo = date("Y-m-d", strtotime($salesTo));
                
                // Gather all the released sales & purchases for the given dates for this member
                $purchasesFromDates = $this->getTotalProductPurchased2($memberid, $salesFrom, $salesTo);
                if(isset($purchasesFromDates)) {
                    foreach($purchasesFromDates as $purchase) {
                        
                    }
                }
                
                
                
            }
        } catch(Exception $e) {
            Error.log($e);
        }
    }
    
    private function getReleasedIncentivesSalesIDArray($member_id)
    {
        $collection = null;
        $salesIDArray = array();
        
        try {
            if($member_id != null && is_numeric($member_id)) {
                $query = "SELECT ";
                $query .= "released_incentives.sales_ids ";
                $query .= "FROM released_incentives ";
                $query .= "WHERE member_id = $member_id ";
                $query .= " AND deleted_at is null ";
                $query .= "ORDER BY ";
                $query .= "created_at DESC";
                $collection = DB::select($query);
                
                if(isset($collection)) {
                    foreach($collection as $incentive) {
                        $sid = str_replace('[', '', $incentive->sales_ids);
                        $sid = str_replace(']', '', $sid);
                        if($sid != '' && $sid != null) {
                            array_push($salesIDArray, $sid);
                        }
                    }
                }
            }
        } catch(Exception $e) {
            Error.log($e);
        }
        return $salesIDArray;
    }
    
    private function sortMembers($a, $b) 
    {
        return strcmp(trim($a->name), trim($b->name));
    }
}
