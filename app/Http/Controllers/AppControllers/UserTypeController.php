<?php

namespace App\Http\Controllers\AppControllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\AppCore\Models\Usertype;
use App\AppCore\Models\Userrole;
use DB;
use Auth;

class UserTypeController extends Controller
{
    private $userrole;
    
    public function __construct()
	{
		$this->userrole = DB::table('userrole')->where('is_active',1)->lists('title','id');
	}
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Usertype $object)
    {
        if(!Auth::check()) return view('auth.login');
        
        try {
			$pageTitle = 'User Type';
			$usertype = $object->where('is_active',1)->get(); 
			return view('content.usertype.index', compact('usertype','pageTitle'));

		} catch (Exception $e) {
			//return $e->getMessage();
		}
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(!Auth::check()) return view('auth.login');
        $pageTitle = 'User Type';
        $userroles = $this->userrole;
        return view('content.usertype.create', compact('pageTitle','userroles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Usertype $usertype, Requests\CreateUserTypeRequest $request)
    {
        if(!Auth::check()) return view('auth.login');
        DB::beginTransaction();
		try
		{
			$object = new Usertype;
			$object->title = $request->get('title');
			$object->role_id = $request->get('role_id');
			$object->is_active = 1;
			$object->save();
			DB::commit();
			$response = 'Record was successfuly saved...';
			$classstyle = 'success';
		}catch (Exception $ex){
			$response = $e->getMessage();
			$classstyle = 'danger';
			DB::rollback();
		}

		return redirect()->route('usertype.index')->with('message',$response)->with('classstyle',$classstyle);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Usertype $usertype)
    {
        if(!Auth::check()) return view('auth.login');
        $pageTitle = 'User Type';
		try {
			if (!empty($usertype) && $usertype != null && $usertype->exists == true){
				$userroles = $this->userrole;
				return view('content.usertype.show', compact('usertype','pageTitle','userroles'));
			} else {
				$response = 'No record found...';
				$classstyle = 'danger';
				return redirect('usertype')->with('message',$response)->with('classstyle',$classstyle);
			}
			
		} catch (Exception $e) {
			Error.log($e);
		}
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Usertype $usertype)
    {
        if(!Auth::check()) return view('auth.login');
        $pageTitle = 'User Type';
		try {
			if (!empty($usertype) && $usertype != null && $usertype->exists == true){
				$userroles = $this->userrole;
				return view('content.usertype.edit', compact('usertype','pageTitle','userroles'));
			} else {
				$response = 'No record found...';
				$classstyle = 'danger';
				return redirect('usertype')->with('message',$response)->with('classstyle',$classstyle);
			}
			
		} catch (Exception $e) {
			Error.log($e);
		}
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Usertype $usertype, Requests\UpdateUserTypeRequest $request)
    {
        if(!Auth::check()) return view('auth.login');
        
        # code to check if the request object is null
		if (!isset($request)) return 'Error';
		
		if (!empty($request->get('delete')))
		{
			# code to delete selected item
			return $this->destroy($usertype);
		}
		elseif (!empty($request->get('update'))) {
			# code to update selected item
			try
			{
				$usertype->title = $request->get('title');
				$usertype->role_id = $request->get('role_id');
				$usertype->save();
				$response = 'Record was successfuly updated...';
				$classstyle = 'success';
			}catch (Exception $ex){
				$response = $e->getMessage();
				$classstyle = 'danger';
			}
			return redirect('usertype')->with('message',$response)->with('classstyle',$classstyle);
		}
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Usertype $usertype)
    {
        try
		{
            if(!Auth::check()) return view('auth.login');
            
			$usertype->is_active = 0;
			$usertype->save();
			$response = 'Record was successfuly deleted...';
			$classstyle = 'success';
		}catch (Exception $ex){
			$response = $e->getMessage();
			$classstyle = 'danger';
		}
		return redirect('usertype')->with('message',$response)->with('classstyle',$classstyle);
    }
}
