<?php

namespace App\Http\Controllers\AppControllers;

use Illuminate\Http\Request;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;

class IndexController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::check()) {
            $pageTitle = 'Dashboard';
            $newOrders = 0;
            $salablePackage = 0;
            $totalSales = 0;
            $memberCount = 0;

            try {
                
                /* Get new orders count*/
                $newOrders = DB::table('sales')
                    ->select( DB::raw('IFNULL(COUNT(sub_total),0) as count'))
                    ->whereBetween('created_at', array(date("Y-m-d") .' 00:00:00', date("Y-m-d").' 23:59:59'))->first();
                
                if ($newOrders != null)
                    $newOrders = $newOrders->count;

                /* Get saleable package*/
                $salablePackage = DB::table('salesdetails')
                    ->select(
                        'package_id',
                        DB::raw('IFNULL(COUNT(package_id),0) as top')
                        )
                    ->groupBy('package_id')
                    ->orderBy('top', 'desc')
                    ->first();

                if ($salablePackage !=null)
                    $salablePackage = $salablePackage->package_id;

                /* Get Total Sales */
                $totalSales = DB::table('sales')
                    ->select (
                        DB::raw('IFNULL(FORMAT(SUM(grand_total),2),0) as grand_total')
                        )
                    ->whereIn('status', array('Complete', 'Cheque Issued'))
                    ->first();
                
                if ($totalSales !=null)
                    $totalSales = $totalSales->grand_total;

                /* Get member Count */
                $memberCount = DB::table('member')
                    ->select (
                        DB::raw('IFNULL(COUNT(id),0) as count')
                        )
                    ->whereNull('deleted_at')  
                    ->first();
                if ($memberCount != null)
                    $memberCount = $memberCount->count;

                //dd($newOrders,$salablePackage,$totalSales,$memberCount);

            } catch (Exception $e) {
                Error.log($e);
            }

            return view('content.dashboard', compact('pageTitle','newOrders','salablePackage','totalSales','memberCount'));
        } else {
            return view('auth.login');
        }
    }
}