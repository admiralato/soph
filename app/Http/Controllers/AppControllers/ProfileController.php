<?php

namespace App\Http\Controllers\AppControllers;

use Illuminate\Http\Request;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Carbon\Carbon;
use App\AppCore\Models\Profile;
use App\AppCore\Models\Member;

class ProfileController extends Controller
{
    private $profile;


    public function __construct(Profile $item)
    {
        $this->profile = $item;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(!Auth::check()) return view('auth.login');
        
        $pageTitle = 'Profile Manager';
        try {
            $q = NULL; $sb = NULL;

            if ($request != null && $request->get('q') != null && $request->get('sb') !=null
                && strlen($request->get('q')) > 0 && strlen($request->get('sb')) > 0) {
                $q = $request->get('q'); $sb = $request->get('sb');

                $profiles = DB::table('profile')
                    ->leftJoin('member', 'profile.id', '=', 'member.profile_id')
                    ->select('profile.*', 'member.id as member_id')
                    ->whereNull('profile.deleted_at')
                    ->where( 
                        ( $sb=='pid'? 'profile.id' : ($sb =='email'? 'email': ($sb =='mb'? 'mobile_no': ( $sb =='ln'? 'profile.last_name': 'profile.first_name') ) )  ),
                        ( $sb=='pid'? '=' : 'LIKE' ),
                        ( $sb=='pid'? $q : '%'.$q.'%' )
                        )
                    ->orderBy('profile.created_at', 'desc')
                    ->paginate(8);
                $profiles->setPath("profile?q=$q&sb=$sb");
                //$members->setPath("member?q=$q&sb=$sb");
            } else {

                $profiles = DB::table('profile')
                    ->leftJoin('member', 'profile.id', '=', 'member.profile_id')
                    ->select('profile.*', 'member.id as member_id')
                    ->whereNull('profile.deleted_at')
                    ->orderBy('profile.created_at', 'desc')
                    ->paginate(8);
                $profiles->setPath('profile');

            }
            
            return view('content.profile.index', compact('pageTitle','profiles'));
        } catch (Exception $e) {
            Error.log($e);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(!Auth::check()) return view('auth.login');
        $pageTitle = 'Create Profile';
        return view('content.profile.create', compact('pageTitle'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Requests\CreateProfileRequest $request)
    {
        if(!Auth::check()) return view('auth.login');
        //dd(storage_path());

        DB::beginTransaction();
        try
        {
            $slug = str_slug($request->first_name . '-' . $request->last_name);

            $img = $request->profile_pic_url;
            if ( strpos($img, 'data:image/jpeg;base64') > -1 ) {
                $img = str_replace('data:image/jpeg;base64,', '', $img);
                $img = str_replace(' ', '+', $img);
                $fileData = base64_decode($img);
                $fileName = storage_path().'/img/profile/'. strtolower($slug) . '.jpg';
                file_put_contents($fileName, $fileData);
            }

            $model = new Profile;
            $model->profile_key = $slug;
            $model->first_name = $request->first_name;
            $model->last_name = $request->last_name;
            $model->middle_name = $request->middle_name;
            $model->nick_name = '';
            $model->profile_pic_url = url().'/storage/img/profile/' . strtolower($slug) . '.jpg';
            $model->age = $request->age;
            $model->date_of_birth = date("Y-m-d", strtotime($request->date_of_birth)); 
            $model->registration_date = date("Y-m-d", strtotime($request->registration_date));
            $model->gender = $request->gender;
            $model->status = $request->status;
            $model->address_primary = $request->address_primary;
            $model->address_secondary = $request->address_secondary;
            $model->contact_no = $request->contact_no;
            $model->mobile_no = $request->mobile_no;
            $model->email = $request->email;
            $model->zip = $request->zip;
            $model->country = $request->country;
            $model->tin = $request->tin;
            $model->save();
           
            DB::commit();
            $response = 'Record was successfully saved!';
            $classstyle = 'success';
        } catch(Exception $e) {
            $response = $e->getMessage();
            $classstyle = 'danger';
            DB::rollback();
        }
        
        return redirect('profile')->with('message',$response)->with('classstyle',$classstyle);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Profile $item)
    {
        if(!Auth::check()) return view('auth.login');
        $pageTitle = 'View Profile';
        try {
            
            if (!empty($item) && $item != null){
                return view('content.profile.show', compact('item','pageTitle'));
            }
            
        } catch (Exception $e) {
            Error.log($e);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Profile $item)
    {
        $pageTitle = 'Edit Profile';
        try {
            if (!empty($item) && $item != null){
                return view('content.profile.edit', compact('item','pageTitle'));
            }
            
        } catch (Exception $e) {
            Error.log($e);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Profile $model, Requests\UpdateProfileRequest $request)
    {
        if(!Auth::check()) return view('auth.login');
        $response = 'Null';
        $classstyle = 'danger';
        //dd($request);

        if (!empty($request->get('delete')) && !empty($model))
        {
            # code to delete selected item
            return $this->destroy($model);
        } else if(!empty($request) && !empty($model)) {
            # code to update selected item
            DB::beginTransaction();
            try {
                
                $slug = $request->profile_key;
                $img = $request->profile_pic_url;
                if ( strpos($img, 'data:image/jpeg;base64') > -1 ) {
                    $img = str_replace('data:image/jpeg;base64,', '', $img);
                    $img = str_replace(' ', '+', $img);
                    $fileData = base64_decode($img);
                    $fileName = storage_path().'/img/profile/'. strtolower($slug) . '.jpg';
                    file_put_contents($fileName, $fileData);
                }

                $model->profile_key = $request->profile_key;
                $model->first_name = $request->first_name;
                $model->last_name = $request->last_name;
                $model->middle_name = $request->middle_name;
                $model->age = $request->age;
                $model->date_of_birth = date("Y-m-d", strtotime($request->date_of_birth)); 
                $model->registration_date = date("Y-m-d", strtotime($request->registration_date));
                if(strpos($img, 'data:image/jpeg;base64') > -1) {
                    $model->profile_pic_url = url().'/storage/img/profile/' . strtolower($slug) . '.jpg';
                }
                $model->gender = $request->gender;
                $model->status = $request->status;
                $model->address_primary = $request->address_primary;
                $model->address_secondary = $request->address_secondary;
                $model->contact_no = $request->contact_no;
                $model->mobile_no = $request->mobile_no;
                $model->email = $request->email;
                $model->zip = $request->zip;
                $model->country = $request->country;
                $model->tin = $request->tin;
                if($model->save()) {
                    $time = date("H:i:s", strtotime(Carbon::now()->toDateTimeString()));
                    Member::where('profile_id', $model->id)
                      ->update(['created_at' => date("Y-m-d ".$time, strtotime($request->registration_date))]);
                }
                
                DB::commit();

                $response = 'Record was successfuly updated...';
                $classstyle = 'success';
            } catch(Exception $e) {
                $response = $e->getMessage();
                DB::rollback();
            }

            return redirect('profile')->with('message',$response)->with('classstyle',$classstyle);
        }

        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Profile $item)
    {
        if(!Auth::check()) return view('auth.login');
        Profile::destroy($item->id);
        
        $response = $item->first_name .'\'s profile was successfuly deleted...';
        $classstyle = 'success';
        return redirect('profile')->with('message',$response)->with('classstyle',$classstyle);
    }

    public function rollbackd($id)
    {
        if(!Auth::check()) return view('auth.login');
        $response = 'No record found.';
        $classstyle = 'danager';
        $model = Profile::withTrashed()
                ->where('id', $id)
                ->whereNotNull('deleted_at')
                ->get();


        return redirect('profile');
                
    }
}
