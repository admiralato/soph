<?php

namespace App\AppCore\Models;

use Illuminate\Database\Eloquent\Model;

class Salesdetails extends Model
{
    protected $table = 'salesdetails';

    protected $fillable = [
		'sales_order_id',
		'package_id','package_title','package_pdc',
		'cheque_issued',
		'price'
	];
}
