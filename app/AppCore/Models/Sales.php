<?php

namespace App\AppCore\Models;

use Illuminate\Database\Eloquent\Model;

class Sales extends Model
{
    protected $table = 'sales';

    protected $fillable = [
		'member_id',
		'profile_id', 'date_purchased',
		'name','email','email_sent',
		'status',
		'sub_total', 'discount',
		'grand_total'
	];
}
