<?php

namespace App\AppCore\Models;

use Illuminate\Database\Eloquent\Model;

class ReleasedIncentivesDetails extends Model
{
    protected $table = 'released_incentives_details';
    
    /**
     * Get the released incentive that owns the detail.
     */
    public function incentive()
    {
        return $this->belongsTo('App\AppCore\Models\ReleasedIncetives');
    }
}
