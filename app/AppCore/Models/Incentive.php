<?php

namespace App\AppCore\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Incentive extends Model
{
    use SoftDeletes;
    
    protected $fillable = [
        'level',
        'incentive'
    ];
    
    protected $dates = ['deleted_at'];
}
