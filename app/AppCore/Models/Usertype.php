<?php

namespace App\AppCore\Models;

use Illuminate\Database\Eloquent\Model;

class Usertype extends Model
{
    protected $table = 'usertype';

	protected $fillable = [
		'title','role_id','is_active'
	];

	public function userrole() {
		return $this->hasOne('AppCore\Models\Userrole', 'id');
	}

	public function user()
	{
		return $this->belongsTo('App\User', 'usertype_id');
	}
}
