<?php

namespace App\AppCore\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Package extends Model
{
    use SoftDeletes;
    
    protected $fillable = [
        'title',
        'amount',
        'first_fifteen',
        'second_fifteen',
        'third_fifteen',
        'color'
    ];
    
    protected $dates = ['deleted_at'];
}
