<?php

namespace App\AppCore\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ReleasedIncentives extends Model
{
    use SoftDeletes;
    
    protected $table = 'released_incentives';
    protected  $fillable = [
        'member_id', 'grand_total', 'to', 'from', 'sales_ids'
    ];
    protected $dates = ['deleted_at'];
    
    /**
     * Get the details for the released incentive.
     */
    public function details()
    {
        return $this->hasMany('App\AppCore\Models\ReleasedIncetivesDetails', 'released_incentives_id');
    }
}
