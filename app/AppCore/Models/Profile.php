<?php 
namespace App\AppCore\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Profile extends Model
{
	use SoftDeletes;

    protected $table = 'profile';
    protected $dates = ['deleted_at'];

    protected $fillable = [
		'profile_key',
		'profile_pic_url',
		'first_name','last_name','middle_name','nick_name',
		'age','date_of_birth', 'registration_date',
		'gender', 'status',
		'address_primary','address_secondary','zip','country',
		'email','contact_no','mobile_no','deleted_at','tin'
	];

	public function setDobAttribute($value) {
       $this->attributes['date_of_birth'] = Carbon::createFromFormat('m-d-Y', $value);
    }

    public function getProfiles($active){

    	$collection = Profile::join('profile','profile.id','=','member.profile_id')
	        	->where('profile.deleted_at',1)
		        ->orderBy('teachers.id', 'DESC')
		        ->get([
		        	'teachers.id as id',
		        	'teachers.profile_id as profile_id',
		        	'profile_key',
		        	'first_name','last_name','middle_name','nick_name',
		        	'profile_pic_url',
		        	'age','date_of_birth','gender', 'registration_date',
		        	'address_primary','address_secondary','country','zip',
		        	'contact_no','mobile_no','email',
		        	'employment_type_id','teacher_status_id','tin',
		        	'teachers.is_active as teachers_is_active','profile.is_active as profile_is_active'
		        ]);
	        return $collection;
    }

}
