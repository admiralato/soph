<?php

namespace App\AppCore\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Userrole extends Model
{
    protected $table = 'userrole';
    
    protected $fillable = [
        'title','route_ids','is_active'
    ];
    
    private function getXmlData($key) {
        $data = null;
        try {
            $data = Cache::get($key);
            if(empty($data) || count($data) < 1) {
                $xmlPath = storage_path()."/xml/Modules.xml";
                $data = Util::simpleXmlToObject($xmlPath, '', '');
                Cache::put($key, $data->asXML(), 1440);
            } else {
                $data = new \SimpleXMLElement($data);
            }
        } catch(Exception $e) {
            $data = null;
        }
        return $data;
    }
    
    public function getUserRoles($id=null) {
        if($id==null) {
            $roles = Userrole::where('is_active', 1)
                ->get([
                    'id',
                    'title',
                    'route_ids'
                ]);
            return $roles;
        } else {
            $roles = Userrole::where('id',$id)
                ->get([
                    'id',
                    'title',
                    'route_ids'
                ])->first();
            return $roles;
        }
    }
    
    public function usertype() {
        return $this->belongsTo('AppCore\Models\Usertype', 'role_id');
    }
}
