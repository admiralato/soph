<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract
{
    use Authenticatable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';
    
    protected $primaryKey = 'id';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password', 'profile_id', 'usertype_id', 'is_active'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];
    
    public function usertype() {
		return $this->hasOne('App\AppCore\Models\Usertype', 'id');
	}
    
    public function getUser($id){

		try {
			$user = User::join('usertype','usertype.id','=','users.usertype_id')
			->join('userrole','userrole.id','=','usertype.role_id')
        	->where('users.is_active',1)
        	->where('users.id',$id)
	        ->get([
	        	'users.id as id',
	        	'users.name as name',
	        	'users.email as email',
	        	'users.profile_id as profile_id',
	        	'users.usertype_id as usertype_id',
	        	'usertype.title as usertype',
	        	'userrole.id as userrole_id',
	        	'userrole.title as userrole',
	        	'userrole.route_ids as route_ids'
	        ])->first();
		} catch (Exception $e) {
			$user = null;
		}
		
        return $user;
	}
    
    public function getUsers(){
		try {
			$users = User::join('usertype','usertype.id','=','users.usertype_id')
			->join('userrole','userrole.id','=','usertype.role_id')
        	->where('users.is_active',1)
	        ->get([
	        	'users.id as id',
	        	'users.name as name',
	        	'users.email as email',
	        	'users.profile_id as profile_id',
	        	'users.usertype_id as usertype_id',
	        	'usertype.title as usertype',
	        	'userrole.id as userrole_id',
	        	'userrole.title as userrole',
	        	'userrole.route_ids as route_ids'
	        ]);
		} catch (Exception $e) {
			$users = null;
		}
		
        return $users;
	}
}
