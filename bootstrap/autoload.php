<?php

define('LARAVEL_START', microtime(true));
use App\Helpers\Util;

/*
|--------------------------------------------------------------------------
| Register The Composer Auto Loader
|--------------------------------------------------------------------------
|
| Composer provides a convenient, automatically generated class loader
| for our application. We just need to utilize it! We'll require it
| into the script here so that we do not have to worry about the
| loading of any our classes "manually". Feels great to relax.
|
*/

require __DIR__.'/../vendor/autoload.php';

/*
|--------------------------------------------------------------------------
| Include The Compiled Class File
|--------------------------------------------------------------------------
|
| To dramatically increase your application's performance, you may use a
| compiled class file which contains all of the classes commonly used
| by a request. The Artisan "optimize" is used to create this file.
|
*/

$compiledPath = __DIR__.'/cache/compiled.php';

if (file_exists($compiledPath)) {
    require $compiledPath;
}

function withEmpty($selectList, $emptyLabel='') {
	return array(''=>$emptyLabel) + $selectList;
}

function randomRange($min,$max)
{
	return rand($min,$max);
}

function menuItems()
{
	if (Auth::user() != null && !empty(Auth::user())) {
		$route_ids = Auth::user()->getUser(Auth::user()->id)->route_ids;
		if (strlen($route_ids) > 0) {
			$arr = explode(',',$route_ids);
			$menu_items = Util::getHtmlMenu($arr);
			return $menu_items;
		}
	}
	return 'No Access rights';
}

function panelItems()
{
	if (Auth::user() != null && !empty(Auth::user())) {
		$route_ids = Auth::user()->getUser(Auth::user()->id)->route_ids;
		if (strlen($route_ids) > 0) {
			$arr = explode(',',$route_ids);
			$panel_items = Util::getHtmlPanelMenu($arr);
			return $panel_items;
		}
	}
	return 'No Access rights';
	
}